package com.hsf.im;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImIdGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImIdGeneratorApplication.class, args);
    }

}
