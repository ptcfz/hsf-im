package com.hsf.im.config;

import com.baidu.fsg.uid.impl.CachedUidGenerator;
import com.hsf.im.config.DisposableWorkerIdAssigner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UidConfig {

    @Bean
    public CachedUidGenerator cachedUidGenerator(){
        CachedUidGenerator cachedUidGenerator=new CachedUidGenerator();
        cachedUidGenerator.setWorkerIdAssigner(disposableWorkerIdAssigner());
        cachedUidGenerator.setTimeBits(29);
        cachedUidGenerator.setWorkerBits(21);
        cachedUidGenerator.setSeqBits(13);
        cachedUidGenerator.setEpochStr("2020-07-20");
        return cachedUidGenerator;
    }

    @Bean
    public DisposableWorkerIdAssigner disposableWorkerIdAssigner(){
        DisposableWorkerIdAssigner disposableWorkerIdAssigner=new DisposableWorkerIdAssigner();
        return  disposableWorkerIdAssigner;
    }
}
