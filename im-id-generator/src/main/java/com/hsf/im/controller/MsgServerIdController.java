package com.hsf.im.controller;

import com.hsf.im.service.MsgServerIdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: chenfz
 */
@RestController
@RequestMapping("/msgId")
public class MsgServerIdController {

    @Autowired
    private MsgServerIdService msgServerIdService;

    @RequestMapping("/getMsgServerId")
    public Long getMsgServerId(){
        return msgServerIdService.getMsgServerId();
    }
}
