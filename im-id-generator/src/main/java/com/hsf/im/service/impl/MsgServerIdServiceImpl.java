package com.hsf.im.service.impl;

import com.baidu.fsg.uid.impl.CachedUidGenerator;
import com.hsf.im.service.MsgServerIdService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: chenfz
 */
@Service
public class MsgServerIdServiceImpl implements MsgServerIdService {

    Logger logger= LoggerFactory.getLogger(MsgServerIdServiceImpl.class);

    @Autowired
    private CachedUidGenerator cachedUidGenerator;

    @Override
    public long getMsgServerId() {
        long id=cachedUidGenerator.getUID();
        logger.debug("generator msgServerId:"+id);
        return id;
    }
}
