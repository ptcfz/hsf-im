package com.hsf.im.service;

/**
 * @Author: chenfz
 * 消息 server-id生成
 */
public interface MsgServerIdService {

    public long getMsgServerId();
}
