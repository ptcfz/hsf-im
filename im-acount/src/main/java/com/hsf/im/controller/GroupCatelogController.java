package com.hsf.im.controller;

import com.hsf.im.domain.GroupCatelog;
import com.hsf.im.service.GroupCatelogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: chenfz
 */
@RestController
@RequestMapping("/groupCatelog")
public class GroupCatelogController {

    @Autowired
    private GroupCatelogService groupCatelogService;

    @GetMapping("/getAllGroupCatelog")
    public List<GroupCatelog> getAllGroupCatelog(){
        return groupCatelogService.getAllGroupCatelog();
    }

}
