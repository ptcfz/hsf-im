package com.hsf.im.controller;

import com.hsf.im.domain.Group;
import com.hsf.im.model.dto.GroupDto;
import com.hsf.im.service.GroupMembersService;
import com.hsf.im.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: chenfz
 */
@RestController
@RequestMapping("/group")
public class GroupController {

    @Autowired
    private GroupMembersService groupMembersService;

    @Autowired
    private GroupService groupService;

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @PostMapping("/createGroup")
    public void createGroup(@RequestBody(required = true) GroupDto groupDto){
        groupService.createGroup(groupDto.getGroup(),groupDto.getUser());
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @RequestMapping("/findUserJoinGroups")
    public List<Group> findUserJoinGroups(@RequestParam(value = "userId",required = true) Long userId,@RequestParam(value = "page",required = true) Integer page){
        return groupMembersService.findUserJoinGroups(userId,page);
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @PostMapping("/modifyGroupInfo")
    public void modifyGroupInfo(@RequestBody(required = true) Group group){
        if(group!=null){
            groupService.updateGroup(group);
        }
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @RequestMapping("/userQuitGroup")
    public void userQuitGroup(@RequestParam(value = "userId",required = true) Long userId,@RequestParam(value = "groupId",required = true) Long groupId){
        groupMembersService.userQuitGroup(userId,groupId);
    }

}
