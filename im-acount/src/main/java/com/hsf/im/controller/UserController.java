package com.hsf.im.controller;

import com.hsf.im.component.*;
import com.hsf.im.domain.User;
import com.hsf.im.service.UserService;
import com.hsf.im.service.ZookeeperService;
import com.hsf.im.utils.ProtostuffUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/user")
public class UserController {

    Logger logger= LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private ZookeeperService zookeeperService;

    private BCryptPasswordEncoder encoder=new BCryptPasswordEncoder();

    @RequestMapping("/register")
    public boolean register(@RequestBody User user){
        if(user == null){
            logger.error("user is null...");
            return false;
        }
        if(user.getPassword()==null){
            logger.error("user.password is null...");
            return false;
        }
        user.setPassword(encoder.encode(user.getPassword()));
        user.setRegisterTime(new Date());
        try{
            userService.register(user);
            logger.info("register user:"+user.getUsername()+" success!");
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            return false;
        }
        return true;
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @RequestMapping("/findUserByUsername")
    public User findUserByUsername(@RequestBody FindUserObj findUserObj){
        if(findUserObj==null || StringUtils.isEmpty(findUserObj.getUsername())){
            logger.error("username is null");
            return null;
        }
        User user=userService.findUserByUsername(findUserObj.getUsername());
        return user;
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @RequestMapping("/findUserById")
    public User findUserById(@RequestParam("userId") Long userId){
        User user = userService.findUserById(userId);
        return user;
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @RequestMapping("/getNettyAddress")
    public NettyAddressModel getNettyAddress(){
        NettyAddressModel model=new NettyAddressModel();
        ZkBalanceNode zkBalanceNode=zookeeperService.getBalanceNode();
        if(zkBalanceNode==null){
            model.setCode(SystemCode.NOT_FOUND.getCode());
            model.setMsg(SystemCode.NOT_FOUND.getMsg());
            return model;
        }
        byte[] payload=zookeeperService.getNodePayload(zkBalanceNode.getNodeName());
        ZkPayloadNode payloadNode= ProtostuffUtil.deserializer(payload,ZkPayloadNode.class);

        if(payloadNode!=null){
            model.setHost(payloadNode.getHost());
            model.setPort(payloadNode.getPort());
            model.setCode(SystemCode.SUCCESS.getCode());
            model.setMsg(SystemCode.SUCCESS.getMsg());
            return model;
        }
        model.setCode(SystemCode.NOT_FOUND.getCode());
        model.setMsg(SystemCode.NOT_FOUND.getMsg());
        return model;
    }

}
