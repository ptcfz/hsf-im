package com.hsf.im.dao;

import com.hsf.im.domain.GroupCatelog;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface GroupCatelogMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(GroupCatelog record);

    int insertSelective(GroupCatelog record);

    GroupCatelog selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GroupCatelog record);

    int updateByPrimaryKey(GroupCatelog record);

    List<GroupCatelog> getAllGroupCatelog();

}