package com.hsf.im.dao;

import com.hsf.im.domain.Group;
import com.hsf.im.domain.GroupMember;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GroupMemberMapper {

    int deleteByPrimaryKey(Long id);

    int insert(GroupMember record);

    int insertSelective(GroupMember record);

    GroupMember selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GroupMember record);

    int updateByPrimaryKey(GroupMember record);

    List<Group> findUserJoinGroups(@Param("userId") long userId,@Param("offset") int offset,@Param("size") int size);

    void userQuitGroup(@Param("userId") long userId,@Param("groupId") long groupId);
}