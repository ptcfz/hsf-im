package com.hsf.im.dao;

import com.hsf.im.domain.GroupMsg;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GroupMsgMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GroupMsg record);

    int insertSelective(GroupMsg record);

    GroupMsg selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GroupMsg record);

    int updateByPrimaryKey(GroupMsg record);
}