package com.hsf.im.dao;

import com.hsf.im.domain.OauthApprovals;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OauthApprovalsMapper {
    int insert(OauthApprovals record);

    int insertSelective(OauthApprovals record);
}