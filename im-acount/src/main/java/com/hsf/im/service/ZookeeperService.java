package com.hsf.im.service;

import com.hsf.im.component.ZkBalanceNode;
import com.hsf.im.component.ZkPayloadNode;
import com.hsf.im.utils.ProtostuffUtil;
import com.hsf.im.utils.ZookeeperClientFactory;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Service
public class ZookeeperService implements InitializingBean {

    Logger logger= LoggerFactory.getLogger(ZookeeperService.class);

    @Value("${zookeeper.address}")
    private String address;

    @Value("${zookeeper.nettyParentNode}")
    private String nettyParentNode;

    private CuratorFramework client;

    private List<ZkBalanceNode> zkBalanceNodes;

    /**
     * 获取客户端用于连接的znode
     * @return
     */
    public ZkBalanceNode getBalanceNode(){
        if(zkBalanceNodes!= null && zkBalanceNodes.size()!=0){
            return zkBalanceNodes.get(0);
        }
        return null;
    }


    /**
     * 创建持久顺序节点
     * @param path
     * @param payload
     */
    public void createPersistentSeqNode(String path,byte[] payload){
        createNode(path,payload,CreateMode.PERSISTENT_SEQUENTIAL);
    }

    /**
     * 创建持久非顺序节点
     * @param path
     * @param payload
     */
    public void createPersistentNode(String path,byte[] payload){
        createNode(path,payload,CreateMode.PERSISTENT);
    }

    /**
     * 创建临时顺序节点
     * @param path
     * @param payload
     */
    public void createEphemeralSeqNode(String path,byte[] payload){
        createNode(path,payload,CreateMode.EPHEMERAL_SEQUENTIAL);
    }

    /**
     * 创建临时节点
     * @param path
     * @param payload
     */
    public void createEphemeralNode(String path,byte[] payload){
        createNode(path,payload,CreateMode.EPHEMERAL);
    }


    /**
     * 获取节点的子节点
     * @param parentPath
     * @return
     */
    public List<String> getNodeChildren(String parentPath){
        try{
            Stat stat=client.checkExists().forPath(parentPath);
            if(stat!=null){
                List<String> nodes=client.getChildren().forPath(parentPath);
                return nodes;
            }
            return null;
        }catch (Exception e){
            logger.error("get node children error:"+parentPath,e);
        }
        return null;
    }

    /**
     * 获取节点的负载
     * @param path
     * @return
     */
    public byte[] getNodePayload(String path){
        try{
            Stat stat=client.checkExists().forPath(path);
            if(stat!=null){
                byte[] payload=client.getData().forPath(path);
                return payload;
            }
            return null;
        }catch (Exception e){
            logger.error("get node data error:"+path,e);
        }
        return null;
    }

    /**
     * 更新节点
     * @param path
     * @param payload
     */
    public void updateNode(String path,byte[] payload){
        try{
            client.setData().forPath(path,payload);
        }catch (Exception e){
            logger.error("update node :"+path+",error.",e);
        }
    }

    public void deleteNode(String path){
        try{
            client.delete().forPath(path);
        }catch (Exception e){
            logger.error("delete node:"+path+" error.",e);
        }
    }


    public void createNettyChildrenNodeListener(String path,PathChildrenCacheListener listener){
        try{
            PathChildrenCache cache=new PathChildrenCache(client,path,true);
            cache.getListenable().addListener(listener);
            cache.start(PathChildrenCache.StartMode.BUILD_INITIAL_CACHE);
        }catch (Exception e){
            logger.error("create netty child node error!",e);
        }
    }

    private void createNode(String path, byte[] payload, CreateMode mode){
        try{
            client.create().creatingParentsIfNeeded().withMode(mode).forPath(path,payload);
        }catch (Exception e){
            logger.error("create node path:"+path+" error",e);
        }
    }

    /**
     * 计算netty注册在zk上node的负载，以便下一次连接的时候提供给客户端 netty服务地址
     */
    private synchronized void doZnodeBalance(){
        logger.info("begin doZnodeBalance...");
        List<String> nodeList=getNodeChildren(nettyParentNode);
        if(nodeList!=null && nodeList.size()!=0){
            logger.info("nodeList size:"+nodeList.size());
            List<ZkBalanceNode> tempNodes=new ArrayList<>(nodeList.size());
            for(String nodePath: nodeList){
                nodePath=nettyParentNode+"/"+nodePath;
                logger.info("计算path:"+nodePath+"的负载。。。");
                byte[] nodepayloadBytes=getNodePayload(nodePath);
                ZkPayloadNode nodePayload= ProtostuffUtil.deserializer(nodepayloadBytes,ZkPayloadNode.class);
                ZkBalanceNode balanceNode=new ZkBalanceNode();
                balanceNode.setNodeName(nodePath);
                //comNum表示当前这个节点的连接数，payload里的factor表示启动时设置该节点的连接系数，
                // 机器性能好可以设置更大的值，将分配更多的客户端连接
                long conNum=nodePayload.getConNum();
                double factor=conNum/nodePayload.getFactor();
                logger.info("znode:"+nodePath+",conNum:"+nodePayload.getConNum());
                logger.info("znode:"+nodePath+",factor:"+factor);
                balanceNode.setFactor(factor);
                tempNodes.add(balanceNode);
                Collections.sort(tempNodes);
                for(ZkBalanceNode zkBalanceNode:tempNodes){
                    logger.info(zkBalanceNode.getNodeName());
                }
                this.zkBalanceNodes=tempNodes;
            }
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        client= ZookeeperClientFactory.createFramework(address);
        client.start();
        //定时对znode重新计算负载
        ScheduledThreadPoolExecutor schedule=new ScheduledThreadPoolExecutor(1);
        schedule.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                doZnodeBalance();
            }
        },1000,10000, TimeUnit.MILLISECONDS);

        //为节点增加监听器，当有新节点连接的时候，重新计算各节点负载
        PathChildrenCacheListener listener=new PathChildrenCacheListener() {
            @Override
            public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
                switch (pathChildrenCacheEvent.getType()){
                    case CHILD_ADDED:
                        //处理节点新增事件，重新计算各节点负载均衡
                        doZnodeBalance();
                        break;
                    default:
                        break;
                }
            }
        };
        createNettyChildrenNodeListener(nettyParentNode,listener);
    }
}
