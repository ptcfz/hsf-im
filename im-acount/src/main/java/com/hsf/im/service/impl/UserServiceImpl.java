package com.hsf.im.service.impl;

import com.hsf.im.Exception.BizException;
import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.dao.UserMapper;
import com.hsf.im.domain.Role;
import com.hsf.im.domain.User;
import com.hsf.im.domain.UserRole;
import com.hsf.im.service.RoleService;
import com.hsf.im.service.UserRoleService;
import com.hsf.im.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    Logger logger= LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRoleService userRoleService;


    @Override
    public void addUser(User user){
        userMapper.insert(user);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public void register(User user){
        if(user!=null){
            Role role=roleService.findByRoleName("ROLE_USER");
            if(role==null){
                throw new BizException(SystemExceptionEnum.ROLE_NOT_FOUND.getCode()
                        ,SystemExceptionEnum.ROLE_NOT_FOUND.getMsg());
            }
            userMapper.insert(user);
            UserRole userRole=new UserRole();
            userRole.setRoleId(role.getId());
            userRole.setUserId(user.getId());
            userRoleService.addUserRole(userRole);
        }
    }

    @Override
    public User findUserByUsername(String username){
        logger.info("findUserByUsername:"+username);
        return userMapper.loadByUserName(username);
    }

    @Override
    public User findUserById(Long userId) {
        return userMapper.findUserById(userId);
    }
}
