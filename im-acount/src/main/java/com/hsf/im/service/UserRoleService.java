package com.hsf.im.service;

import com.hsf.im.domain.UserRole;

/**
 * @Author: chenfz
 */
public interface UserRoleService {

    public void addUserRole(UserRole userRole);
}
