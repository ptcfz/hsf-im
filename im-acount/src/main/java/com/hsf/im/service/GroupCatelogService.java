package com.hsf.im.service;

import com.hsf.im.domain.GroupCatelog;

import java.util.List;

/**
 * @Author: chenfz
 */
public interface GroupCatelogService {

    public List<GroupCatelog> getAllGroupCatelog();

}
