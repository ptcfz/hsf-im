package com.hsf.im.service;

import com.hsf.im.domain.User;

public interface UserService {

    public void addUser(User user);

    public void register(User user);

    public User findUserByUsername(String username);

    public User findUserById(Long userId);
}
