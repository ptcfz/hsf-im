package com.hsf.im.service.impl;

import com.hsf.im.dao.RoleMapper;
import com.hsf.im.domain.Role;
import com.hsf.im.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: chenfz
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public void addRole(Role role) {
        roleMapper.insert(role);
    }

    @Override
    public Role findByRoleName(String roleName){
        return roleMapper.findByRoleName(roleName);
    }
}
