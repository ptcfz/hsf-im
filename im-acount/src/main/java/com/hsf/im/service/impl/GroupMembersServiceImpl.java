package com.hsf.im.service.impl;

import com.hsf.im.dao.GroupMemberMapper;
import com.hsf.im.domain.Group;
import com.hsf.im.domain.GroupMember;
import com.hsf.im.service.GroupMembersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: chenfz
 */
@Service
public class GroupMembersServiceImpl implements GroupMembersService {

    @Autowired
    private GroupMemberMapper groupMemberMapper;

    @Override
    public List<Group> findUserJoinGroups(long userId,int page) {
        int offset=page*10;
        int size=10;
        return groupMemberMapper.findUserJoinGroups(userId,offset,size);
    }

    @Override
    public void userQuitGroup(long userId, long groupId) {
        groupMemberMapper.userQuitGroup(userId,groupId);
    }

    @Override
    public void addGroupMember(GroupMember groupMember) {
        groupMemberMapper.insert(groupMember);
    }
}
