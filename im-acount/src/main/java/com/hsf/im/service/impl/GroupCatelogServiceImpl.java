package com.hsf.im.service.impl;

import com.hsf.im.dao.GroupCatelogMapper;
import com.hsf.im.domain.GroupCatelog;
import com.hsf.im.service.GroupCatelogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: chenfz
 */
@Service
public class GroupCatelogServiceImpl implements GroupCatelogService {

    @Autowired
    private GroupCatelogMapper groupCatelogMapper;

    @Override
    public List<GroupCatelog> getAllGroupCatelog() {
        return groupCatelogMapper.getAllGroupCatelog();
    }
}
