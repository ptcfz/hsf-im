package com.hsf.im.service;

import com.hsf.im.domain.Group;
import com.hsf.im.domain.GroupMember;

import java.util.List;

/**
 * @Author: chenfz
 */
public interface GroupMembersService {

    /**
     * 查找用户加入的群
     * @param userId
     * @return
     */
    public List<Group> findUserJoinGroups(long userId,int page);

    /**
     * 用户退群
     * @param userId
     * @param groupId
     */
    public void userQuitGroup(long userId,long groupId);

    /**
     * 添加群成员
     * @param groupMember
     */
    public void addGroupMember(GroupMember groupMember);
}
