package com.hsf.im.service;


import com.hsf.im.dao.RoleMapper;
import com.hsf.im.dao.UserMapper;
import com.hsf.im.domain.Role;
import com.hsf.im.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("myUserDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user=userMapper.loadByUserName(userName);
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        if(user!=null){
            List<Role> roles = roleMapper.findByUserId(user.getId());
            if(roles!= null){
                for (Role role : roles) {
                    if(role != null && role.getName()!= null){
                        GrantedAuthority grantedAuthority=new SimpleGrantedAuthority(role.getName());
                        grantedAuthorities.add(grantedAuthority);
                    }
                }
            }
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),grantedAuthorities);
    }
}
