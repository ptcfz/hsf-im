package com.hsf.im.service;

import com.hsf.im.domain.Group;
import com.hsf.im.domain.User;

/**
 * @Author: chenfz
 */
public interface GroupService {

    public void updateGroup(Group group);

    public void createGroup(Group group, User user);
}
