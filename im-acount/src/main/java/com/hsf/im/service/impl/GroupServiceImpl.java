package com.hsf.im.service.impl;

import com.hsf.im.dao.GroupMapper;
import com.hsf.im.domain.Group;
import com.hsf.im.domain.GroupMember;
import com.hsf.im.domain.User;
import com.hsf.im.service.GroupMembersService;
import com.hsf.im.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @Author: chenfz
 */
@Service
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupMapper groupMapper;

    @Autowired
    private GroupMembersService groupMembersService;

    @Override
    public void updateGroup(Group group) {
        groupMapper.updateByPrimaryKey(group);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createGroup(Group group, User user) {
        groupMapper.insert(group);
        GroupMember groupMember=new GroupMember();
        groupMember.setGroupId(group.getId());
        groupMember.setUserId(user.getId());
        groupMember.setApproveUserId(user.getId());//创建群组，则批准入群者为自己
        groupMember.setApplyTime(new Date());
        groupMember.setJoinTime(new Date());
        groupMember.setLastAckMsgId(0L);
        groupMembersService.addGroupMember(groupMember);
    }
}
