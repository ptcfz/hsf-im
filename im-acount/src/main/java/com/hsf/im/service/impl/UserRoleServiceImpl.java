package com.hsf.im.service.impl;

import com.hsf.im.dao.UserRoleMapper;
import com.hsf.im.domain.UserRole;
import com.hsf.im.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: chenfz
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public void addUserRole(UserRole userRole) {
        userRoleMapper.insert(userRole);
    }
}
