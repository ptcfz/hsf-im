package com.hsf.im.service;

import com.hsf.im.domain.Role;

/**
 * @Author: chenfz
 */
public interface RoleService {

    public void addRole(Role role);

    public Role findByRoleName(String roleName);
}
