package com.hsf.im.mq.consumer;

import com.hsf.im.model.RocketMqConst;
import com.hsf.im.model.SingleMsgStoreModel;
import com.hsf.im.msg.MsgTypeEnum;
import com.hsf.im.service.MongoMsgStoreService;
import com.hsf.im.utils.ProtostuffUtil;
import org.apache.rocketmq.client.consumer.*;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: chenfz
 */
@Component
public class NewSingleMsgConsumer implements InitializingBean {

    Logger logger= LoggerFactory.getLogger(NewSingleMsgConsumer.class);

    private MQPullConsumerScheduleService scheduleService;

    @Value("${mq.rocketmq.nameSrvAddr}")
    private String rocketMqNameSrvAddr;

    @Autowired
    private MongoMsgStoreService msgStoreService;


    private void dealMsg(List<MessageExt> msgList){
        //将消息保存至db
        if(msgList!= null && msgList.size()!=0){
            logger.info("接收到消息："+msgList.size());
            for(MessageExt msg: msgList){
                SingleMsgStoreModel singleMsgStoreModel = ProtostuffUtil.deserializer(msg.getBody(), SingleMsgStoreModel.class);
                if(singleMsgStoreModel.getType()== MsgTypeEnum.SINGLE_MSG_REQUEST.getCode()){
                    msgStoreService.saveSingleMsg(singleMsgStoreModel);
                }else if(singleMsgStoreModel.getType()==MsgTypeEnum.SINGLE_MSG_RESPONSE.getCode()){
                    msgStoreService.ackSingleMsgById(singleMsgStoreModel);
                }
            }
        }
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        scheduleService=new MQPullConsumerScheduleService(RocketMqConst.SINGLE_MSG_CONSUMER_GROUP);
        scheduleService.getDefaultMQPullConsumer().setNamesrvAddr(rocketMqNameSrvAddr);
        scheduleService.setMessageModel(MessageModel.CLUSTERING);
        scheduleService.registerPullTaskCallback(RocketMqConst.SINGLE_MSG_TOPIC, new PullTaskCallback() {
            @Override
            public void doPullTask(MessageQueue mq, PullTaskContext context) {
                MQPullConsumer consumer = context.getPullConsumer();
                try {
                    //获取从哪里开始拉取
                    long offset = consumer.fetchConsumeOffset(mq, false);
                    if(offset < 0) {
                        offset = 0;
                    }
                    PullResult pullResult = consumer.pull(mq, "*", offset, 32);
                    switch (pullResult.getPullStatus()) {
                        case FOUND:
                            List<MessageExt> list = pullResult.getMsgFoundList();
                            dealMsg(list);
                            break;
                        case NO_MATCHED_MSG:
                            break;
                        case NO_NEW_MSG:
                        case OFFSET_ILLEGAL:
                            break;
                        default:
                            break;
                    }
                    //存储offset，客户端每隔5s会定时刷新到broker
                    consumer.updateConsumeOffset(mq, pullResult.getNextBeginOffset());
                    //重新拉取 建议超过5s这样就不会重复获取
                    context.setPullNextDelayTimeMillis(8000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        scheduleService.start();
    }
}
