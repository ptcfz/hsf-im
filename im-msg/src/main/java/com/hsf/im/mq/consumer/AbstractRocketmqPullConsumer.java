package com.hsf.im.mq.consumer;

import com.hsf.im.utils.RunTimeUtil;
import org.apache.rocketmq.client.consumer.DefaultMQPullConsumer;
import org.apache.rocketmq.client.consumer.MessageQueueListener;
import org.apache.rocketmq.client.consumer.PullResult;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

public abstract class AbstractRocketmqPullConsumer {

    Logger logger= LoggerFactory.getLogger(AbstractRocketmqPullConsumer.class);

    public void listener(String namesrvAddr,String topic,String tag,String groupName) throws MQClientException {
        DefaultMQPullConsumer consumer=new DefaultMQPullConsumer(groupName);
        consumer.setNamesrvAddr(namesrvAddr);
        consumer.setInstanceName(RunTimeUtil.getRocketMqUniqeInstanceName());
        consumer.setMessageModel(MessageModel.CLUSTERING);
        consumer.registerMessageQueueListener(topic, new MessageQueueListener() {
            @Override
            public void messageQueueChanged(String topic, Set<MessageQueue> mqAll, Set<MessageQueue> mqDivided) {
                for(MessageQueue queue : mqAll){
                    logger.info("all broker name:"+queue.getBrokerName()+",queueId:"+queue.getQueueId());
                }
                for(MessageQueue queue : mqDivided){
                    logger.info("divided broker name:"+queue.getBrokerName()+",queueId:"+queue.getQueueId());

                }
            }
        });
        consumer.start();
        while(true){
            Set<MessageQueue> mqs=consumer.fetchSubscribeMessageQueues(topic);
            for(MessageQueue mq: mqs) {
                long offset = consumer.fetchConsumeOffset(mq, true);
                try {
                    Long localOffset = getOffset(mq);
                    if (localOffset == null) {
                        localOffset = offset;
                    }
                    PullResult pullResult =consumer.pull(mq,"*",localOffset,10);
                    //PullResult pullResult = consumer.pullBlockIfNotFound(mq, null, localOffset, 32);
                    switch (pullResult.getPullStatus()) {
                        case FOUND:
                            List<MessageExt> msgList = pullResult.getMsgFoundList();
                            dealMsg(msgList);
                            consumer.updateConsumeOffset(mq,pullResult.getNextBeginOffset());
                            updateOffset(mq, pullResult.getNextBeginOffset());
                            break;
                        case NO_MATCHED_MSG:
                            break;
                        case NO_NEW_MSG:
                        case OFFSET_ILLEGAL:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    logger.error("consumer error:" + e.getMessage(), e);
                }
            }
        }


    }

    public abstract void dealMsg(List<MessageExt> msgList);

    public abstract void updateOffset(MessageQueue queue,Long offset);

    public abstract Long getOffset(MessageQueue queue);
}
