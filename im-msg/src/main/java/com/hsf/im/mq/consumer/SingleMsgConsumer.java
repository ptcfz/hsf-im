package com.hsf.im.mq.consumer;

import com.hsf.im.model.RocketMqConst;
import com.hsf.im.model.SingleMsgStoreModel;
import com.hsf.im.msg.MsgTypeEnum;
import com.hsf.im.service.MongoMsgStoreService;
import com.hsf.im.service.RedisService;
import com.hsf.im.utils.ProtostuffUtil;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.common.message.MessageQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: chenfz
 */

public class SingleMsgConsumer extends AbstractRocketmqPullConsumer implements InitializingBean {

    Logger logger= LoggerFactory.getLogger(SingleMsgConsumer.class);

    @Value("${mq.rocketmq.nameSrvAddr}")
    private String rocketMqNameSrvAddr;

    @Autowired
    private RedisService redisService;

    @Autowired
    private MongoMsgStoreService msgStoreService;

    @Override
    public void afterPropertiesSet() throws Exception {
        ExecutorService executorService= Executors.newFixedThreadPool(1);
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                try{
                    logger.debug("rocketMqNameSrvAddr:"+rocketMqNameSrvAddr+",topic:"+RocketMqConst.SINGLE_MSG_TOPIC+
                            ",tag:"+RocketMqConst.SINGLE_MSG_TAG+",consumer group:"+RocketMqConst.SINGLE_MSG_CONSUMER_GROUP);
                    listener(rocketMqNameSrvAddr,RocketMqConst.SINGLE_MSG_TOPIC,RocketMqConst.SINGLE_MSG_TAG,RocketMqConst.SINGLE_MSG_CONSUMER_GROUP);
                }catch (MQClientException e){
                    logger.error("创建SingleMsgConsumer异常:"+e.getErrorMessage(),e);
                }catch (Exception e){
                    logger.error(e.getMessage(),e);
                }
            }
        });
    }

    @Override
    public void dealMsg(List<MessageExt> msgList) {
        //将消息保存至db
        if(msgList!= null && msgList.size()!=0){
            for(MessageExt msg: msgList){
                SingleMsgStoreModel singleMsgStoreModel = ProtostuffUtil.deserializer(msg.getBody(), SingleMsgStoreModel.class);
                if(singleMsgStoreModel.getType()== MsgTypeEnum.SINGLE_MSG_REQUEST.getCode()){
                    msgStoreService.saveSingleMsg(singleMsgStoreModel);
                }else if(singleMsgStoreModel.getType()==MsgTypeEnum.SINGLE_MSG_RESPONSE.getCode()){
                    msgStoreService.ackSingleMsgById(singleMsgStoreModel);
                }
            }
        }
    }

    @Override
    public void updateOffset(MessageQueue queue, Long offset) {
        redisService.set(queue.getTopic()+"_"+queue.getQueueId(),offset+"");
    }

    @Override
    public Long getOffset(MessageQueue queue) {
        String tempOffset=(String)redisService.get(queue.getTopic()+"_"+queue.getQueueId());
        if(!StringUtils.isEmpty(tempOffset)){
            return Long.parseLong(tempOffset);
        }
        return null;
    }
}
