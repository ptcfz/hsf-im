package com.hsf.im.dao;

import com.hsf.im.domain.SingleMsg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SingleMsgMapper {

    int deleteByPrimaryKey(Long msgServerId);

    int insert(SingleMsg record);

    int insertSelective(SingleMsg record);

    SingleMsg selectByPrimaryKey(Long msgServerId);

    int updateByPrimaryKeySelective(SingleMsg record);

    int updateByPrimaryKey(SingleMsg record);

    List<SingleMsg> loadUserOfflineMsg(Long userId);

    void batchAckSingleMsgByLastMsgId(@Param("userId") Long userId, @Param("lastMsgServerId") Long lastMsgServerId);

    void ackSingleMsgByMsgServerId(@Param("toId") long toId, @Param("msgServerId") long msgServerId);
}