package com.hsf.im.service;

import com.hsf.im.domain.SingleMsg;
import com.hsf.im.model.SingleMsgStoreModel;

import java.util.List;

/**
 * @Author: chenfz
 */
public interface MysqlMsgStoreService {

    public void saveSingleMsg(SingleMsg singleMsg);

    public void deleteSingleMsg(long userId,SingleMsg singleMsg);

    public long updateSingleMsgById(SingleMsg singleMsg);

    public void batchAckSingleMsgByLastMsgId(Long userId, Long lastMsgServerId);

    public List<SingleMsg> findSingleMsg(SingleMsg singleMsg);

    public List<SingleMsg> loadUserOffLineMsg(Long userId);

}
