package com.hsf.im.service.impl;

import com.hsf.im.model.SingleMsgStoreModel;
import com.hsf.im.msg.MsgStatusEnum;
import com.hsf.im.service.MongoMsgStoreService;
import com.mongodb.client.result.UpdateResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: chenfz
 */
@Service
public class MongoMsgStoreServiceImpl implements MongoMsgStoreService {

    Logger logger= LoggerFactory.getLogger(MongoMsgStoreServiceImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    private ReentrantLock lock=new ReentrantLock();

    @Override
    public void saveSingleMsg(SingleMsgStoreModel singleMsgStoreModel) {
        logger.debug("mongodb 保存 from:"+singleMsgStoreModel.getFrom()+"to:"+singleMsgStoreModel.getTo()+"的消息，msgId:"
                +singleMsgStoreModel.getMsgServerId()+",content:"+singleMsgStoreModel.getContent());
        lock.lock();
        try{
            mongoTemplate.save(singleMsgStoreModel);
        }finally {
            lock.unlock();
        }
    }

    @Override
    public void deleteSingleMsg(SingleMsgStoreModel singleMsgStoreModel) {
        mongoTemplate.remove(singleMsgStoreModel);
    }

    @Override
    public long updateSingleMsgById(SingleMsgStoreModel singleMsgStoreModel) {
        Query query = new Query(Criteria.where("_id").is(singleMsgStoreModel.getMsgServerId()));
        Update update = new Update();
        if(!StringUtils.isEmpty(singleMsgStoreModel.getContent())){
            update.set("content",singleMsgStoreModel.getContent());
        }
        if(singleMsgStoreModel.getTime()!=null){
            update.set("time",singleMsgStoreModel.getTime());
        }
        UpdateResult result=mongoTemplate.updateFirst(query,update,SingleMsgStoreModel.class);
        return result.getModifiedCount();
    }

    @Override
    public long ackSingleMsgById(SingleMsgStoreModel singleMsgStoreModel) {
        Query query = new Query(Criteria.where("_id").is(singleMsgStoreModel.getMsgServerId()));
        Update update = new Update();
        update.set("status", MsgStatusEnum.SERVER_ACK.getCode());
        UpdateResult result=mongoTemplate.updateFirst(query,update,SingleMsgStoreModel.class);
        logger.info("ack msg:"+singleMsgStoreModel.getMsgServerId()+",to status:"+MsgStatusEnum.SERVER_ACK.getCode()
                +",update count:"+result.getModifiedCount());

        return result.getModifiedCount();
    }

    @Override
    public List<SingleMsgStoreModel> findSingleMsg(SingleMsgStoreModel singleMsgStoreModel) {
        if(singleMsgStoreModel==null){
            return null;
        }
        Criteria criteria=new Criteria();
        if(!StringUtils.isEmpty(singleMsgStoreModel.getFrom())){
            criteria=criteria.and("from").is(singleMsgStoreModel.getFrom());
        }
        if(!StringUtils.isEmpty(singleMsgStoreModel.getTo())){
            criteria=criteria.and("to").is(singleMsgStoreModel.getTo());
        }
        if(StringUtils.isEmpty(singleMsgStoreModel.getType())){
            criteria=criteria.and("type").is(singleMsgStoreModel.getType());
        }
        Query query=new Query(criteria);

        List<SingleMsgStoreModel> singleMsgStoreModelList=mongoTemplate.find(query,SingleMsgStoreModel.class);
        return singleMsgStoreModelList;
    }

    @Override
    public List<SingleMsgStoreModel> loadUserOffLineMsg(String username){
        if(StringUtils.isEmpty(username)){
            return null;
        }
        lock.lock();
        try{
            Criteria criteria=new Criteria();
            criteria=criteria.and("to").is(username);
            criteria.and("status").is(MsgStatusEnum.SERVER_RECEIVE.getCode());
            Query query=new Query(criteria);
            List<SingleMsgStoreModel> singleMsgStoreModelList=mongoTemplate.find(query,SingleMsgStoreModel.class);

            //查出消息之后，更新消息的状态为SERVER_ACK
            Update update=new Update();
            update.set("status",MsgStatusEnum.SERVER_ACK.getCode());
            mongoTemplate.updateMulti(query,update,SingleMsgStoreModel.class);
            return singleMsgStoreModelList;
        }finally {
            lock.unlock();
        }
    }
}
