package com.hsf.im.service;

import com.hsf.im.model.SingleMsgStoreModel;

import java.util.List;

/**
 * @Author: chenfz
 */
public interface MongoMsgStoreService {

    public void saveSingleMsg(SingleMsgStoreModel singleMsgStoreModel);

    public void deleteSingleMsg(SingleMsgStoreModel singleMsgStoreModel);

    public long updateSingleMsgById(SingleMsgStoreModel singleMsgStoreModel);

    public long ackSingleMsgById(SingleMsgStoreModel singleMsgStoreModel);

    public List<SingleMsgStoreModel> findSingleMsg(SingleMsgStoreModel singleMsgStoreModel);

    public List<SingleMsgStoreModel> loadUserOffLineMsg(String username);
}
