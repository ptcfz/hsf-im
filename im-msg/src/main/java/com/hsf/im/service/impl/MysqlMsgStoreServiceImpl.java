package com.hsf.im.service.impl;

import com.hsf.im.dao.SingleMsgMapper;
import com.hsf.im.domain.SingleMsg;
import com.hsf.im.model.SingleMsgStoreModel;
import com.hsf.im.service.MongoMsgStoreService;
import com.hsf.im.service.MysqlMsgStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: chenfz
 */
@Service
public class MysqlMsgStoreServiceImpl implements MysqlMsgStoreService {

    @Autowired
    private SingleMsgMapper singleMsgMapper;

    @Override
    public void saveSingleMsg(SingleMsg singleMsg) {
        singleMsgMapper.insert(singleMsg);
    }

    @Override
    public void deleteSingleMsg(long userId,SingleMsg singleMsg) {

    }

    @Override
    public long updateSingleMsgById(SingleMsg singleMsg) {
        return 0;
    }

    @Override
    public void batchAckSingleMsgByLastMsgId(Long userId, Long lastMsgServerId) {
        singleMsgMapper.batchAckSingleMsgByLastMsgId(userId,lastMsgServerId);
    }

    @Override
    public List<SingleMsg> findSingleMsg(SingleMsg singleMsg) {
        return null;
    }

    @Override
    public List<SingleMsg> loadUserOffLineMsg(Long userId) {
        return singleMsgMapper.loadUserOfflineMsg(userId);
    }
}
