package com.hsf.im;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImMsgApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImMsgApplication.class, args);
    }

}
