package com.hsf.im.controller;

import com.hsf.im.domain.SingleMsg;
import com.hsf.im.model.SingleMsgStoreModel;
import com.hsf.im.service.MongoMsgStoreService;
import com.hsf.im.service.MysqlMsgStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: chenfz
 */
@RestController
public class MsgController {

    @Autowired
    private MongoMsgStoreService mongoMsgStoreService;

    @Autowired
    private MysqlMsgStoreService mysqlMsgStoreService;

    @RequestMapping("/loadUserOffLineMsg")
    public List<SingleMsgStoreModel> loadUserOffLineMsg(String username){
        return mongoMsgStoreService.loadUserOffLineMsg(username);
    }

    @RequestMapping("/loadUserOffLineMsgFromMysql")
    public List<SingleMsg> loadUserOffLineMsgFromMysql(@RequestParam("userId") Long userId){
        return mysqlMsgStoreService.loadUserOffLineMsg(userId);
    }

    @RequestMapping("/batchAckMsgByLastMsgServerId")
    public void batchAckMsgByLastMsgServerId(@RequestParam("userId") Long userId, @RequestParam("lastMsgServerId") Long lastMsgServerId){
        mysqlMsgStoreService.batchAckSingleMsgByLastMsgId(userId,lastMsgServerId);
    }
}
