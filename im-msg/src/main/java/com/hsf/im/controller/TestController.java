package com.hsf.im.controller;

import com.hsf.im.model.SingleMsgStoreModel;
import com.hsf.im.service.MongoMsgStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: chenfz
 */
@RestController
public class TestController {

    @Autowired
    private MongoMsgStoreService mongoMsgStoreService;

    @RequestMapping("/saveObj")
    public void saveObj(@RequestBody SingleMsgStoreModel singleMsgStoreModel){
        mongoMsgStoreService.saveSingleMsg(singleMsgStoreModel);
    }

    @RequestMapping("/updateMsg")
    public long updateMsg(@RequestBody SingleMsgStoreModel singleMsgStoreModel){
        return mongoMsgStoreService.updateSingleMsgById(singleMsgStoreModel);
    }

    @RequestMapping("/findSingleMsg")
    public List<SingleMsgStoreModel> findSingleMsg(@RequestBody SingleMsgStoreModel singleMsgStoreModel){
        return mongoMsgStoreService.findSingleMsg(singleMsgStoreModel);
    }
}
