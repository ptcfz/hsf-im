# hsf-im

#### 介绍
基于netty、zookeeper、mongodb、redis等开发的一个命令行聊天工具

#### 软件架构
im-acount    用户账户模块,
im-server    netty server node模块，用户即时消息将发送到此节点上，路由到远端客户端上,
im-msg       处理消息模块,
im-client    一个简单的客户端模块,

![输入图片说明](https://images.gitee.com/uploads/images/2020/0329/224056_f954364b_5110766.png "1585492767(1).png")


#### 安装教程

1.  依赖环境：mysql，zookeeper集群，redis集群，mongodb副本集(replicaSet=hsfIm)，rocket mq集群，客户端使用sqlite
2.  修改各模块配置文件内mysql，zookeeper，redis，mongodb，rocketmq地址
3.  修改im-client模块配置文件内im-client.sqlite文件的地址
4.  使用hsf_im.sql文件恢复数据库

#### 使用说明

1.  正确配置mysql\zookeeper\redis\mongodb\rocket mq\sqlite等信息
2.  依次启动im-acount，im-id-generator,im-msg,im-server(一个或多个都可以),im-client


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
