-- MySQL dump 10.13  Distrib 5.7.26, for el7 (x86_64)
--
-- Host: localhost    Database: hsf_im
-- ------------------------------------------------------
-- Server version	5.7.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `hsf_im`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `hsf_im` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `hsf_im`;

--
-- Table structure for table `WORKER_NODE`
--

DROP TABLE IF EXISTS `WORKER_NODE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `WORKER_NODE` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'auto increment id',
  `HOST_NAME` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'host name',
  `PORT` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'port',
  `TYPE` int(11) NOT NULL COMMENT 'node type: ACTUAL or CONTAINER',
  `LAUNCH_DATE` date NOT NULL COMMENT 'launch date',
  `MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'modified time',
  `CREATED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'created time',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=437 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='DB WorkerID Assigner for UID Generator';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `WORKER_NODE`
--

LOCK TABLES `WORKER_NODE` WRITE;
/*!40000 ALTER TABLE `WORKER_NODE` DISABLE KEYS */;
INSERT INTO `WORKER_NODE` VALUES (417,'192.168.122.1','1595327702278-8774',2,'2020-07-21','2020-07-21 10:35:02','2020-07-21 10:35:02'),(418,'192.168.122.1','1595328944660-24161',2,'2020-07-21','2020-07-21 10:55:44','2020-07-21 10:55:44'),(419,'192.168.122.1','1595386464217-68980',2,'2020-07-22','2020-07-22 02:54:24','2020-07-22 02:54:24'),(420,'192.168.122.1','1595388173419-26981',2,'2020-07-22','2020-07-22 03:22:53','2020-07-22 03:22:53'),(421,'192.168.122.1','1595388242761-58108',2,'2020-07-22','2020-07-22 03:24:02','2020-07-22 03:24:02'),(422,'192.168.4.79','1595389215480-72827',2,'2020-07-22','2020-07-22 03:40:15','2020-07-22 03:40:15'),(423,'192.168.4.79','1595389583712-73402',2,'2020-07-22','2020-07-22 03:46:23','2020-07-22 03:46:23'),(424,'192.168.4.79','1595389815423-2062',2,'2020-07-22','2020-07-22 03:50:15','2020-07-22 03:50:15'),(425,'192.168.122.1','1595390207233-25615',2,'2020-07-22','2020-07-22 03:56:47','2020-07-22 03:56:47'),(426,'192.168.122.1','1595398883507-90119',2,'2020-07-22','2020-07-22 06:21:23','2020-07-22 06:21:23'),(427,'192.168.122.1','1595496498880-66451',2,'2020-07-23','2020-07-23 09:28:18','2020-07-23 09:28:18'),(428,'192.168.122.1','1595498186662-19397',2,'2020-07-23','2020-07-23 09:56:26','2020-07-23 09:56:26'),(429,'192.168.122.1','1595498225204-2031',2,'2020-07-23','2020-07-23 09:57:05','2020-07-23 09:57:05'),(430,'192.168.122.1','1595502495262-96137',2,'2020-07-23','2020-07-23 11:08:15','2020-07-23 11:08:15'),(431,'192.168.122.1','1595513133184-96839',2,'2020-07-23','2020-07-23 14:05:33','2020-07-23 14:05:33'),(432,'192.168.122.1','1595516400048-64728',2,'2020-07-23','2020-07-23 15:00:00','2020-07-23 15:00:00'),(433,'192.168.122.1','1595520629412-48604',2,'2020-07-24','2020-07-23 16:10:29','2020-07-23 16:10:29'),(434,'192.168.4.90','1595559949577-73387',2,'2020-07-24','2020-07-24 11:05:47','2020-07-24 11:05:47'),(435,'192.168.122.1','1595589265884-22371',2,'2020-07-24','2020-07-24 11:14:25','2020-07-24 11:14:25'),(436,'192.168.122.1','1595624026063-71766',2,'2020-07-25','2020-07-24 20:53:45','2020-07-24 20:53:45');
/*!40000 ALTER TABLE `WORKER_NODE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientdetails`
--

DROP TABLE IF EXISTS `clientdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientdetails` (
  `appId` varchar(128) NOT NULL,
  `resourceIds` varchar(256) DEFAULT NULL,
  `appSecret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `grantTypes` varchar(256) DEFAULT NULL,
  `redirectUrl` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additionalInformation` varchar(4096) DEFAULT NULL,
  `autoApproveScope` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`appId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientdetails`
--

LOCK TABLES `clientdetails` WRITE;
/*!40000 ALTER TABLE `clientdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `clientdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_token`
--

DROP TABLE IF EXISTS `oauth_access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(128) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `authentication` blob,
  `refresh_token` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_token`
--

LOCK TABLES `oauth_access_token` WRITE;
/*!40000 ALTER TABLE `oauth_access_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_approvals`
--

DROP TABLE IF EXISTS `oauth_approvals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_approvals` (
  `userId` varchar(256) DEFAULT NULL,
  `clientId` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `expiresAt` datetime DEFAULT NULL,
  `lastModifiedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_approvals`
--

LOCK TABLES `oauth_approvals` WRITE;
/*!40000 ALTER TABLE `oauth_approvals` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_approvals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_client_details`
--

DROP TABLE IF EXISTS `oauth_client_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_client_details` (
  `client_id` varchar(256) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `client_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `authorized_grant_types` varchar(256) DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_client_details`
--

LOCK TABLES `oauth_client_details` WRITE;
/*!40000 ALTER TABLE `oauth_client_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_client_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_client_token`
--

DROP TABLE IF EXISTS `oauth_client_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_client_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(128) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_client_token`
--

LOCK TABLES `oauth_client_token` WRITE;
/*!40000 ALTER TABLE `oauth_client_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_client_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_code`
--

DROP TABLE IF EXISTS `oauth_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_code` (
  `code` varchar(256) DEFAULT NULL,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_code`
--

LOCK TABLES `oauth_code` WRITE;
/*!40000 ALTER TABLE `oauth_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_token`
--

DROP TABLE IF EXISTS `oauth_refresh_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_token`
--

LOCK TABLES `oauth_refresh_token` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'ROLE_USER'),(2,'ROLE_ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_group`
--

DROP TABLE IF EXISTS `t_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '群组主键',
  `group_name` varchar(100) NOT NULL COMMENT '群组名称',
  `create_user_id` bigint(20) NOT NULL COMMENT '创建者',
  `catelog_top_id` int(11) NOT NULL COMMENT '一级分类id',
  `catelog_top_name` varchar(50) NOT NULL COMMENT '一级分类名称',
  `catelog_second_id` int(11) NOT NULL COMMENT '二级分类id',
  `catelog_second_name` varchar(50) NOT NULL COMMENT '二级分类名称',
  `need_apply` tinyint(4) NOT NULL COMMENT '入群是否需要申请',
  `create_time` date NOT NULL COMMENT '群创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_create_user_id` (`create_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_group`
--

LOCK TABLES `t_group` WRITE;
/*!40000 ALTER TABLE `t_group` DISABLE KEYS */;
INSERT INTO `t_group` VALUES (1,'福建八闽游',1,1,'休闲',1,'旅行',1,'2020-07-22');
/*!40000 ALTER TABLE `t_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_group_apply_msg`
--

DROP TABLE IF EXISTS `t_group_apply_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_group_apply_msg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_id` bigint(20) NOT NULL COMMENT '群id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `apply_msg` varchar(50) DEFAULT NULL COMMENT '申请信息',
  `apply_status` tinyint(4) NOT NULL COMMENT '申请状态',
  `apply_time` date NOT NULL COMMENT '申请时间',
  `update_time` date NOT NULL COMMENT '状态修改时间',
  PRIMARY KEY (`id`),
  KEY `idx_group_id_apply_status_user_id_apply_msg` (`group_id`,`apply_status`,`user_id`,`apply_msg`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_group_apply_msg`
--

LOCK TABLES `t_group_apply_msg` WRITE;
/*!40000 ALTER TABLE `t_group_apply_msg` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_group_apply_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_group_catelog`
--

DROP TABLE IF EXISTS `t_group_catelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_group_catelog` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `catelog_name` varchar(50) NOT NULL COMMENT '类目名称',
  `level` tinyint(4) NOT NULL COMMENT '类目级别',
  `parent_catelog_id` int(11) NOT NULL COMMENT '父类目id,top级别的父类目id为0',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_group_catelog`
--

LOCK TABLES `t_group_catelog` WRITE;
/*!40000 ALTER TABLE `t_group_catelog` DISABLE KEYS */;
INSERT INTO `t_group_catelog` VALUES (1,'休闲',1,0,'2020-07-22 13:48:08'),(2,'旅行',2,1,'2020-07-22 13:48:09'),(3,'旅行',2,1,'2020-07-22 13:48:09');
/*!40000 ALTER TABLE `t_group_catelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_group_members`
--

DROP TABLE IF EXISTS `t_group_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_group_members` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_id` bigint(20) NOT NULL COMMENT '群id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `approve_user_id` bigint(20) NOT NULL COMMENT '同意入群审批者',
  `last_ack_msg_id` bigint(20) NOT NULL COMMENT '用户此群接收的最后一条消息',
  `apply_time` date NOT NULL COMMENT '申请入群时间',
  `join_time` date NOT NULL COMMENT '入群时间',
  PRIMARY KEY (`id`),
  KEY `idx_group_id` (`group_id`),
  KEY `idx_user_id` (`user_id`,`last_ack_msg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_group_members`
--

LOCK TABLES `t_group_members` WRITE;
/*!40000 ALTER TABLE `t_group_members` DISABLE KEYS */;
INSERT INTO `t_group_members` VALUES (1,1,1,1,0,'2020-07-22','2020-07-22'),(2,1,2,1,0,'2020-07-24','2020-07-24');
/*!40000 ALTER TABLE `t_group_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_group_msg`
--

DROP TABLE IF EXISTS `t_group_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_group_msg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `group_id` bigint(20) NOT NULL COMMENT '群id',
  `sender_id` bigint(20) NOT NULL COMMENT '发送者id',
  `content` varchar(500) NOT NULL COMMENT '消息内容',
  `msg_type` tinyint(4) NOT NULL COMMENT '消息类型',
  `send_time` date NOT NULL COMMENT '发送时间',
  PRIMARY KEY (`id`),
  KEY `idx_group_id_send_time` (`group_id`,`send_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_group_msg`
--

LOCK TABLES `t_group_msg` WRITE;
/*!40000 ALTER TABLE `t_group_msg` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_group_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_group_quit_msg`
--

DROP TABLE IF EXISTS `t_group_quit_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_group_quit_msg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户退群提示消息',
  `group_master_user_id` bigint(20) NOT NULL COMMENT '群主id',
  `group_master_user_name` varchar(100) NOT NULL COMMENT '群主名称',
  `group_id` bigint(20) NOT NULL COMMENT '群id',
  `group_name` varchar(100) NOT NULL COMMENT '群名称',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `user_name` varchar(255) NOT NULL COMMENT '用户名',
  `msg` varchar(100) NOT NULL COMMENT '短讯',
  `create_time` datetime NOT NULL COMMENT '退群时间',
  PRIMARY KEY (`id`),
  KEY `idx_group_master_user_id_group_id` (`group_master_user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_group_quit_msg`
--

LOCK TABLES `t_group_quit_msg` WRITE;
/*!40000 ALTER TABLE `t_group_quit_msg` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_group_quit_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_notify_msg`
--

DROP TABLE IF EXISTS `t_notify_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_notify_msg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_id` tinyint(4) NOT NULL COMMENT '类型id',
  `type_name` varchar(50) NOT NULL COMMENT '类型名称',
  `from_id` bigint(20) NOT NULL COMMENT '来源id',
  `from_name` varchar(50) NOT NULL COMMENT '来源名称',
  `to_id` bigint(20) NOT NULL COMMENT '目的id',
  `to_name` varchar(50) NOT NULL COMMENT '目的名称',
  `group_name` varchar(100) DEFAULT NULL COMMENT '群名称',
  `receive_time` datetime NOT NULL COMMENT '接收时间',
  `exert_msg` varchar(100) DEFAULT NULL COMMENT '补充信息',
  `msg_status` tinyint(4) DEFAULT NULL COMMENT '消息状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_notify_msg`
--

LOCK TABLES `t_notify_msg` WRITE;
/*!40000 ALTER TABLE `t_notify_msg` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_notify_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_single_msg0`
--

DROP TABLE IF EXISTS `t_single_msg0`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_single_msg0` (
  `msg_server_id` bigint(20) NOT NULL COMMENT '消息在服务端生成的id',
  `msg_client_id` varchar(100) NOT NULL COMMENT '消息在客户端生成的id',
  `from_id` bigint(20) NOT NULL COMMENT '发送者id',
  `from` varchar(50) NOT NULL COMMENT '发送者',
  `to_id` bigint(20) NOT NULL COMMENT '接收者id',
  `to` varchar(50) NOT NULL COMMENT '接收者',
  `time` datetime NOT NULL COMMENT '时间',
  `type` int(11) NOT NULL COMMENT '消息类型',
  `content` varchar(300) NOT NULL COMMENT '消息内容',
  `status` varchar(20) NOT NULL COMMENT '消息状态',
  PRIMARY KEY (`msg_server_id`),
  KEY `idx_from_id_status` (`from_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_single_msg0`
--

LOCK TABLES `t_single_msg0` WRITE;
/*!40000 ALTER TABLE `t_single_msg0` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_single_msg0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_single_msg1`
--

DROP TABLE IF EXISTS `t_single_msg1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_single_msg1` (
  `msg_server_id` bigint(20) NOT NULL COMMENT '消息在服务端生成的id',
  `msg_client_id` varchar(100) NOT NULL COMMENT '消息在客户端生成的id',
  `from_id` bigint(20) NOT NULL COMMENT '发送者id',
  `from` varchar(50) NOT NULL COMMENT '发送者',
  `to_id` bigint(20) NOT NULL COMMENT '接收者id',
  `to` varchar(50) NOT NULL COMMENT '接收者',
  `time` datetime NOT NULL COMMENT '时间',
  `type` int(11) NOT NULL COMMENT '消息类型',
  `content` varchar(300) NOT NULL COMMENT '消息内容',
  `status` varchar(20) NOT NULL COMMENT '消息状态',
  PRIMARY KEY (`msg_server_id`),
  KEY `idx_from_id_status` (`from_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_single_msg1`
--

LOCK TABLES `t_single_msg1` WRITE;
/*!40000 ALTER TABLE `t_single_msg1` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_single_msg1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_single_msg2`
--

DROP TABLE IF EXISTS `t_single_msg2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_single_msg2` (
  `msg_server_id` bigint(20) NOT NULL COMMENT '消息在服务端生成的id',
  `msg_client_id` varchar(100) NOT NULL COMMENT '消息在客户端生成的id',
  `from_id` bigint(20) NOT NULL COMMENT '发送者id',
  `from` varchar(50) NOT NULL COMMENT '发送者',
  `to_id` bigint(20) NOT NULL COMMENT '接收者id',
  `to` varchar(50) NOT NULL COMMENT '接收者',
  `time` datetime NOT NULL COMMENT '时间',
  `type` int(11) NOT NULL COMMENT '消息类型',
  `content` varchar(300) NOT NULL COMMENT '消息内容',
  `status` varchar(20) NOT NULL COMMENT '消息状态',
  PRIMARY KEY (`msg_server_id`),
  KEY `idx_from_id_status` (`from_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_single_msg2`
--

LOCK TABLES `t_single_msg2` WRITE;
/*!40000 ALTER TABLE `t_single_msg2` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_single_msg2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `register_time` datetime DEFAULT NULL,
  `bak` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'chenfz','$2a$10$q2xQzSJCpRUSzdIfJX6pqOycNr7u8hYKDGzcSIcgyffvsL/thanLa',NULL,NULL,NULL,'2020-07-21 19:07:23',NULL),(2,'putiancfz','$2a$10$uv3zNSsZMpHLkpxEW8ikPevV3mKD2QeuNzQlNUZlT93vIMB0uC5y2',NULL,NULL,NULL,'2020-07-21 19:08:45',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-25  5:52:42
