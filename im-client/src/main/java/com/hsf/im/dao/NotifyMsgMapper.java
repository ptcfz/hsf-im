package com.hsf.im.dao;

import com.hsf.im.domain.NotifyMsg;
import com.hsf.im.domain.NotifyTypeCount;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface NotifyMsgMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(NotifyMsg record);

    int insertSelective(NotifyMsg record);

    NotifyMsg selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(NotifyMsg record);

    int updateByPrimaryKey(NotifyMsg record);

    //TODO 还需要完善分页查询
    public List<NotifyTypeCount> getNotifySumInfo();

    List<NotifyMsg> getNotifyMsgByType(@Param("typeId") int typeId, @Param("offset") int offset, @Param("pageSize") int pageSize);

    void updateNotifyMsgStatus(List<NotifyMsg> notifyMsgList);
}