package com.hsf.im.dao;

import com.hsf.im.domain.MyDetail;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MyDetailMapper {
    int deleteByPrimaryKey(String id);

    int insert(MyDetail record);

    int insertSelective(MyDetail record);

    MyDetail selectByPrimaryKey(String id);

    MyDetail selectByUsername(String username);

    int updateByPrimaryKeySelective(MyDetail record);

    int updateByPrimaryKey(MyDetail record);
}