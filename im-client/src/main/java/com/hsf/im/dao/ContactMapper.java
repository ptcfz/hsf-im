package com.hsf.im.dao;

import com.hsf.im.domain.Contact;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ContactMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Contact record);

    int insertSelective(Contact record);

    Contact selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Contact record);

    int updateByPrimaryKey(Contact record);

    List<Contact> findRecentContact(int limit, int offset);

    Contact findByUsername(String username);

    void updateByUsername(Contact contact);

}