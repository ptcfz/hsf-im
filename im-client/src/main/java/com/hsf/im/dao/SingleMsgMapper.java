package com.hsf.im.dao;

import com.hsf.im.domain.ClientSingleMsg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SingleMsgMapper {

    int deleteByPrimaryKey(String msgClientId);

    int insert(ClientSingleMsg record);

    int insertSelective(ClientSingleMsg record);

    ClientSingleMsg selectByPrimaryKey(String msgClientId);

    int updateByPrimaryKeySelective(ClientSingleMsg record);

    int updateByPrimaryKey(ClientSingleMsg record);

    int ackMsgByMsgClientId(String msgClientId,String msgServerId,String status);

    void updateUserMsgStatus(String status,String username);

    List<ClientSingleMsg> findFromSingleMsg(@Param("fromUsername") String fromUsername, @Param("skip") int skip, @Param("limit") int limit);
}