package com.hsf.im.sender;

import com.hsf.im.client.ClientSession;
import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@Component
public class HeartBeatMsgSender extends BaseSender {

    public void sendHeartBearMsg(){
        ProtoMsg.Message msg=MsgBuilder.heartBeat(getSession().getUsername(),getSession().getSessionId(),0);
        sendMsg(msg);
    }

    public void sendClientSession(ClientSession session){
        super.setSession(session);
    }
}
