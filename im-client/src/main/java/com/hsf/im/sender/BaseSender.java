package com.hsf.im.sender;

import com.hsf.im.client.ClientSession;
import com.hsf.im.msg.ProtoMsg;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseSender {

    Logger logger= LoggerFactory.getLogger(BaseSender.class);

    private ClientSession session;

    public void sendMsg(ProtoMsg.Message msg){

        Channel channel = session.getChannel();
        ChannelFuture f = channel.writeAndFlush(msg);
        f.addListener(new GenericFutureListener<Future<? super Void>>() {
            @Override
            public void operationComplete(Future<? super Void> future)
                    throws Exception {
                // 回调
                if (future.isSuccess()) {
                    logger.debug("msg send success...");

                } else {
                    logger.error("msg send faild:"+future.cause().getMessage(),future.cause());
                }
            }

        });
    }


    public ClientSession getSession() {
        return session;
    }

    public void setSession(ClientSession session) {
        this.session = session;
    }
}
