package com.hsf.im.sender;

import com.hsf.im.client.ClientSession;
import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@Component
public class CloseChannelMsgSender extends BaseSender{

    public void sendCloseChannelMsg(int seqId,String code,String info){
        ProtoMsg.Message msg= MsgBuilder.closeChannel(getSession().getSessionId(),seqId,code,info);
        sendMsg(msg);
    }

    public void setClientSession(ClientSession session){
        super.setSession(session);
    }

}
