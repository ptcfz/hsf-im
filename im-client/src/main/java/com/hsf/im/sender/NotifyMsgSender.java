package com.hsf.im.sender;

import com.hsf.im.client.ClientSession;
import com.hsf.im.msg.ProtoMsg;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@Component
public class NotifyMsgSender extends BaseSender{

    public void sendNotifyMsg(ProtoMsg.Message msg){
        sendMsg(msg);
    }

    public void setClientSession(ClientSession session){
        super.setSession(session);
    }
}
