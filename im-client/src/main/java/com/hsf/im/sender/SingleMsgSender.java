package com.hsf.im.sender;

import com.hsf.im.client.ClientSession;
import com.hsf.im.msg.ProtoMsg;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 * 一对一 消息发送器
 */
@Component
public class SingleMsgSender extends BaseSender{

    public void sendSingleMsg(ProtoMsg.Message msg){
        sendMsg(msg);
    }

    public void sendSingleMsgAck(ProtoMsg.Message msg){
        sendMsg(msg);
    }

    public void setClientSession(ClientSession session){
        super.setSession(session);
    }
}
