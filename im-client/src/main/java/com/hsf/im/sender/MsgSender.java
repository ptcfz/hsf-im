package com.hsf.im.sender;

import com.hsf.im.client.ClientSession;
import com.hsf.im.domain.ClientSingleMsg;
import com.hsf.im.domain.SingleMsg;
import com.hsf.im.msg.MsgStatusEnum;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.service.SingleMsgService;
import com.hsf.im.utils.ClientMsgConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@Component
public class MsgSender extends BaseSender{

    @Autowired
    private CloseChannelMsgSender closeChannelMsgSender;

    @Autowired
    private HeartBeatMsgSender heartBeatMsgSender;

    @Autowired
    private SingleMsgSender singleMsgSender;

    @Autowired
    private SingleMsgService singleMsgService;

    @Autowired
    private NotifyMsgSender notifyMsgSender;

    public void sendCloseChannelMsg(int seqId,String code,String info){
        closeChannelMsgSender.sendCloseChannelMsg(seqId,code,info);
    }

    public void sendHeartBearMsg(){
        heartBeatMsgSender.sendHeartBearMsg();
    }

    public void sendSingleMsg(ProtoMsg.Message msg){
        //发送消息之前保存到本地数据库
        ClientSingleMsg singleMsg= ClientMsgConvertUtil.singleMsgRequest2SingleMsg(msg.getSingleMessageRequest());
        singleMsg.setStatus(MsgStatusEnum.CLIENT_SEND.getCode());
        singleMsgService.addSingleMsg(singleMsg);
        singleMsgSender.sendSingleMsg(msg);
    }

    public void sendSingleMsgAck(ProtoMsg.Message msg){
        singleMsgSender.sendSingleMsgAck(msg);
    }

    public void sendNotifyMsg(ProtoMsg.Message msg){
        notifyMsgSender.sendMsg(msg);
    }

    public void setClientSession(ClientSession session){
        super.setSession(session);
        singleMsgSender.setSession(session);
        heartBeatMsgSender.setSession(session);
        closeChannelMsgSender.setClientSession(session);
        notifyMsgSender.setClientSession(session);
    }
}
