package com.hsf.im.utils;

import com.hsf.im.domain.NotifyMsg;
import com.hsf.im.msg.MsgTypeEnum;
import com.hsf.im.msg.ProtoMsg;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: chenfz
 */
public class ClientMsgBuilder {

    private static final int NOTIFY_MSG_INIT_STATUS=0;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /**
     * 将接收到的message转换为client端保存的notify对象
     * @param message
     * @return
     */
    public static NotifyMsg buildNotifyMsg(ProtoMsg.Message message, long toUserId,String toUsername){
        ProtoMsg.HeadType headType = ((ProtoMsg.Message) message).getType();
        if (!headType.equals(ProtoMsg.HeadType.GROUP_QUIT_NOTIFY)) {
            return null;
        }
        NotifyMsg notifyMsg=new NotifyMsg();
        notifyMsg.setTypeId(MsgTypeEnum.GROUP_QUIT_NOTIFY.getCode());
        notifyMsg.setTypeName(MsgTypeEnum.GROUP_QUIT_NOTIFY.getDesc());
        notifyMsg.setReceiveTime(sdf.format(new Date()));
        notifyMsg.setFromId(message.getGroupQuitNotify().getUserId());
        notifyMsg.setFromName(message.getGroupQuitNotify().getUserName());
        notifyMsg.setToId(toUserId);
        notifyMsg.setToName(toUsername);
        notifyMsg.setGroupName(message.getGroupQuitNotify().getGroupName());
        notifyMsg.setExertMsg(message.getGroupQuitNotify().getMsg());
        notifyMsg.setMsgStatus(NOTIFY_MSG_INIT_STATUS);
        return notifyMsg;
    }
}
