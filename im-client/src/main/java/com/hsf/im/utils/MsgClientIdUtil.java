package com.hsf.im.utils;

import java.util.UUID;

/**
 * @Author: chenfz
 */
public class MsgClientIdUtil {

    public static String buildNewMsgClientId() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replaceAll("-", "");
    }
}
