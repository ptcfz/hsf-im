package com.hsf.im.utils;

/**
 * @Author: chenfz
 */
public class MenuUiUtil {
    /**
     * 向屏幕输出多行空白
     * @param numRow 空白行数量
     */
    public static void printNumBlankRow(int numRow){
        if(numRow<=0){
            numRow=1;
        }
        for(int i=0;i<numRow;i++){
            System.out.println();
        }
    }

    /**
     * 向屏幕输出分隔行
     * @param flag  分隔符号
     * @param num   每行几个分隔符号
     * @param rows  输出几行
     */
    public static void printRowSeparateLine(String flag,int num,int rows){
        if(num<=0){
            num=10;
        }
        if(rows<=0){
            rows=1;
        }
        for(int i=0;i<rows;i++){
            for(int j=0;j<num;j++){
                System.out.print(flag);
            }
            System.out.println();
        }
    }
}
