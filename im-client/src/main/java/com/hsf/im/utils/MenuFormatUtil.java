package com.hsf.im.utils;

import com.hsf.im.domain.*;
import com.hsf.im.msg.ProtoMsg;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @Author: chenfz
 */
public class MenuFormatUtil {

    public static SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void formatAndPrintSingleMsg(ProtoMsg.Message message){
        MenuUiUtil.printNumBlankRow(1);
        System.out.println(sdf.format(message.getSingleMessageRequest().getTime()));
        System.out.println("from: "+message.getSingleMessageRequest().getFrom());
        System.out.println(message.getSingleMessageRequest().getContent());
        MenuUiUtil.printNumBlankRow(1);

    }

    public static void formatUserinfoAndPrint(User user){
        if(user==null){
            System.out.println("╯０╰     用户不存在！");
        }else{
            MenuUiUtil.printNumBlankRow(1);
            MenuUiUtil.printRowSeparateLine("-",50,1);
            System.out.println(" username:"+user.getUsername());
            System.out.println("   gender:"+user.getGender());
            System.out.println("   mobile:"+user.getMobile());
            System.out.println("  address:"+user.getAddress());
            System.out.println("      bak:"+user.getBak());
            MenuUiUtil.printRowSeparateLine("-",50,1);
            MenuUiUtil.printNumBlankRow(1);

        }
    }

    public static void formatRecentContactAndPrint(List<Contact> contactList){
        MenuUiUtil.printNumBlankRow(1);
        MenuUiUtil.printRowSeparateLine("-",50,1);
        if(contactList==null || contactList.size()==0){
            System.out.println("没有更多联系人了");
            return;
        }
        for(Contact contact : contactList){
            MenuUiUtil.printNumBlankRow(1);
            System.out.println("用    户："+contact.getUsername());
            System.out.println("未读消息："+contact.getUnReadMsgCount());
        }
        MenuUiUtil.printRowSeparateLine("-",50,1);
        MenuUiUtil.printNumBlankRow(1);

    }

    public static void formatUserRecentMsgAndPrint(List<ClientSingleMsg> singleMsgList,String selfUsername){
        MenuUiUtil.printNumBlankRow(2);
        for(ClientSingleMsg singleMsg : singleMsgList){
            if(selfUsername.equals(singleMsg.getFromUser())){
                formatSelfSingleMsg(singleMsg);
            }else{
                formatRemoteUserSingleMsg(singleMsg);
            }
        }
    }

    public static void formatRemoteUserSingleMsg(ClientSingleMsg singleMsg){
        MenuUiUtil.printNumBlankRow(1);
        System.out.println(singleMsg.getTime());
        System.out.println("from:"+singleMsg.getFromUser());
        System.out.println(singleMsg.getContent());
        MenuUiUtil.printNumBlankRow(1);

    }

    public static void formatSelfSingleMsg(ClientSingleMsg singleMsg){
        MenuUiUtil.printNumBlankRow(1);
        System.out.println(singleMsg.getContent());
        MenuUiUtil.printNumBlankRow(1);

    }

    public static void formatGroupsInfo(List<Group> groups){
        if(groups!=null && groups.size()!=0){
            MenuUiUtil.printNumBlankRow(1);
            for(int i=0;i<groups.size();i++){
                Group group=groups.get(i);
                System.out.println("     index:"+i);
                System.out.println(" groupName:"+group.getGroupName());
                System.out.println("catelogTop:"+group.getCatelogTopName());
                System.out.println("catelogSec:"+group.getCatelogSecondName());
            }
            MenuUiUtil.printNumBlankRow(1);
        }
    }

    public static void formatSysNotifyTypeInfo(List<NotifyTypeCount> notifyTypeCountList){
        if(notifyTypeCountList!=null && notifyTypeCountList.size()!=0){
            MenuUiUtil.printNumBlankRow(1);
            for(int i=0;i<notifyTypeCountList.size();i++){
                NotifyTypeCount notifyTypeCount = notifyTypeCountList.get(i);
                System.out.println("序    号:"+i);
                System.out.println("提示类型:"+notifyTypeCount.getTypeName());
                System.out.println("数    量:"+notifyTypeCount.getCount());
            }
            MenuUiUtil.printNumBlankRow(1);
        }
    }

    public static void formatQuitGroupNotifyInfo(List<NotifyMsg> notifyMsgList){
        if(notifyMsgList!=null && notifyMsgList.size()!=0){
            MenuUiUtil.printNumBlankRow(1);
            for(int i=0;i<notifyMsgList.size();i++){
                NotifyMsg notifyMsg = notifyMsgList.get(i);
                System.out.println("  消息类型："+notifyMsg.getTypeName());
                System.out.println("    群名称："+notifyMsg.getGroupName());
                System.out.println("      用户："+notifyMsg.getFromName());
            }
            MenuUiUtil.printNumBlankRow(1);
        }
    }
}
