package com.hsf.im.utils;

import com.hsf.im.domain.ClientSingleMsg;
import com.hsf.im.domain.SingleMsg;
import com.hsf.im.msg.ProtoMsg;

import java.text.SimpleDateFormat;

/**
 * @Author: chenfz
 */
public class ClientMsgConvertUtil {

    private static final SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static ClientSingleMsg singleMsgRequest2SingleMsg(ProtoMsg.SingleMessageRequest request){
        ClientSingleMsg msg=new ClientSingleMsg();
        msg.setMsgClientId(request.getMsgClientId());
        msg.setMsgServerId(Long.toString(request.getMsgServerId()));
        msg.setFromUser(request.getFrom());
        msg.setToUser(request.getTo());
        msg.setMsgType(request.getMsgType());
        msg.setContent(request.getContent());
        msg.setTime(sdf.format(request.getTime()));
        return msg;
    }
}
