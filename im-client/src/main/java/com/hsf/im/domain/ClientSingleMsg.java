package com.hsf.im.domain;

/**
 * @Author: chenfz
 */
public class ClientSingleMsg {

    private String msgClientId;

    private String msgServerId;

    private String fromUser;

    private String toUser;

    private Integer msgType;

    private String content;

    private String status;

    private String time;

    public String getMsgClientId() {
        return msgClientId;
    }

    public void setMsgClientId(String msgClientId) {
        this.msgClientId = msgClientId == null ? null : msgClientId.trim();
    }

    public String getMsgServerId() {
        return msgServerId;
    }

    public void setMsgServerId(String msgServerId) {
        this.msgServerId = msgServerId == null ? null : msgServerId.trim();
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser == null ? null : fromUser.trim();
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser == null ? null : toUser.trim();
    }

    public Integer getMsgType() {
        return msgType;
    }

    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time == null ? null : time.trim();
    }
    
}
