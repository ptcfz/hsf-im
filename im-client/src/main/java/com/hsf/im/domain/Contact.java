package com.hsf.im.domain;

public class Contact {
    private Long id;

    private String username;

    private String mobile;

    private String gender;

    private String address;

    private String bak;

    private Integer unReadMsgCount;

    private String lastContectTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getBak() {
        return bak;
    }

    public void setBak(String bak) {
        this.bak = bak == null ? null : bak.trim();
    }

    public Integer getUnReadMsgCount() {
        return unReadMsgCount;
    }

    public void setUnReadMsgCount(Integer unReadMsgCount) {
        this.unReadMsgCount = unReadMsgCount;
    }

    public String getLastContectTime() {
        return lastContectTime;
    }

    public void setLastContectTime(String lastContectTime) {
        this.lastContectTime = lastContectTime == null ? null : lastContectTime.trim();
    }
}