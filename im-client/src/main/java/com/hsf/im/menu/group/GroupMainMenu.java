package com.hsf.im.menu.group;

import com.hsf.im.client.Clientsessionholder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Scanner;

/**
 * @Author: chenfz
 * 群组主页面，在这个页面可以选择：查看已有群，创建群，删除群，加入群，退出群，审批入群申请等
 */
@Component
public class GroupMainMenu {

    Logger logger = LoggerFactory.getLogger(GroupMainMenu.class);

    private boolean group_main_quit = false;

    @Autowired
    private GroupListMenu groupListMenu;

    @Autowired
    private CreateGroupMenu createGroupMenu;

    @Autowired
    private Clientsessionholder sessionholder;

    public void start() {
        group_main_quit=false;
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        while(!group_main_quit){
            String menuChoose=printAndReciveChoose();
            switch (menuChoose) {
                case GroupMenuOptions.GROUPMAIN_LIST_GROUPS:
                    groupListMenu.start(sessionholder.getSession().getUserInfo().getId());
                    break;
                case GroupMenuOptions.GROUPMAIN_CREATE_GROUP:
                    createGroupMenu.start();
                    break;
                case GroupMenuOptions.GROUPMAIN_SEARCH_GROUP:
                    break;
                case GroupMenuOptions.GROUPMAIN_APPROVE_GROUP:
                    break;
                case GroupMenuOptions.GROUPMAIN_QUIT_MENU:
                    group_main_quit=true;
                    break;
                default:
                    System.out.println("输入的选项有误哦~");
                    break;
            }
        }
    }


    private String printAndReciveChoose(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("1:查看群，2:创建群，3:查找群，4:入群申请审批，q:返回上级页面");
        String menuChoose="";
        try{
            menuChoose=scanner.nextLine();
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            System.out.println("输入选项有误哦~");
            scanner.close();
        }
        return menuChoose;
    }

}