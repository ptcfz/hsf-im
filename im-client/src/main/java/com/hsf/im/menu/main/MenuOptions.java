package com.hsf.im.menu.main;

/**
 * @Author: chenfz
 * 页面上的命令
 */
public class MenuOptions {

    /**
     * common command
     */
    public static final String COMMAND_PREFIX="cmd";
    public static final String QUIT="quit";
    public static final String HELP="help";

    /**
     * main menu
     */
    public static final String REGISTER="1";
    public static final String LOGIN="2";
    public static final String CONTACT="3";
    public static final String GROUP="4";
    public static final String SYS_NOTIFY="5";
    public static final String MAIN_QUIT="q";

    /**
     * contact menu
     */
    public static final String FIND_USER="1";
    public static final String CONTACT_LIST="2";
    public static final String CONTACT_GO_BACK="3";

    /**
     * userdetail menu
     */
    public static final String USERDETAIL_SEND_MSG="1";
    public static final String USERDETAIL_GO_BACK="2";

    /**
     * contact list menu
     */
    public static final String CONTACTLIST_NEXT_PAGE="1";
    public static final String CONTACTLIST_PRE_PAGE="2";
    public static final String CONTACTLIST_CHOSE_CONTACT="3";
    public static final String CONTACTLIST_QUIT="4";

    /**
     * SysNotifyMenu页面的选项
     */
    public static final String SYSNOTIFY_NEXT_PAGE="n";
    public static final String SYSNOTIFY_PREVIOUT_PAGE="p";
    public static final String SYSNOTIFY_QUIT="q";

    /**
     * SysNotifyListMenu页面的选项
     */
    public static final String SYSNOTIFY_LIST_NEXT_PAGE="n";
    public static final String SYSNOTIFY_LIST_PREVIOUS_PAGE="p";
    public static final String SYSNOTIFY_LIST_QUIT="q";

}
