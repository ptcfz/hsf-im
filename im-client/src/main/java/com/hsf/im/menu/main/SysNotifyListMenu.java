package com.hsf.im.menu.main;

import com.hsf.im.domain.NotifyMsg;
import com.hsf.im.domain.NotifyTypeCount;
import com.hsf.im.service.NotifyMsgService;
import com.hsf.im.utils.MenuFormatUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;

/**
 * @Author: chenfz
 */
@Component
public class SysNotifyListMenu {

    Logger logger = LoggerFactory.getLogger(SysNotifyListMenu.class);

    private boolean sys_notify_list_quit = false;

    @Autowired
    private NotifyMsgService notifyMsgService;

    private int currentPage=0;
    private int pageSize=10;
    private List<NotifyMsg> currentNotifyMsgList;

    public void start(NotifyTypeCount notifyTypeCount) {
        //查看某一类的系统提示消息
        sys_notify_list_quit=false;
        currentNotifyMsgList = notifyMsgService.getNotifyMsgByType(notifyTypeCount.getTypeId(),currentPage*pageSize,pageSize);
        MenuFormatUtil.formatQuitGroupNotifyInfo(currentNotifyMsgList);
        updateReadMsgStatus(currentNotifyMsgList);
        while(!sys_notify_list_quit){
            String menuChoose = printAndReciveChoose();
            switch (menuChoose){
                case MenuOptions.SYSNOTIFY_LIST_NEXT_PAGE:
                    //下一页，如果currentNotifyMsgList数量==pageSize，才可以查看下一页
                    int currentListSize=currentNotifyMsgList.size();
                    if(currentListSize==pageSize){
                        currentPage++;
                        currentNotifyMsgList=notifyMsgService.getNotifyMsgByType(notifyTypeCount.getTypeId(),currentPage*pageSize,pageSize);
                        MenuFormatUtil.formatQuitGroupNotifyInfo(currentNotifyMsgList);
                        updateReadMsgStatus(currentNotifyMsgList);
                    }else{
                        System.out.println();
                        System.out.println("没有更多信息了~");
                        System.out.println();
                    }
                    break;
                case MenuOptions.SYSNOTIFY_LIST_PREVIOUS_PAGE:
                    break;
                case MenuOptions.SYSNOTIFY_LIST_QUIT:
                    //返回上一级
                    sys_notify_list_quit=true;
                    break;
                default:
                    break;
            }
        }
    }


    private String printAndReciveChoose(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("p：上一页，n：下一页，q：返回上级页面");
        String menuChoose="";
        try{
            menuChoose=scanner.next();
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            System.out.println("输入选项有误哦~");
            scanner.close();
        }
        return menuChoose;
    }

    private void updateReadMsgStatus(List<NotifyMsg> notifyMsgList){
        if(notifyMsgList!=null && notifyMsgList.size()!=0){
            notifyMsgService.updateNotifyMsgStatus(notifyMsgList);
        }
    }
}
