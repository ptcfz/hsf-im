package com.hsf.im.menu.main;

import com.hsf.im.menu.contact.ContactMenu;
import com.hsf.im.menu.group.GroupMainMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class MainMenu {

    Logger logger = LoggerFactory.getLogger(MainMenu.class);

    @Autowired
    private RegisterMenu registerCommand;

    @Autowired
    private LoginMenu loginMenu;

    @Autowired
    private ContactMenu contactMenu;

    @Autowired
    private GroupMainMenu groupMainMenu;

    @Autowired
    private SysNotifyMenu sysNotifyMenu;

    public void start(){
        boolean main_quit=false;
        printWelcome();
        while(!main_quit){
            String menuChoose=printAndReciveChoose();
            switch (menuChoose){
                //注册
                case MenuOptions.REGISTER:
                    registerCommand.register();
                    break;
                    //登录
                case MenuOptions.LOGIN:
                    loginMenu.start();
                    break;
                case MenuOptions.CONTACT:
                    contactMenu.start();
                    break;
                case MenuOptions.GROUP:
                    groupMainMenu.start();
                    break;
                case MenuOptions.SYS_NOTIFY:
                    //TODO 查看系统提示信息
                    sysNotifyMenu.start();
                    break;
                case MenuOptions.MAIN_QUIT:
                    main_quit=true;
                    break;
                default:
                    System.out.println("输入的选项有误哦~");
                    break;
            }
        }
    }

    public void printWelcome(){
        System.out.println("-------------欢迎使用hsf-im系统---------------");
    }

    public String printAndReciveChoose(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("1:注册，2:登录，3:联系人，4:群组，5:系统消息，q:退出");
        String menuChoose="";
        try{
            menuChoose=scanner.next();
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            System.out.println("输入选项有误哦~");
            scanner.close();
        }
        return menuChoose;
    }
}
