package com.hsf.im.menu.contact;

import com.hsf.im.domain.Contact;
import com.hsf.im.menu.main.MenuOptions;
import com.hsf.im.service.ContactService;
import com.hsf.im.utils.MenuFormatUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;

/**
 * @Author: chenfz
 */
@Component
public class ContactListMenu {

    Logger logger= LoggerFactory.getLogger(ContactListMenu.class);

    @Autowired
    private ContactService contactService;

    @Autowired
    private ChartMenu chartMenu;

    public void start() {
        boolean contact_list_quit=false;
        int limit=10;
        int offset=0;
        while(!contact_list_quit){
            List<Contact> contactList=contactService.findRecentContact(limit,offset);
            if(contactList.size()==0){
                offset-=10;
            }
            MenuFormatUtil.formatRecentContactAndPrint(contactList);
            String chose=printAndReciveChoose();
            switch (chose){
                case MenuOptions.CONTACTLIST_NEXT_PAGE:
                    //下一页
                    offset+=10;
                    break;
                case MenuOptions.CONTACTLIST_PRE_PAGE:
                    if(offset>=10){
                        offset-=10;
                    }
                    break;
                case MenuOptions.CONTACTLIST_CHOSE_CONTACT:
                    //选择联系人，打开与该用户的会话
                    System.out.print("请输入此页联系人用户名：");
                    Scanner scanner = new Scanner(System.in);
                    try{
                        String username=scanner.next();
                        Contact contact=contactList.stream().filter(e->e.getUsername().equals(username)).findFirst().get();
                        if(contact!=null){
                            chartMenu.start(contact);
                        }else{
                            System.out.println("此页中没有该用户哦~");
                        }
                    }catch (Exception e){
                        System.out.println("输入有误哦~");
                        logger.error(e.getMessage(),e);
                    }
                    break;
                case MenuOptions.CONTACTLIST_QUIT:
                    contact_list_quit=true;
                    break;
                default:
                    System.out.println("输入选项有误哦~");
                    break;
            }
        }
    }

    public String printAndReciveChoose(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("1:下一页，2:上一页，3:打开用户会话，4:返回上一级");
        String menuChoose="";
        try{
            menuChoose=scanner.next();
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            System.out.println("输入选项有误哦~");
            scanner.close();
        }
        return menuChoose;
    }
}
