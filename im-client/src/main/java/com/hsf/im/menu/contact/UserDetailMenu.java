package com.hsf.im.menu.contact;

import com.hsf.im.domain.Contact;
import com.hsf.im.domain.User;
import com.hsf.im.menu.contact.ChartMenu;
import com.hsf.im.menu.main.MenuOptions;
import com.hsf.im.service.ContactService;
import com.hsf.im.utils.MenuFormatUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * @Author: chenfz
 * 显示用户详细信息
 */
@Component
public class UserDetailMenu {

    @Autowired
    private ChartMenu chartMenu;

    @Autowired
    private ContactService contactService;

    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void start(User user){
        if(user==null){
            System.out.println("╯０╰     用户不存在！");
            return;
        }
        MenuFormatUtil.formatUserinfoAndPrint(user);
        String chose=printAndReciveChoose();
        switch (chose){
            case MenuOptions.USERDETAIL_SEND_MSG:
                //往联系人列表新增或更新记录
                Contact contact=contactService.findContactByUsername(user.getUsername());
                if(contact==null){
                    contact=new Contact();
                    contact.setId(user.getId());
                    contact.setAddress(user.getAddress());
                    contact.setGender(user.getGender());
                    contact.setMobile(user.getMobile());
                    contact.setBak(user.getBak());
                    contact.setUsername(user.getUsername());
                    contact.setUnReadMsgCount(0);
                    contact.setLastContectTime(sdf.format(new Date()));
                    contactService.addContact(contact);
                }else{
                    contact.setLastContectTime(sdf.format(new Date()));
                    contactService.updateContactByUsername(contact);
                }
                chartMenu.start(contact);
                break;
            case MenuOptions.USERDETAIL_GO_BACK:
                break;
            default:
                System.out.println("输入选项有误哦~");
        }
    }

    public String printAndReciveChoose(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("1:发送消息，2：返回上一级");
        String chose="";
        try{
            chose=scanner.next();
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("输入有误哦~");
            scanner.close();
        }
        return chose;
    }
}
