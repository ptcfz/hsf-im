package com.hsf.im.menu.group;

import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.component.SystemConst;
import com.hsf.im.domain.Group;
import com.hsf.im.domain.GroupCatelog;
import com.hsf.im.model.dto.GroupDto;
import com.hsf.im.utils.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * @Author: chenfz
 * 创建群页面
 */
@Component
public class CreateGroupMenu {

    Logger logger = LoggerFactory.getLogger(CreateGroupMenu.class);

    private final String APPROVE_CHOOSE="y";
    private final String DENY_CHOOSE="n";

    @Autowired
    private Clientsessionholder clientsessionholder;

    @Autowired
    private SystemConst systemConst;

    public void start() {
        List<GroupCatelog> allGroupCatelog = HttpUtils.getAllGroupCatelog(systemConst.getGetAllGroupCatelogUrl(),clientsessionholder.getSession().getToken().getToken());
        Map<Byte,List<GroupCatelog>> map = allGroupCatelog.stream().collect(Collectors.groupingBy(t ->t.getLevel()));
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入群名称:");
        String groupName = scanner.nextLine();
        //TODO 输入群分类
        //是否需要申请加入
        System.out.println("是否需要申请加入?(y/n)");
        String needApply = scanner.nextLine();
        Byte na;
        if(APPROVE_CHOOSE.equalsIgnoreCase(needApply)){
            na=1;
        }else {
            na=0;
        }
        GroupDto groupDto = buildGroupDto(groupName,na);
        HttpUtils.createGroup(systemConst.getCreateGroupUrl(),clientsessionholder.getSession().getToken().getToken(),groupDto);
    }

    /**
     * TODO 群类目后续再做
     * @param groupName
     * @param needApply
     * @return
     */
    private GroupDto buildGroupDto(String groupName, Byte needApply){
        Group group = new Group();
        group.setGroupName(groupName);
        group.setNeedApply(needApply);
        group.setCreateTime(new Date());
        group.setCreateUserId(clientsessionholder.getSession().getUserInfo().getId());
        //TODO 这里赋值默认群分类，后续修改
        group.setCatelogTopId(1);
        group.setCatelogTopName("休闲");
        group.setCatelogSecondId(2);
        group.setCatelogSecondName("旅行");
        GroupDto groupDto = new GroupDto();
        groupDto.setGroup(group);
        groupDto.setUser(clientsessionholder.getSession().getUserInfo());
        return groupDto;
    }

}