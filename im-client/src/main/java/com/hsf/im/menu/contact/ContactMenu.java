package com.hsf.im.menu.contact;

import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.menu.main.MenuOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

/**
 * @Author: chenfz
 */
@Component
public class ContactMenu {

    @Autowired
    private Clientsessionholder sessionholder;

    @Autowired
    private FindUserMenu findUserMenu;

    @Autowired
    private ContactListMenu contactListMenu;

    public void start(){
        boolean contact_quit=false;
        while(!contact_quit){
            String chose=printAndReciveChoose();
            switch (chose){
                case MenuOptions.FIND_USER:
                    findUserMenu.start();
                    break;
                case MenuOptions.CONTACT_LIST:
                    contactListMenu.start();
                    break;
                case MenuOptions.CONTACT_GO_BACK:
                    contact_quit=true;
                    break;
                default:
                    break;
            }
        }
    }

    public String printAndReciveChoose(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("1:查找用户，2:联系人列表，3:返回上一级");
        String menuChoose="";
        try{
            menuChoose=scanner.next();
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("输入选项有误哦~");
            scanner.close();
        }
        return menuChoose;
    }
}
