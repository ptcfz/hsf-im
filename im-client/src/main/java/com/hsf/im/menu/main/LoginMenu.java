package com.hsf.im.menu.main;

import com.hsf.im.client.ClientSession;
import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.client.NettyClient;
import com.hsf.im.component.NettyAddressModel;
import com.hsf.im.component.SystemConst;
import com.hsf.im.component.TokenRequestObj;
import com.hsf.im.component.TokenResponseObj;
import com.hsf.im.domain.ClientSingleMsg;
import com.hsf.im.domain.Contact;
import com.hsf.im.domain.SingleMsg;
import com.hsf.im.domain.User;
import com.hsf.im.msg.MsgStatusEnum;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.service.ContactService;
import com.hsf.im.service.SingleMsgService;
import com.hsf.im.utils.HttpUtils;
import com.hsf.im.utils.LockObjFactory;
import com.hsf.im.utils.ThreadPoolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Component
@EnableConfigurationProperties(SystemConst.class)
public class LoginMenu {

    private Logger logger= LoggerFactory.getLogger(LoginMenu.class);

    @Autowired
    private SystemConst systemConst;

    @Autowired
    private Clientsessionholder sessionholder;

    @Autowired
    private SingleMsgService singleMsgService;

    @Autowired
    private ContactService contactService;

    @Autowired
    private NettyClient nettyClient;

    @Autowired
    private MsgSender msgSender;

    public void start(){
        if(sessionholder.getSession()!=null && sessionholder.getSession().isConnected()){
            logger.info("已经登录成功。。。");
            return;
        }
        if(login()){
            logger.info("登录成功。。。");
            //登录成功之后，拿着返回的token去服务器获取netty server的连接地址
            NettyAddressModel model=HttpUtils.sendNettyServerRequest(systemConst.getRequestNettyServerUrl(),sessionholder.getSession().getToken().getToken());
            if(model!=null){
                logger.info(model.toString());
                sessionholder.getSession().setNettyAddressModel(model);
                //连接netty server
                ThreadPoolUtil.runTask(new Runnable() {
                    @Override
                    public void run() {
                        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        //启动客户端连接netty server
                        nettyClient.start(sessionholder.getSession().getNettyAddressModel());
                        //load用户离线消息
                        //List<SingleMsgStoreModel> list=HttpUtils.loadUserOffLineMsg(systemConst.getUserOffLineMsgUrl(),sessionholder.getSession().getUsername());

                    }
                });
            }else{
                logger.error("获取netty server地址失败");
            }
        }else{
            logger.info("登录失败。。。");
        }
    }

    public boolean login(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入用户名:");
        String username=scanner.next();
        System.out.println("请输入密码:");
        String password=scanner.next();
        if(StringUtils.isEmpty(username)||StringUtils.isEmpty(password)){
            return false;
        }
        TokenRequestObj obj=new TokenRequestObj();
        obj.setGrant_type("password");
        obj.setScope("im");
        obj.setUsername(username);
        obj.setPassword(password);
        TokenResponseObj tokenResponse=HttpUtils.sendTokenPost(systemConst.getTokenUrl(),obj);
        if(tokenResponse==null){
            return false;
        }else{
            ClientSession session=new ClientSession();
            session.setUsername(username);
            session.setToken(tokenResponse);
            sessionholder.setSession(session);
            msgSender.setClientSession(session);
            //登录成功之后，再异步的去服务端获取，用户的详细信息
            ThreadPoolUtil.runTask(new AsyncGetUserInfo(session,sessionholder.getSession().getToken().getToken(),username,contactService,singleMsgService,systemConst));
            return true;
        }
    }
}

class AsyncGetUserInfo implements Runnable{

    private ClientSession clientSession;
    private String token;
    private String username;
    private SystemConst systemConst;
    private ContactService contactService;
    private SingleMsgService singleMsgService;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public AsyncGetUserInfo(ClientSession clientSession,String token,String username,ContactService contactService,SingleMsgService singleMsgService,SystemConst systemConst){
        this.clientSession=clientSession;
        this.token=token;
        this.username=username;
        this.contactService=contactService;
        this.singleMsgService=singleMsgService;
        this.systemConst=systemConst;
    }

    @Override
    public void run() {
        User user= HttpUtils.findUserByUsername(systemConst.getFindUserUrl(),token,username);
        if(user!=null){
            clientSession.setUserInfo(user);
        }
        List<SingleMsg> list=HttpUtils.loadUserOffLineMsgFromMysql(systemConst.getUserOffLineMsgFromMysqlUrl(),clientSession.getUserInfo().getId());
        if(list != null && list.size()!=0){
            //存储离线消息，更新用户的未读消息数量
            ReentrantLock lock= LockObjFactory.msgRecordLock;
            lock.lock();
            try{
                Map<String,List<SingleMsg>> msgMap = list.stream().collect(Collectors.groupingBy(t->t.getFromName()));
                //Map<String,List<SingleMsgStoreModel>> msgMap=list.stream().collect(Collectors.groupingBy(t ->t.getFrom()));
                for(Map.Entry entry : msgMap.entrySet()){
                    String fromUsername=(String)entry.getKey();
                    List<SingleMsg> userMsgList=(List<SingleMsg>)entry.getValue();
                    //TODO 这个地方要修改，循环保存效率低
                    for(SingleMsg singleMsg : userMsgList){
                        ClientSingleMsg clientSingleMsg = new ClientSingleMsg();
                        clientSingleMsg.setMsgServerId(String.valueOf(singleMsg.getMsgServerId()));
                        clientSingleMsg.setMsgClientId(singleMsg.getMsgClientId());
                        clientSingleMsg.setContent(singleMsg.getContent());
                        clientSingleMsg.setFromUser(singleMsg.getFromName());
                        clientSingleMsg.setToUser(singleMsg.getToName());
                        clientSingleMsg.setMsgType(singleMsg.getMsgType());
                        clientSingleMsg.setTime(sdf.format(singleMsg.getSendTime()));
                        clientSingleMsg.setStatus(MsgStatusEnum.CLIENT_ACK.getCode());
                        singleMsgService.addSingleMsg(clientSingleMsg);
                    }
                    Collections.sort(list, new Comparator<SingleMsg>() {
                        @Override
                        public int compare(SingleMsg o1, SingleMsg o2) {
                            if(o1.getMsgServerId().compareTo(o2.getMsgServerId())>0){
                                return -1;
                            }else if(o1.getMsgServerId().compareTo(o2.getMsgServerId())<0){
                                return 1;
                            }else{
                                return 0;
                            }
                        }
                    });
                    Long lastAckMsgId = list.get(0).getMsgServerId();
                    HttpUtils.batchAckMsgByLastMsgServerId(systemConst.getBatchAckMsgByLastMsgServerId(),clientSession.getUserInfo().getId(),lastAckMsgId);
//                                    }
                    Contact contact=contactService.findContactByUsername(fromUsername);
                    if(contact==null){
                        User fromUser = HttpUtils.findUserByUsername(systemConst.getFindUserUrl(),clientSession.getToken().getToken(),fromUsername);
                        if(fromUser!=null){
                            contact=new Contact();
                            contact.setId(fromUser.getId());
                            contact.setMobile(fromUser.getMobile());
                            contact.setGender(fromUser.getGender());
                            contact.setAddress(fromUser.getAddress());
                            contact.setBak(fromUser.getBak());
                            contact.setUsername(fromUsername);
                            contact.setUnReadMsgCount(userMsgList.size());
                            contact.setLastContectTime(sdf.format(new Date()));
                            contactService.addContact(contact);
                        }
                    }else{
                        contact.setLastContectTime(sdf.format(new Date()));
                        contact.setUnReadMsgCount(contact.getUnReadMsgCount()+userMsgList.size());
                        contactService.updateContactByUsername(contact);
                    }
                }
            }finally {
                lock.unlock();
            }
        }
    }
}
