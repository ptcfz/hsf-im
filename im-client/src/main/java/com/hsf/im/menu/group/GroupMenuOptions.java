package com.hsf.im.menu.group;

/**
 * @Author: chenfz
 */
public class GroupMenuOptions {


    /**
     * group main menu
     * 群主页面的选项
     */
    public static final String GROUPMAIN_LIST_GROUPS="1";
    public static final String GROUPMAIN_CREATE_GROUP="2";
    public static final String GROUPMAIN_SEARCH_GROUP="3";
    public static final String GROUPMAIN_APPROVE_GROUP="4";
    public static final String GROUPMAIN_QUIT_MENU="q";

    /**
     * group list menu
     * 查看群页面的选项
     */
    public static final String GROUPLIST_GROUP_PREVIOUS="P";
    public static final String GROUPLIST_GROUP_NEXT="n";
    public static final String GROUPLIST_GROUP_QUIT_MENU="q";

    /**
     * group choose menu
     * 群详细信息页面选项
     */
    public static final String GROUPCHOOSE_GROUP_MODIFY="1";
    public static final String GROUPCHOOSE_GROUP_QUIT="2";
    public static final String GROUPCHOOSE_GROUP_DELETE="3";
    public static final String GROUPCHOOSE_GROUP_OPEN="4";
    public static final String GROUPCHOOSE_GROUP_BACK="q";
}
