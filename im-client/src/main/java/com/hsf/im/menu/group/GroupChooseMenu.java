package com.hsf.im.menu.group;

import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.component.SystemConst;
import com.hsf.im.domain.Group;
import com.hsf.im.domain.User;
import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.utils.HttpUtils;
import com.hsf.im.utils.MenuFormatUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @Author: chenfz
 * 选中某个群之后的操作,在这个页面可以，
 * 1：修改群详细信息，2：退出群，3：删除群，4：打开群会话，5：返回上一级
 */
@Component
public class GroupChooseMenu {

    Logger logger = LoggerFactory.getLogger(GroupChooseMenu.class);

    private boolean group_choose_quit = false;

    @Autowired
    private Clientsessionholder clientsessionholder;

    @Autowired
    private ModifyGroupInfoMenu modifyGroupInfoMenu;

    @Autowired
    private SystemConst systemConst;

    @Autowired
    private MsgSender msgSender;


    public void start(Group group) {
        group_choose_quit=false;
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //首先显示群的详细信息
        List<Group> groupList = new ArrayList<>(1);
        groupList.add(group);
        MenuFormatUtil.formatGroupsInfo(groupList);
        while(!group_choose_quit){
            String menuChoose=printAndReciveChoose();
            long userId=clientsessionholder.getSession().getUserInfo().getId();
            String username=clientsessionholder.getSession().getUserInfo().getUsername();
            long groupCreateUserId=group.getCreateUserId();
            switch (menuChoose){
                case GroupMenuOptions.GROUPCHOOSE_GROUP_MODIFY:
                    //修改群详细信息，只有群主有权限做此操作
                    if(userId!=groupCreateUserId){
                        System.out.println("只有群主可以做此操作，您当前无此权限！");
                        break;
                    }
                    modifyGroupInfoMenu.start(group);
                    break;
                case GroupMenuOptions.GROUPCHOOSE_GROUP_QUIT:
                    //退出当前群,群主不可退出当前群，只能解散群
                    if(userId==groupCreateUserId){
                        //群主不可以退出当前群，目前只能解散群
                        //TODO 后续支持修改群主
                        System.out.println("您是此群的创建者，不可退出！");
                        break;
                    }
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("请输入退群的理由:");
                    String msg = scanner.nextLine();
                    //进行用户退群的操作：
                    // 1，删除t_group_members表里该成员
                    HttpUtils.userQuitGroup(systemConst.getUserQuitGroup(),clientsessionholder.getSession().getToken().getToken(),userId,group.getId());
                    //TODO 2，删除本地该群的消息
                    //3，发退群提示给群主
                    User toUser = HttpUtils.findUserByUserId(systemConst.getFindUserByIdUrl(),clientsessionholder.getSession().getToken().getToken(),groupCreateUserId);
                    ProtoMsg.Message quitGroupMsg=MsgBuilder.quitGroupMsg(userId,username,toUser.getUsername(),group.getId(),group.getGroupName(),msg);
                    msgSender.sendNotifyMsg(quitGroupMsg);
                    group_choose_quit=true;
                    break;
                case GroupMenuOptions.GROUPCHOOSE_GROUP_DELETE:
                    //解散当前群，只有群主可以做此操作
                    break;
                case GroupMenuOptions.GROUPCHOOSE_GROUP_OPEN:
                    //打开群会话，接收消息
                    break;
                case GroupMenuOptions.GROUPCHOOSE_GROUP_BACK:
                    //返回上一级页面
                    group_choose_quit=true;
                    break;
                default:
                    System.out.println("输入的选项有误哦~");
                    break;
            }
        }


    }

    private String printAndReciveChoose(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("1：修改群详细信息，2：退出群，3：删除群，4：打开群会话，q：返回上一级");
        String menuChoose="";
        try{
            menuChoose=scanner.nextLine();
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            System.out.println("输入选项有误哦~");
            scanner.close();
        }
        return menuChoose;
    }

}
