package com.hsf.im.menu.group;

import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.component.SystemConst;
import com.hsf.im.domain.Group;
import com.hsf.im.utils.HttpUtils;
import com.hsf.im.utils.MenuFormatUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;

/**
 * @Author: chenfz
 * 查看用户已加入的群，在这个页面用户可以：选择群 之后进行后续操作
 */
@Component
public class GroupListMenu {

    Logger logger = LoggerFactory.getLogger(GroupListMenu.class);

    private  boolean group_list_quit = false;

    @Autowired
    private SystemConst systemConst;

    @Autowired
    private Clientsessionholder clientsessionholder;

    @Autowired
    private GroupChooseMenu groupChooseMenu;

    private Integer currentPage=0;

    private List<Group> currentGroups;


    public void start(Long userId) {
        group_list_quit=false;
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        currentGroups = findUserJoinGroups(currentPage);
        if(currentGroups!=null && currentGroups.size()!=0){
            //默认打开的时候加载前10条
            showGroupsInfo(currentGroups);
        }
        while(!group_list_quit){
            String menuChoose=printAndReciveChoose();
            if(GroupMenuOptions.GROUPLIST_GROUP_QUIT_MENU.equalsIgnoreCase(menuChoose)){
                group_list_quit=true;
                break;
            }
            //根据选择的群，来进行具体的操作
            switch (menuChoose){
                case GroupMenuOptions.GROUPLIST_GROUP_NEXT:
                    //如果当前显示的groups大于10条，可进行下一页
                    if(currentGroups!=null && currentGroups.size()==10){
                        currentPage++;
                    }
                    currentGroups=findUserJoinGroups(currentPage);
                    showGroupsInfo(currentGroups);
                    break;
                case GroupMenuOptions.GROUPLIST_GROUP_PREVIOUS:
                    //如果当前页面不是第0页，则可以显示前一页
                    if(currentPage!=0){
                        currentPage--;
                    }
                    currentGroups=findUserJoinGroups(currentPage);
                    showGroupsInfo(currentGroups);
                    break;
                case GroupMenuOptions.GROUPLIST_GROUP_QUIT_MENU:
                    group_list_quit=true;
                    break;
                default:
                    Boolean flag=menuChoose.matches("^[0-9]$");
                    if(!flag){
                        System.out.println("输入的选项有误哦~");
                    }else{
                        //选中某个群，进入群详细信息页面
                        int index=Integer.parseInt(menuChoose);
                        if(index>currentGroups.size()){
                            System.out.println("超出范围，请重新选中!");
                        }else{
                            Group group = currentGroups.get(index);
                            if(group!=null){
                                //选中某个group，进入到GroupChooseMenu页面
                                groupChooseMenu.start(group);
                            }
                        }
                    }
                    break;
            }
        }
    }

    private List<Group> findUserJoinGroups(int currentPage){
        List<Group> groups = HttpUtils.findUserJoinGroups(systemConst.getFindUserJoinGroups(),clientsessionholder.getSession().getToken().getToken(),clientsessionholder.getSession().getUserInfo().getId(),currentPage);
        return groups;
    }

    private void showGroupsInfo(List<Group> groups){
        MenuFormatUtil.formatGroupsInfo(groups);
    }

    private String printAndReciveChoose(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("按群序号选择群，p：上一页，n：下一页，q：返回上级页面");
        String menuChoose="";
        try{
            menuChoose=scanner.nextLine();
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            System.out.println("输入选项有误哦~");
            scanner.close();
        }
        return menuChoose;
    }
}
