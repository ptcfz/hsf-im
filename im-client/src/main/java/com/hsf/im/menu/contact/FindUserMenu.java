package com.hsf.im.menu.contact;

import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.component.SystemConst;
import com.hsf.im.domain.User;
import com.hsf.im.utils.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

/**
 * @Author: chenfz
 */
@Component
public class FindUserMenu {

    @Autowired
    private SystemConst systemConst;

    @Autowired
    private UserDetailMenu userDetailMenu;

    @Autowired
    private Clientsessionholder sessionholder;

    public void start(){
        String username=printAndReciveChoose();
        //发请求到acount查找用户
        User user= HttpUtils.findUserByUsername(systemConst.getFindUserUrl(),sessionholder.getSession().getToken().getToken()
                ,username);
        if(user!=null){
            //如果有此用户
            userDetailMenu.start(user);
        }

    }

    public String printAndReciveChoose(){
        Scanner scanner = new Scanner(System.in);

        System.out.print("请输入用户名：");
        String username="";
        try{
            username=scanner.next();
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("输入有误哦~");
            scanner.close();
        }
        return username;
    }
}
