package com.hsf.im.menu.group;

import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.component.SystemConst;
import com.hsf.im.domain.Group;
import com.hsf.im.utils.HttpUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

/**
 * @Author: chenfz
 * 修改群详细信息页面
 * 可修改的信息有：群名称，群类目，是否需要申请加入
 */
@Component
public class ModifyGroupInfoMenu {

    Logger logger = LoggerFactory.getLogger(ModifyGroupInfoMenu.class);

    private final String APPROVE_CHOSE="y";

    @Autowired
    private SystemConst systemConst;

    @Autowired
    private Clientsessionholder clientsessionholder;

    public void start(Group group){
        Scanner scanner = new Scanner(System.in);
        System.out.println("群名称:"+group.getGroupName());
        System.out.println("是否需要修改?(y/n)");
        String choose=scanner.nextLine();
        if(APPROVE_CHOSE.equalsIgnoreCase(choose)){
            //修改群名称
            System.out.println("请输入新的群名称:");
            String newGroupName = scanner.nextLine();
            group.setGroupName(newGroupName);
        }
        //TODO 修改群类目
        int needApply=group.getNeedApply();
        System.out.println("是否需要申请验证入群:"+(needApply==1?"是":"否"));
        System.out.println("是否需要修改?(y/n)");
        choose=scanner.nextLine();
        if(APPROVE_CHOSE.equalsIgnoreCase(choose)){
            //修改群名称
            System.out.println("请输入是否需要验证入群?(y/n):");
            String isNeedApply=scanner.nextLine();
            if(APPROVE_CHOSE.equalsIgnoreCase(isNeedApply)){
                group.setNeedApply((byte)1);
            }else{
                group.setNeedApply((byte)0);
            }
        }
        System.out.println("是否需要保存修改?(y/n)");
        String isNeedSave=scanner.nextLine();
        if(APPROVE_CHOSE.equalsIgnoreCase(isNeedSave)){
            //需要保存
            HttpUtils.modifyGroupInfo(systemConst.getModifyGroupInfo(),clientsessionholder.getSession().getToken().getToken(),group);
        }
    }


}
