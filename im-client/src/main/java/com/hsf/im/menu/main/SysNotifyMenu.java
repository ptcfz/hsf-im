package com.hsf.im.menu.main;

import com.hsf.im.domain.NotifyTypeCount;
import com.hsf.im.menu.group.GroupListMenu;
import com.hsf.im.service.NotifyMsgService;
import com.hsf.im.utils.MenuFormatUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;

/**
 * @Author: chenfz
 */
@Component
public class SysNotifyMenu {

    Logger logger = LoggerFactory.getLogger(SysNotifyMenu.class);

    private boolean sys_notify_quit = false;

    List<NotifyTypeCount> notifyTypeCountList;

    @Autowired
    private NotifyMsgService notifyMsgService;

    @Autowired
    private SysNotifyListMenu sysNotifyListMenu;

    public void start(){
        sys_notify_quit=false;
        notifyTypeCountList = notifyMsgService.getNotifySumInfo();
        if(notifyTypeCountList!=null && notifyTypeCountList.size()!=0){
            //含有系统提示信息
            MenuFormatUtil.formatSysNotifyTypeInfo(notifyTypeCountList);
        }else{
            System.out.println();
            System.out.println("无系统提示信息");
            System.out.println();
        }
        while(!sys_notify_quit){
            notifyTypeCountList = notifyMsgService.getNotifySumInfo();
            String menuChoose = printAndReciveChoose();
            //TODO 查看系统提示信息
            switch (menuChoose){
                case MenuOptions.SYSNOTIFY_NEXT_PAGE:
                    //TODO 下一页
                    break;
                case MenuOptions.SYSNOTIFY_PREVIOUT_PAGE:
                    //TODO 上一页
                    break;
                case MenuOptions.SYSNOTIFY_QUIT:
                    //退出
                    sys_notify_quit=true;
                    break;
                default:
                    Boolean flag=menuChoose.matches("^[0-9]$");
                    if(!flag){
                        System.out.println("输入的选项有误哦~");
                    }else{
                        //选中某类系统信息，进入查看系统信息页面
                        //TODO
                        NotifyTypeCount notifyTypeCount = notifyTypeCountList.get(Integer.parseInt(menuChoose));
                        sysNotifyListMenu.start(notifyTypeCount);
                    }
                    break;
            }
        }
    }

    private String printAndReciveChoose(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("选中提示信息分类序号，可查看详细信息，p：上一页，n：下一页，q：返回上级页面");
        String menuChoose="";
        try{
            menuChoose=scanner.next();
        }catch (Exception e){
            logger.error(e.getMessage(),e);
            System.out.println("输入选项有误哦~");
            scanner.close();
        }
        return menuChoose;
    }
}
