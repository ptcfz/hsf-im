package com.hsf.im.menu.main;

import com.hsf.im.component.RegisterObj;
import com.hsf.im.component.SystemConst;
import com.hsf.im.utils.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
@EnableConfigurationProperties(SystemConst.class)
public class RegisterMenu {

    @Autowired
    private SystemConst systemConst;

    public void start(){
        boolean result=register();

    }

    public boolean register(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入用户名:");
        String username=scanner.next();
        System.out.println("请输入密码:");
        String password1=scanner.next();
        System.out.println("请再次输入密码:");
        String password2=scanner.next();
        if(!password1.equals(password2)){
            System.out.println("两次输入的密码不一致！");
            return false;
        }
        RegisterObj obj=new RegisterObj();
        obj.setUsername(username);
        obj.setPassword(password1);
        String result= HttpUtils.sendRegisterPost(systemConst.getRegisterUrl(),obj);
        if(result!=null){
            if("true".equalsIgnoreCase(result)){
                System.out.println("恭喜您，注册成功！");
                return true;
            }
        }
        return false;
    }
}
