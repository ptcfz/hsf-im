package com.hsf.im.menu.contact;

import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.client.CurrentTalkUserHolder;
import com.hsf.im.command.Command;
import com.hsf.im.command.chart.ChartCommands;
import com.hsf.im.domain.ClientSingleMsg;
import com.hsf.im.domain.Contact;
import com.hsf.im.domain.SingleMsg;
import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.MsgStatusEnum;
import com.hsf.im.msg.MsgTypeEnum;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.service.ContactService;
import com.hsf.im.service.SingleMsgService;
import com.hsf.im.utils.LockObjFactory;
import com.hsf.im.utils.MenuFormatUtil;
import com.hsf.im.utils.MsgClientIdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: chenfz
 */
@Component
public class ChartMenu {

    private boolean chart_quit=false;

    @Autowired
    private Clientsessionholder sessionholder;

    @Autowired
    private CurrentTalkUserHolder currentTalkUserHolder;

    @Autowired
    private ContactService contactService;

    @Autowired
    private SingleMsgService singleMsgService;

    @Autowired
    private MsgSender msgSender;

    @Autowired
    private ChartCommands chartCommands;

    public void start(Contact contact){
        chart_quit=false;
        currentTalkUserHolder.setCurrentTalkUser(contact);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("输入cmd:help可查看帮助指令，cmd为指令前缀");
        currentTalkUserHolder.setUsername(contact.getUsername());
        //打开用户会话，更新联系人的未读消息为0，并且更新该联系人的消息状态为CLIENT_READ
        ReentrantLock lock= LockObjFactory.msgRecordLock;
        lock.lock();
        try{
            //更新联系人，未读消息数量
            if(contact!=null){
                contact.setLastContectTime(sdf.format(new Date()));
                contact.setUnReadMsgCount(0);
                contactService.updateContactByUsername(contact);
            }
            //更新联系人，消息状态
            singleMsgService.updateUserMsgStatus(MsgStatusEnum.CLIENT_READ.getCode(),contact.getUsername());
        }finally {
            lock.unlock();
        }

        //TODO load出该联系人的消息展示
        List<ClientSingleMsg> singleMsgList= singleMsgService.findFromSingleMsg(contact.getUsername(),0,50);
        Collections.reverse(singleMsgList);
        MenuFormatUtil.formatUserRecentMsgAndPrint(singleMsgList,sessionholder.getSession().getUsername());
        Scanner scanner=new Scanner(System.in);

        while(!chart_quit){
            String content=scanner.nextLine();
            if(StringUtils.isEmpty(content)){
                continue;
            }
            if(content.startsWith(Command.COMMAND_PREFIX)){
                //查找命令
                String[] cmd=content.split(Command.COMMAND_SEPARATE);
                if(cmd==null || cmd.length!=2){
                    System.out.println("指令格式有误");
                    continue;
                }
                String command=cmd[1];
                if(!chartCommands.containsKey(command)){
                    System.out.println("未找到该指令");
                    continue;
                }
                chartCommands.getCommand(command).execute();
                continue;
            }
            ProtoMsg.Message msg=MsgBuilder.singleMessageRequest(MsgClientIdUtil.buildNewMsgClientId(),0,sessionholder.getSession().getUserInfo().getId(),
                    sessionholder.getSession().getUsername(), contact.getId(),contact.getUsername(),
                    new Date(), MsgTypeEnum.SINGLE_MSG_REQUEST.getCode(),content,sessionholder.getSession().getSessionId(),0);

            msgSender.sendSingleMsg(msg);
        }

    }

    public boolean isChart_quit() {
        return chart_quit;
    }

    public void setChart_quit(boolean chart_quit) {
        this.chart_quit = chart_quit;
    }
}
