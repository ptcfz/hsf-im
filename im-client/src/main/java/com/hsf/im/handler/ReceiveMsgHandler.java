package com.hsf.im.handler;

import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.MsgStatusEnum;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.msg.ResultCodeEnum;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.service.ReceiveMsgService;
import com.hsf.im.service.SingleMsgService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: chenfz
 */
@Component
@ChannelHandler.Sharable
public class ReceiveMsgHandler extends ChannelInboundHandlerAdapter {

    Logger logger= LoggerFactory.getLogger(ReceiveMsgHandler.class);

    private ArrayBlockingQueue<ProtoMsg.Message> msgQueue = new ArrayBlockingQueue<>(1000);

    private ReentrantLock queueLock = new ReentrantLock();

    public static final AttributeKey<ScheduledThreadPoolExecutor> SCHEDULE_KEY = AttributeKey.valueOf("SCHEDULE_KEY");

    ScheduledThreadPoolExecutor schedule=new ScheduledThreadPoolExecutor(1);

    @Autowired
    private ReceiveMsgService receiveMsgService;

    @Autowired
    private MsgSender msgSender;

    @Autowired
    private SingleMsgService singleMsgService;

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        schedule.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                logger.debug("queue schedule start...");
                queueLock.lock();
                try{
                    logger.debug("before queue size:"+ msgQueue.size());
                    ArrayBlockingQueue<ProtoMsg.Message> tempQueue=msgQueue;
                    msgQueue=new ArrayBlockingQueue<>(1000);
                    logger.debug("after queue size:"+msgQueue.size());
                    receiveMsgService.dealMsgQueue(tempQueue);
                }finally {
                    queueLock.unlock();
                }
            }
        },10,1, TimeUnit.SECONDS);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (null == msg || !(msg instanceof ProtoMsg.Message)) {
            super.channelRead(ctx, msg);
            return;
        }
        //判断类型
        ProtoMsg.Message pkg = (ProtoMsg.Message) msg;
        ProtoMsg.HeadType headType = ((ProtoMsg.Message) msg).getType();
        //TODO 增加其它的消息类型
        if (headType.equals(ProtoMsg.HeadType.SINGLE_MESSAGE_RESPONSE)) {
            //处理服务端对发送消息的ack
            logger.debug("receive single message response");
            singleMsgService.ackMsgByMsgClientId(pkg.getSingleMessageResponse().getMsgClientId(),Long.toString(pkg.getSingleMessageResponse().getMsgServerId()), MsgStatusEnum.CLIENT_ACK.getCode());
            return;
        }
        if (!headType.equals(ProtoMsg.HeadType.SINGLE_MESSAGE_REQUEST)) {
            super.channelRead(ctx, msg);
            return;
        }
        //到这里 接收到指定的消息类型
        logger.debug("client receive msg");
        addMsgToQueue(pkg);
        //接收到消息之后向服务端返回消息的ack
        ProtoMsg.SingleMessageRequest request=pkg.getSingleMessageRequest();
        ProtoMsg.Message ack= MsgBuilder.singleMessageResponse(true,request.getMsgClientId(),request.getMsgServerId()
                    , ResultCodeEnum.SUCCESS,pkg.getSessionId(),pkg.getSequence(),request.getFromId(),pkg.getFrom(),request.getToId(),pkg.getTo());
        msgSender.sendSingleMsgAck(ack);
    }

    private void addMsgToQueue(ProtoMsg.Message msg){
        queueLock.lock();
        try{
            logger.debug("client add msg to queue");
            boolean resule=msgQueue.offer(msg);
            if(!resule){
                //如果队列已满，则将队列传递 展示
                ArrayBlockingQueue<ProtoMsg.Message> tempQueue=msgQueue;
                msgQueue=new ArrayBlockingQueue<>(1000);
                msgQueue.offer(msg);
                receiveMsgService.dealMsgQueue(tempQueue);
            }
            logger.debug("client add msg to queue success");
        }finally {
            queueLock.unlock();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(),cause);
    }
}
