package com.hsf.im.handler;

import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.domain.NotifyMsg;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.service.NotifyMsgService;
import com.hsf.im.utils.ClientMsgBuilder;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 * 客户端接收到提示信息的处理器
 * 1：用户退群的提示信息
 */
@Component
@ChannelHandler.Sharable
public class ReceiveNotifyMsgHandler extends ChannelInboundHandlerAdapter {

    Logger logger= LoggerFactory.getLogger(ReceiveNotifyMsgHandler.class);

    @Autowired
    private NotifyMsgService notifyMsgService;

    @Autowired
    private Clientsessionholder clientsessionholder;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (null == msg || !(msg instanceof ProtoMsg.Message)) {
            super.channelRead(ctx, msg);
            return;
        }
        //判断类型
        ProtoMsg.Message pkg = (ProtoMsg.Message) msg;
        ProtoMsg.HeadType headType = ((ProtoMsg.Message) msg).getType();
        //这里指定该处理器，需要处理的消息类型，其它消息直接向后传递
        if (!headType.equals(ProtoMsg.HeadType.GROUP_QUIT_NOTIFY)) {
            super.channelRead(ctx, msg);
            return;
        }

        //到这里接收到指定的消息类型
        logger.debug("client receive notify msg");
        NotifyMsg notifyMsg = ClientMsgBuilder.buildNotifyMsg(pkg,clientsessionholder.getSession().getUserInfo().getId(),clientsessionholder.getSession().getUserInfo().getUsername());
        notifyMsgService.addNotifyMsg(notifyMsg);
    }
}
