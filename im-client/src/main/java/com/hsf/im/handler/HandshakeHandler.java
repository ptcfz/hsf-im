package com.hsf.im.handler;

import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.client.ClientSession;
import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.component.SystemConst;
import com.hsf.im.component.TokenRequestObj;
import com.hsf.im.component.TokenResponseObj;
import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.msg.ResultCodeEnum;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.utils.HttpUtils;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
@EnableConfigurationProperties(SystemConst.class)
@ChannelHandler.Sharable
public class HandshakeHandler extends ChannelInboundHandlerAdapter {

    Logger logger= LoggerFactory.getLogger(HandshakeHandler.class);

    @Autowired
    private SystemConst systemConst;

    @Autowired
    private Clientsessionholder sessionholder;

    @Autowired
    private HeartBeatHandler heartBeatHandler;

    @Autowired
    private ReceiveMsgHandler receiveMsgHandler;

    @Autowired
    private ReceiveNotifyMsgHandler receiveNotifyMsgHandler;

    @Autowired
    private MsgSender msgSender;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //判断消息实例
        if (null == msg || !(msg instanceof ProtoMsg.Message)) {
            super.channelRead(ctx, msg);
            return;
        }

        //判断类型
        ProtoMsg.Message pkg = (ProtoMsg.Message) msg;
        ProtoMsg.HeadType headType = ((ProtoMsg.Message) msg).getType();
        if(headType.equals(ProtoMsg.HeadType.CLOSE_CHANNEL)){
            ctx.channel().close();
        }
        if (!headType.equals(ProtoMsg.HeadType.HANDSHAKE_RESPONSE)) {
            super.channelRead(ctx, msg);
            return;
        }
        //判断返回是否成功
        ProtoMsg.HandshakeResponse info = pkg.getHandshakeResponse();

        ResultCodeEnum result =
                ResultCodeEnum.values()[info.getCode()];

        if (!result.equals(ResultCodeEnum.SUCCESS)) {
            logger.info("与netty server握手失败。。。关闭channel");
            //握手失败,关闭channel
            ctx.channel().close();
        } else {
            //握手成功
            logger.info("hsf-im server连接成功^_^");
            ClientSession session=sessionholder.getSession();
            session.setConnected(true);
            session.setSessionId(pkg.getSessionId());
            logger.debug("sessionId:"+session.getSessionId());
            //启动定时器 更新token
            refreshToken();
            ChannelPipeline p = ctx.pipeline();
            //移除登录响应处理器
            p.remove(this);
            p.addLast("heartBeat",heartBeatHandler);
            p.addLast(receiveMsgHandler);
            p.addLast(receiveNotifyMsgHandler);

        }

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //当通道激活，发送一个握手信息
        sessionholder.getSession().setChannel(ctx.channel());
        ProtoMsg.Message msg= MsgBuilder.handshakeRequest(0,sessionholder.getSession().getToken().getToken());
        //握手阶段不是要 msgSender发送信号，其它场合应统一使用msgSender发送消息
        ctx.channel().writeAndFlush(msg);
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }

    private void refreshToken(){
        ScheduledThreadPoolExecutor schedule=new ScheduledThreadPoolExecutor(1);
        schedule.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                TokenRequestObj tokenRequestObj=new TokenRequestObj();
                tokenRequestObj.setGrant_type("refresh_token");
                tokenRequestObj.setScope("im");
                tokenRequestObj.setRefresh_token(sessionholder.getSession().getToken().getRefresh_token());

                TokenResponseObj tokenResponseObj= HttpUtils.sendRefreshTokenPost(systemConst.getTokenUrl(),tokenRequestObj);
                if(tokenResponseObj!=null){
                    logger.debug("refresh_token success:token:"+tokenResponseObj.getToken()+",refresh_token:"+tokenResponseObj.getRefresh_token());
                    sessionholder.getSession().setToken(tokenResponseObj);
                }
            }
        },10,30, TimeUnit.MINUTES);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(),cause);
        ProtoMsg.Message cloMsg= MsgBuilder.closeChannel("0",0, SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getCode()
                ,SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getMsg());
        ctx.channel().writeAndFlush(cloMsg);
    }
}
