package com.hsf.im.handler;

import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.client.NettyClient;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.sender.MsgSender;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: chenfz
 * 发送心跳信号
 */
@Component
@ChannelHandler.Sharable
public class HeartBeatHandler extends ChannelInboundHandlerAdapter {

    Logger logger= LoggerFactory.getLogger(HeartBeatHandler.class);

    @Autowired
    private Clientsessionholder sessionholder;

    @Autowired
    private NettyClient nettyClient;

    @Autowired
    private MsgSender msgSender;

    private ScheduledThreadPoolExecutor schedule=null;


    /**
     * 最后从服务器接收到心跳信号的时间，用于判断与服务器的连接是否正常
     */
    private LocalDateTime lastHeartBeat;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if (null == msg || !(msg instanceof ProtoMsg.Message)) {
            super.channelRead(ctx, msg);
            return;
        }
        //判断类型
        ProtoMsg.Message pkg = (ProtoMsg.Message) msg;
        ProtoMsg.HeadType headType = ((ProtoMsg.Message) msg).getType();
        if(headType.equals(ProtoMsg.HeadType.CLOSE_CHANNEL)){
            //如果接收到服务端发送过来关闭通道信号，将关闭通道
            schedule.shutdownNow();
            ctx.channel().close();
        }
        if (!headType.equals(ProtoMsg.HeadType.HEART_BEAT)) {
            super.channelRead(ctx, msg);
            return;
        }
        //到此为接收到 服务端返回的 心跳信号
        //更新心跳信号最后接收时间
        this.lastHeartBeat=LocalDateTime.now();
        logger.debug("update lastHeartBeat:"+lastHeartBeat.toLocalTime());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(),cause);
        msgSender.sendCloseChannelMsg(0, SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getCode()
        ,SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getMsg());
        ctx.channel().close();
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        schedule=new ScheduledThreadPoolExecutor(1);
        lastHeartBeat=LocalDateTime.now();
        //每隔10秒发送一次心跳信号，如果超过30秒未接收到服务器返回的心跳信号，将断开连接，重新获取netty server地址连接
        schedule.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                LocalDateTime now=LocalDateTime.now();
                logger.debug("now:"+now.toLocalTime()+",lastHeartBeat:"+lastHeartBeat.toLocalTime());
                if(now.minusSeconds(30).isAfter(lastHeartBeat)){
                    logger.info("服务端心跳信号超时，将关闭通道，重新连接netty server");
                    //如果30秒没有接收到服务器返回的心跳信号，将关闭channel
                    //从account再获取其它netty server地址连接
                    //1.发送关闭channel信号，并关闭channel
                    msgSender.sendCloseChannelMsg(0,SystemExceptionEnum.CHANNEL_HEARTBEAT_SERVER_TIMEOUT_CLOSE.getCode()
                    ,SystemExceptionEnum.CHANNEL_HEARTBEAT_SERVER_TIMEOUT_CLOSE.getMsg());
                    ctx.channel().close();
                    //2.从account获取新的netty server地址
                    sessionholder.refreshNettyServerAddress();
                    //3.重新连接netty server
                    nettyClient.start(sessionholder.getSession().getNettyAddressModel());
                    schedule.shutdownNow();
                    ctx.pipeline().remove("heartBeat");
                }
                msgSender.sendHeartBearMsg();
            }
        },10,10, TimeUnit.SECONDS);
    }

}
