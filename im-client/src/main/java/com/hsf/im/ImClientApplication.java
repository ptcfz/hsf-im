package com.hsf.im;

import com.hsf.im.menu.main.MainMenu;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ImClientApplication {


    public static void main(String[] args) {
        ApplicationContext context=SpringApplication.run(ImClientApplication.class, args);
        MainMenu menuCommand=(MainMenu) context.getBean("mainMenu");
        Thread t=new Thread(new MenuTask(menuCommand));
        t.start();
        try{
            t.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

}


class MenuTask implements Runnable{

    private MainMenu menuCommand;

    public MenuTask(MainMenu menuCommand){
        this.menuCommand=menuCommand;
    }

    @Override
    public void run() {
        menuCommand.start();
    }
}
