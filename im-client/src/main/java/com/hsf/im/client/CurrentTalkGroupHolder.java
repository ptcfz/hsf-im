package com.hsf.im.client;

import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@Component
public class CurrentTalkGroupHolder {

    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
