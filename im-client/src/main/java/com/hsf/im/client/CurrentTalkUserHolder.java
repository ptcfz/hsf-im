package com.hsf.im.client;

import com.hsf.im.domain.Contact;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 * 当前正在对话对象
 */
@Component
public class CurrentTalkUserHolder {

    private String username;

    private Contact currentTalkUser;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Contact getCurrentTalkUser() {
        return currentTalkUser;
    }

    public void setCurrentTalkUser(Contact currentTalkUser) {
        this.currentTalkUser = currentTalkUser;
    }
}
