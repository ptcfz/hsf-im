package com.hsf.im.client;


import com.hsf.im.component.NettyAddressModel;
import com.hsf.im.component.TokenResponseObj;
import com.hsf.im.domain.User;
import com.hsf.im.msg.ProtoMsg;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.util.AttributeKey;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 实现客户端 Session会话
 */
@Slf4j
@Data
public class ClientSession {

    Logger logger= LoggerFactory.getLogger(ClientSession.class);

    public static final AttributeKey<ClientSession> SESSION_KEY =
            AttributeKey.valueOf("SESSION_KEY");


    /**
     * 用户实现客户端会话管理的核心
     */
    private Channel channel;

    private String username;

    /**
     * 用户account服务认证的token
     */
    private TokenResponseObj token;

    /**
     * 客户端用于连接netty服务器的地址
     */
    private NettyAddressModel nettyAddressModel;
    /**
     * 保存登录后的服务端sessionid
     */
    private String sessionId;

    private boolean isConnected = false;


    /**
     * session中存储的session 变量属性值
     */
    private Map<String, Object> map = new HashMap<String, Object>();


    private User userInfo;

    public ClientSession(){}


    //登录成功之后,设置sessionId
    public void connectSuccess(ProtoMsg.Message pkg) {
        this.sessionId=pkg.getSessionId();
        this.isConnected=true;
    }

    //获取channel
    public static ClientSession getSession(ChannelHandlerContext ctx) {
        Channel channel = ctx.channel();
        ClientSession session = channel.attr(ClientSession.SESSION_KEY).get();
        return session;
    }

    public String getRemoteAddress() {
        return channel.remoteAddress().toString();
    }

    //写protobuf 数据帧
    public ChannelFuture witeAndFlush(Object pkg) {
        ChannelFuture f = channel.writeAndFlush(pkg);
        return f;
    }

    public void writeAndClose(Object pkg) {
        ChannelFuture future = channel.writeAndFlush(pkg);
        future.addListener(ChannelFutureListener.CLOSE);
    }

    //关闭通道
    public void close() {
        isConnected = false;

        ChannelFuture future = channel.close();
        future.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture channelFuture) throws Exception {
                if(channelFuture.isSuccess()){
                    logger.info("通道已关闭。。。");
                }
            }
        });
    }

    public TokenResponseObj getToken() {
        return token;
    }

    public void setToken(TokenResponseObj token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
        channel.attr(ClientSession.SESSION_KEY).set(this);
    }

    public NettyAddressModel getNettyAddressModel() {
        return nettyAddressModel;
    }

    public void setNettyAddressModel(NettyAddressModel nettyAddressModel) {
        this.nettyAddressModel = nettyAddressModel;
    }

    public User getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(User userInfo) {
        this.userInfo = userInfo;
    }
}
