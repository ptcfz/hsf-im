package com.hsf.im.client;

import com.hsf.im.component.NettyAddressModel;
import com.hsf.im.component.SystemConst;
import com.hsf.im.utils.HttpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties(SystemConst.class)
public class Clientsessionholder {

    @Autowired
    private SystemConst systemConst;

    private ClientSession session;

    public ClientSession getSession() {
        return session;
    }

    public void setSession(ClientSession session) {
        this.session = session;
    }

    public void refreshNettyServerAddress(){
        //去account服务器获取新的netty server地址
        NettyAddressModel nettyAddressModel= HttpUtils.sendNettyServerRequest(systemConst.getRequestNettyServerUrl(),session.getToken().getToken());
        if(nettyAddressModel!=null){
            session.setNettyAddressModel(nettyAddressModel);
        }
    }
}
