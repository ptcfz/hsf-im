package com.hsf.im.client;

import com.hsf.im.component.NettyAddressModel;
import com.hsf.im.handler.HandshakeHandler;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.utils.SSLContextFactory;
import com.hsf.im.utils.ThreadPoolUtil;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.net.ssl.SSLEngine;
import java.net.InetSocketAddress;

@Component
public class NettyClient {

    Logger logger= LoggerFactory.getLogger(NettyClient.class);

    @Autowired
    private HandshakeHandler handshakeHandler;

    @Autowired
    private SSLContextFactory sslContextFactory;

    public void start(NettyAddressModel nettyAddressModel){
        if(nettyAddressModel==null){
            logger.error("启动netty client的参数nettyAddressModel不能为空。。。",new IllegalArgumentException());
        }
        InetSocketAddress inetSocketAddress=new InetSocketAddress(nettyAddressModel.getHost(),nettyAddressModel.getPort());
        ThreadPoolUtil.runTask(new Runnable() {
            @Override
            public void run() {
                startClient(inetSocketAddress);
            }
        });
    }
    private void startClient(InetSocketAddress address){
        //netty基本操作，线程组
        EventLoopGroup group = new NioEventLoopGroup();
        //netty基本操作，启动类
        Bootstrap boot = new Bootstrap();
        boot.option(ChannelOption.SO_KEEPALIVE, true)
                .option(ChannelOption.TCP_NODELAY, true)
                .group(group)
                .handler(new LoggingHandler(LogLevel.INFO))
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        pipeline.addLast(new ProtobufVarint32FrameDecoder());
                        pipeline.addLast(new ProtobufDecoder(ProtoMsg.Message.getDefaultInstance()));
                        pipeline.addLast(new ProtobufVarint32LengthFieldPrepender());
                        pipeline.addLast(new ProtobufEncoder());
                        pipeline.addLast(handshakeHandler);
                        SSLEngine engine = sslContextFactory.getSslContext().createSSLEngine();
                        engine.setUseClientMode(true);
                        pipeline.addFirst("ssl", new SslHandler(engine));
                    }
                });

        ChannelFuture f=boot.connect(address);

        try{
            f.channel().closeFuture().sync();
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
    }



}
