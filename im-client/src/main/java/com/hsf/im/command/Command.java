package com.hsf.im.command;

/**
 * @Author: chenfz
 */
public interface Command {

    public static String COMMAND_PREFIX="cmd:";

    public static String COMMAND_SEPARATE=":";

    public void execute();
}
