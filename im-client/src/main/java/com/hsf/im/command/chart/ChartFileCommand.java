package com.hsf.im.command.chart;

import com.hsf.im.command.Command;
import com.hsf.im.utils.MenuUiUtil;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@Component
public class ChartFileCommand implements Command {

    @Override
    public void execute() {
        MenuUiUtil.printNumBlankRow(2);
        MenuUiUtil.printRowSeparateLine("-",50,1);
        System.out.println("暂未实现，敬请期待^_^");
        MenuUiUtil.printRowSeparateLine("-",50,1);
        MenuUiUtil.printNumBlankRow(2);

    }
}
