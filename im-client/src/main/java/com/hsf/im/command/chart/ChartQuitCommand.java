package com.hsf.im.command.chart;

import com.hsf.im.command.Command;
import com.hsf.im.menu.contact.ChartMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@Component
public class ChartQuitCommand implements Command {

    @Autowired
    private ChartMenu chartMenu;

    @Override
    public void execute() {
        chartMenu.setChart_quit(true);
    }
}
