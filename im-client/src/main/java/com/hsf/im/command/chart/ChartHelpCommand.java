package com.hsf.im.command.chart;

import com.hsf.im.command.Command;
import com.hsf.im.command.CommandInfoModel;
import com.hsf.im.utils.MenuUiUtil;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: chenfz
 */
@Component
public class ChartHelpCommand implements Command {

    @Override
    public void execute() {
        List<CommandInfoModel> chartCmdList=ChartCommandEnum.toList();
        if(chartCmdList!=null && chartCmdList.size()!=0){
            MenuUiUtil.printNumBlankRow(1);
            MenuUiUtil.printRowSeparateLine("-",50,1);
            System.out.println("chart menu可使用的命令如下：");
        }
        for(CommandInfoModel infoModel : chartCmdList){
            System.out.println(Command.COMMAND_PREFIX+infoModel.getCode()+"   "+infoModel.getInfo());
        }
        MenuUiUtil.printRowSeparateLine("-",50,1);
        MenuUiUtil.printNumBlankRow(2);

    }
}
