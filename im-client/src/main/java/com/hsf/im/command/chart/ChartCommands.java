package com.hsf.im.command.chart;

import com.hsf.im.command.Command;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: chenfz
 */
@Component
public class ChartCommands implements InitializingBean {

    @Autowired
    private ChartHelpCommand chartHelpCommand;

    @Autowired
    private ChartQuitCommand chartQuitCommand;

    @Autowired
    private ChartFileCommand chartFileCommand;
    /**
     * 用户会话页面的可执行指令
     */
    private Map<String, Command> chartCommandMap=new HashMap<String,Command>();

    public boolean containsKey(String command){
        return chartCommandMap.containsKey(command);
    }

    public Command getCommand(String command){
        return chartCommandMap.get(command);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        chartCommandMap.put(ChartCommandEnum.HELP.getCode(),chartHelpCommand);
        chartCommandMap.put(ChartCommandEnum.QUIT.getCode(),chartQuitCommand);
        chartCommandMap.put(ChartCommandEnum.FILE.getCode(),chartFileCommand);
    }
}
