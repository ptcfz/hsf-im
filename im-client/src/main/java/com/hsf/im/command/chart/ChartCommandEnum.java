package com.hsf.im.command.chart;

import com.hsf.im.command.CommandInfoModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: chenfz
 */
public enum ChartCommandEnum {

    HELP("help","查看帮助"),
    QUIT("quit","退出当前用户会话"),
    FILE("file","收发文件");

    private String code;
    private String info;

    ChartCommandEnum(String code, String info){
        this.code=code;
        this.info=info;
    }

    public String getCode() {
        return code;
    }

    public String getInfo() {
        return info;
    }

    public static List toList() {
        List<CommandInfoModel> list = new ArrayList();

        for (ChartCommandEnum chartCommandEnum : ChartCommandEnum.values()) {
            CommandInfoModel infoModel=new CommandInfoModel();
            infoModel.setCode(chartCommandEnum.getCode());
            infoModel.setInfo(chartCommandEnum.getInfo());
            list.add(infoModel);
        }
        return list;
    }

    public static Map toMap() {
        Map<String,String> map = new HashMap<String,String>();
        for (ChartCommandEnum chartCommandEnum : ChartCommandEnum.values()) {
            map.put(chartCommandEnum.getCode(),chartCommandEnum.getInfo());
        }
        return map;
    }
}
