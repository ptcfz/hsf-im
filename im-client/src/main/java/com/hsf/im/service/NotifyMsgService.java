package com.hsf.im.service;

import com.hsf.im.domain.NotifyMsg;
import com.hsf.im.domain.NotifyTypeCount;

import java.util.List;

/**
 * @Author: chenfz
 */
public interface NotifyMsgService {

    void addNotifyMsg(NotifyMsg notifyMsg);

    List<NotifyTypeCount> getNotifySumInfo();

    List<NotifyMsg> getNotifyMsgByType(int typeId,int offset,int pageSize);

    void updateNotifyMsgStatus(List<NotifyMsg> notifyMsgList);
}
