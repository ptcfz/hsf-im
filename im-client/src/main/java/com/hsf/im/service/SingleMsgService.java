package com.hsf.im.service;

import com.hsf.im.domain.ClientSingleMsg;
import com.hsf.im.domain.SingleMsg;

import java.util.List;

/**
 * @Author: chenfz
 */
public interface SingleMsgService {

    public void addSingleMsg(ClientSingleMsg singleMsg);

    public void deleteSingleMsg(ClientSingleMsg singleMsg);

    public void updateSingleMsg(ClientSingleMsg singleMsg);

    public void ackMsgByMsgClientId(String msgClientId,String msgServerId,String status);

    public void updateUserMsgStatus(String status,String username);

    public List<ClientSingleMsg> findFromSingleMsg(String fromUsername, int skip, int limit);
}
