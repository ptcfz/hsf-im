package com.hsf.im.service;

import com.hsf.im.domain.MyDetail;

/**
 * @Author: chenfz
 */
public interface MyDetailService {

    public void addMyDetail(MyDetail myDetail);

    public void deleteMyDetailByPrimarykey(MyDetail myDetail);

    public void updateMyDetail(MyDetail myDetail);

    public MyDetail findMyDetail(String username);
}
