package com.hsf.im.service.impl;

import com.hsf.im.dao.NotifyMsgMapper;
import com.hsf.im.domain.NotifyMsg;
import com.hsf.im.domain.NotifyTypeCount;
import com.hsf.im.service.NotifyMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: chenfz
 */
@Service
public class NotifyMsgServiceImpl implements NotifyMsgService {

    @Autowired
    private NotifyMsgMapper notifyMsgMapper;

    @Override
    public void addNotifyMsg(NotifyMsg notifyMsg) {
        notifyMsgMapper.insert(notifyMsg);
    }

    @Override
    public List<NotifyTypeCount> getNotifySumInfo() {
        return notifyMsgMapper.getNotifySumInfo();
    }

    @Override
    public List<NotifyMsg> getNotifyMsgByType(int typeId, int currentPage, int pageSize) {
        int offset = currentPage*pageSize;
        return notifyMsgMapper.getNotifyMsgByType(typeId,offset,pageSize);
    }

    @Override
    public void updateNotifyMsgStatus(List<NotifyMsg> notifyMsgList) {
        notifyMsgMapper.updateNotifyMsgStatus(notifyMsgList);
    }

}
