package com.hsf.im.service.impl;

import com.hsf.im.client.Clientsessionholder;
import com.hsf.im.client.CurrentTalkUserHolder;
import com.hsf.im.component.SystemConst;
import com.hsf.im.domain.ClientSingleMsg;
import com.hsf.im.domain.Contact;
import com.hsf.im.domain.SingleMsg;
import com.hsf.im.domain.User;
import com.hsf.im.msg.MsgStatusEnum;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.service.ContactService;
import com.hsf.im.service.DisPlayMsgService;
import com.hsf.im.service.ReceiveMsgService;
import com.hsf.im.service.SingleMsgService;
import com.hsf.im.utils.ClientMsgConvertUtil;
import com.hsf.im.utils.HttpUtils;
import com.hsf.im.utils.LockObjFactory;
import com.hsf.im.utils.ThreadPoolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * @Author: chenfz
 */
@Service
public class ReceiveMsgServiceImpl implements ReceiveMsgService {

    Logger logger= LoggerFactory.getLogger(ReceiveMsgServiceImpl.class);

    @Autowired
    private CurrentTalkUserHolder currentTalkUserHolder;

    @Autowired
    private DisPlayMsgService disPlayMsgService;

    @Autowired
    private SingleMsgService singleMsgService;

    @Autowired
    private ContactService contactService;

    @Autowired
    private SystemConst systemConst;

    @Autowired
    private Clientsessionholder clientsessionholder;

    private Comparator<ProtoMsg.Message> comparator=new Comparator<ProtoMsg.Message>() {
        @Override
        public int compare(ProtoMsg.Message o1, ProtoMsg.Message o2) {
            if(o1.getMsgServerId()>o2.getMsgServerId()){
                return 1;
            }else if(o1.getMsgServerId()<o2.getMsgServerId()){
                return -1;
            }else{
                return 0;
            }
        }
    };

    @Override
    public void dealMsgQueue(ArrayBlockingQueue<ProtoMsg.Message> msgQueue) {
        if(msgQueue == null || msgQueue.size()==0){
            return;
        }
        ThreadPoolUtil.runTask(new DealMsgTask(msgQueue));
    }


    class DealMsgTask implements Runnable{

        private ArrayBlockingQueue<ProtoMsg.Message> msgQueue;

        public DealMsgTask(ArrayBlockingQueue<ProtoMsg.Message> msgQueue){
            this.msgQueue=msgQueue;
        }

        @Override
        public void run() {
            List<ProtoMsg.Message> msgList=new ArrayList<>(msgQueue.size());
            Iterator<ProtoMsg.Message> it=msgQueue.iterator();
            while(it.hasNext()){
                ProtoMsg.Message msg=it.next();
                msgList.add(msg);
            }
            Map<String,List<ProtoMsg.Message>> msgMap=msgList.stream().collect(Collectors.groupingBy(t ->t.getFrom()));
            if(msgMap.containsKey(currentTalkUserHolder.getUsername())){
                //展示当前正在对话 的内容
                logger.debug("receive current talk user:"+currentTalkUserHolder.getUsername());
                List<ProtoMsg.Message> disMsgList=msgMap.get(currentTalkUserHolder.getUsername());
                Collections.sort(disMsgList, comparator);
                disPlayMsgService.displayMsg(disMsgList);
            }
            for(Map.Entry entry : msgMap.entrySet()){
                String fromUsername=(String)entry.getKey();
                List<ProtoMsg.Message> userMsgList=(List<ProtoMsg.Message>)entry.getValue();
                //消息 客户端本地保存
                if(userMsgList!=null && userMsgList.size()!=0){
                    if(fromUsername.equals(currentTalkUserHolder.getUsername())){
                        dealMsgListWithLock(userMsgList,fromUsername);
                    }else{
                        dealMsgListWithoutLock(userMsgList,fromUsername,MsgStatusEnum.CLIENT_ACK.getCode());
                    }
                }
            }
        }

        private void dealMsgListWithLock(List<ProtoMsg.Message> userMsgList,String fromUsername){
            //1:查找联系人列表
            ReentrantLock lock= LockObjFactory.msgRecordLock;
            lock.lock();
            try{
                dealMsgListWithoutLock(userMsgList,fromUsername,MsgStatusEnum.CLIENT_READ.getCode());
            }finally {
                lock.unlock();
            }

        }

        private void dealMsgListWithoutLock(List<ProtoMsg.Message> userMsgList,String fromUsername,String status){
            Contact contact=contactService.findContactByUsername(fromUsername);
            if(contact!=null){
                contact.setUnReadMsgCount(contact.getUnReadMsgCount()+userMsgList.size());
                contactService.updateContactByUsername(contact);
            }else{
                //原本没有该联系人，新增一个联系人
                User user = HttpUtils.findUserByUsername(systemConst.getFindUserUrl(),clientsessionholder.getSession().getToken().getToken()
                        ,fromUsername);
                System.out.println("add newe Contact:"+user.toString());
                if(user != null){
                    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    contact=new Contact();
                    contact.setId(user.getId());
                    contact.setGender(user.getGender());
                    contact.setBak(user.getBak());
                    contact.setAddress(user.getAddress());
                    contact.setMobile(user.getMobile());
                    contact.setUsername(fromUsername);
                    contact.setUnReadMsgCount(userMsgList.size());
                    contact.setLastContectTime(sdf.format(new Date()));
                    contactService.addContact(contact);
                }
            }
            for(ProtoMsg.Message msg: userMsgList){
                ProtoMsg.SingleMessageRequest msgRequest=msg.getSingleMessageRequest();
                ClientSingleMsg singleMsg= ClientMsgConvertUtil.singleMsgRequest2SingleMsg(msgRequest);
                singleMsg.setStatus(status);
                singleMsgService.addSingleMsg(singleMsg);
            }
        }
    }
}


