package com.hsf.im.service.impl;

import com.hsf.im.dao.MyDetailMapper;
import com.hsf.im.domain.MyDetail;
import com.hsf.im.service.MyDetailService;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

/**
 * @Author: chenfz
 */
@Service
public class MyDetailServiceImpl implements MyDetailService {

    @Mapper
    private MyDetailMapper myDetailMapper;

    @Override
    public void addMyDetail(MyDetail myDetail) {
        myDetailMapper.insert(myDetail);
    }

    @Override
    public void deleteMyDetailByPrimarykey(MyDetail myDetail) {
        myDetailMapper.deleteByPrimaryKey(myDetail.getId());
    }

    @Override
    public void updateMyDetail(MyDetail myDetail) {
        myDetailMapper.updateByPrimaryKey(myDetail);
    }

    @Override
    public MyDetail findMyDetail(String username) {
        return myDetailMapper.selectByUsername(username);
    }
}
