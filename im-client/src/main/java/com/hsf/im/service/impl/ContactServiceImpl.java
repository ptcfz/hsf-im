package com.hsf.im.service.impl;

import com.hsf.im.dao.ContactMapper;
import com.hsf.im.domain.Contact;
import com.hsf.im.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: chenfz
 */
@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    private ContactMapper contactMapper;

    @Override
    public void addContact(Contact contact) {
        contactMapper.insert(contact);
    }

    @Override
    public void updateContactByUsername(Contact contact) {
        contactMapper.updateByUsername(contact);
    }

    @Override
    public Contact findContactByUsername(String username) {
        return contactMapper.findByUsername(username);
    }

    @Override
    public List<Contact> findRecentContact(int limit, int offset){
        return contactMapper.findRecentContact(limit,offset);
    }
}
