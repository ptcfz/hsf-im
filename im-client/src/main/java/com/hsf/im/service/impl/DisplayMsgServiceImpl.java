package com.hsf.im.service.impl;

import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.service.DisPlayMsgService;
import com.hsf.im.utils.MenuFormatUtil;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: chenfz
 */
@Component
public class DisplayMsgServiceImpl implements DisPlayMsgService {

    @Override
    public void displayMsg(List<ProtoMsg.Message> msgList) {
        for(ProtoMsg.Message message : msgList){
            MenuFormatUtil.formatAndPrintSingleMsg(message);
        }
    }
}
