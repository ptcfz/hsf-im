package com.hsf.im.service;

import com.hsf.im.msg.ProtoMsg;

import java.util.List;

/**
 * @Author: chenfz
 */
public interface DisPlayMsgService {

    public void displayMsg(List<ProtoMsg.Message> msgList);

}
