package com.hsf.im.service.impl;

import com.hsf.im.dao.SingleMsgMapper;
import com.hsf.im.domain.ClientSingleMsg;
import com.hsf.im.domain.SingleMsg;
import com.hsf.im.service.SingleMsgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: chenfz
 */
@Service
public class SingleMsgServiceImpl implements SingleMsgService {

    Logger logger= LoggerFactory.getLogger(SingleMsgServiceImpl.class);

    @Autowired
    private SingleMsgMapper singleMsgMapper;

    @Override
    public void addSingleMsg(ClientSingleMsg singleMsg) {
        singleMsgMapper.insert(singleMsg);
    }

    @Override
    public void deleteSingleMsg(ClientSingleMsg singleMsg) {
        singleMsgMapper.deleteByPrimaryKey(singleMsg.getMsgClientId());
    }

    @Override
    public void updateSingleMsg(ClientSingleMsg singleMsg) {
        singleMsgMapper.updateByPrimaryKey(singleMsg);
    }

    @Override
    public void ackMsgByMsgClientId(String msgClientId,String msgServerId,String status){
        logger.debug("ack message msgClientId:"+msgClientId+",msgServerId:"+msgServerId+",status:"+status);
        singleMsgMapper.ackMsgByMsgClientId(msgClientId,msgServerId,status);
    }

    @Override
    public void updateUserMsgStatus(String status,String username){
        singleMsgMapper.updateUserMsgStatus(status,username);
    }

    @Override
    public List<ClientSingleMsg> findFromSingleMsg(String fromUsername, int skip, int limit) {
        return singleMsgMapper.findFromSingleMsg(fromUsername,skip,limit);
    }
}
