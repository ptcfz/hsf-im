package com.hsf.im.service;

import com.hsf.im.domain.Contact;

import java.util.List;

/**
 * @Author: chenfz
 */
public interface ContactService {

    public void addContact(Contact contact);

    public void updateContactByUsername(Contact contact);

    public Contact findContactByUsername(String username);

    public List<Contact> findRecentContact(int limit,int offset);
}
