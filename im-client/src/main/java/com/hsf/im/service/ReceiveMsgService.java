package com.hsf.im.service;

import com.hsf.im.msg.ProtoMsg;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @Author: chenfz
 */
public interface ReceiveMsgService {

    public void dealMsgQueue(ArrayBlockingQueue<ProtoMsg.Message> msgQueue);
}
