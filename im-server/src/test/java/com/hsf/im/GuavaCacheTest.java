package com.hsf.im;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author: chenfz
 */
public class GuavaCacheTest {

    public static void main(String[] args)throws Exception{
        testLoadingCache();
    }

    public static void testCache() throws Exception{
        Cache<String,String> cache= CacheBuilder.newBuilder()
                .maximumSize(100000)
                .expireAfterWrite(5, TimeUnit.SECONDS)
                .build();


        cache.put("abc","efg");

        System.out.println("get key abc from cache...");
        String value=cache.get("abc", new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("...");
                return ".....";
            }
        });
        if(value!=null){
            System.out.println("abc:"+value);
        }else{
            System.out.println("abc is null");
        }
        Thread.sleep(12000);
        System.out.println("get key abc from cache again...");
        value=null;
        value=cache.get("abc", new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("...");
                return ".....";
            }
        });
        if(value!=null){
            System.out.println("abc:"+value);
        }else{
            System.out.println("abc is null");
        }
    }

    public static void testLoadingCache() throws Exception{
        AtomicInteger ai=new AtomicInteger(0);
        LoadingCache<String,String> cache=CacheBuilder.newBuilder()
                .maximumSize(10000)
                .expireAfterWrite(5,TimeUnit.SECONDS)
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String s) throws Exception {
                        System.out.println("call load:"+ai.intValue());
                        ai.incrementAndGet();
                        return "hello world!"+ai.intValue();
                    }
                });

        System.out.println(cache.get("abc"));
        System.out.println(cache.get("abc"));
        Thread.sleep(60000);
        System.out.println(cache.get("abc"));
        System.out.println(cache.get("abc"));
        Thread.sleep(60000);
        System.out.println(cache.get("abc"));
        System.out.println(cache.get("abc"));
    }
}
