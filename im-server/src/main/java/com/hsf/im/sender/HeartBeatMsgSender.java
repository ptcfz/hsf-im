package com.hsf.im.sender;

import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.server.UserServerSession;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@Component
public class HeartBeatMsgSender extends BaseSender {

    public void sendHeartBearMsg(UserServerSession serverSession){
        super.setUserServerSession(serverSession);
        ProtoMsg.Message msg=MsgBuilder.heartBeat(serverSession.getUser().getUsername(),serverSession.getSessionId(),0);
        sendMsg(msg);
    }
}
