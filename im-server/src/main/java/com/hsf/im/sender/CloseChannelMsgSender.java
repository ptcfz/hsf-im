package com.hsf.im.sender;

import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.server.NodeServerSession;
import com.hsf.im.server.UserServerSession;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@Component
public class CloseChannelMsgSender extends BaseSender{

    /**
     * 关闭客户端与server之间的通道
     * @param serverSession
     * @param code
     * @param info
     */
    public void sendCloseChannelMsg(UserServerSession serverSession,String code,String info){
        super.setUserServerSession(serverSession);
        ProtoMsg.Message msg= MsgBuilder.closeChannel(serverSession.getSessionId(),0,code,info);
        sendMsg(msg);
    }

    /**
     * 关闭server节点之间的通道
     * @param nodeServerSession
     * @param code
     * @param info
     */
    public void sendCloseNodeChannelMsg(NodeServerSession nodeServerSession,String code,String info){
        super.setNodeServerSession(nodeServerSession);
        ProtoMsg.Message msg=MsgBuilder.closeChannel(nodeServerSession.getSessionId(),0,code,info);
        sendNodeMsg(msg);
    }
}
