package com.hsf.im.sender;

import com.hsf.im.Exception.IllegalMsgException;
import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.Exception.UnSupportMsgException;
import com.hsf.im.component.SingleMsgChannelRouteHolder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.route.SingleMsgChannelRouteItem;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @Author: chenfz
 * 单聊消息的转发器
 */
@Component
public class SingleMsgSender {

    Logger logger= LoggerFactory.getLogger(SingleMsgSender.class);

    /**
     * 一对一消息的路由表
     */
    @Autowired
    private SingleMsgChannelRouteHolder singleMsgChannelRouteHolder;

    /**
     * 转发一对一 消息
     * @param message
     * @throws UnSupportMsgException
     * @throws IllegalMsgException
     */
    public void transmitMsg(ProtoMsg.Message message) throws UnSupportMsgException,IllegalMsgException{
        ProtoMsg.HeadType type=message.getType();
        if((!type.equals(ProtoMsg.HeadType.SINGLE_MESSAGE_REQUEST)) && (!type.equals(ProtoMsg.HeadType.SINGLE_MESSAGE_RESPONSE))){
            //如果不是一对一单聊的 消息
            UnSupportMsgException e=new UnSupportMsgException(SystemExceptionEnum.MESSAGE_UNSUPPORT_TYPE.getCode(),SystemExceptionEnum.MESSAGE_UNSUPPORT_TYPE.getMsg());
            logger.error(e.getMessage(),e);
            throw e;
        }
        String toUsername=message.getSingleMessageRequest().getTo();
        if(StringUtils.isEmpty(toUsername)){
            IllegalMsgException e=new IllegalMsgException(SystemExceptionEnum.MESSAGE_ARGUMENT_TOUSER_NULL.getCode()
                    ,SystemExceptionEnum.MESSAGE_ARGUMENT_TOUSER_NULL.getMsg());
            logger.error(e.getMsg(),e);
            throw e;
        }
        String fromUsername=message.getSingleMessageRequest().getFrom();
        if(StringUtils.isEmpty(fromUsername)){
            IllegalMsgException e=new IllegalMsgException(SystemExceptionEnum.MESSAGE_ARGUMENT_FROMUSER_NULL.getCode()
                    ,SystemExceptionEnum.MESSAGE_ARGUMENT_FROMUSER_NULL.getMsg());
            logger.error(e.getMsg(),e);
            throw e;
        }

        Channel channel=singleMsgChannelRouteHolder.getToUserChannel(toUsername);
        if(channel==null){
            //用户已离线，消息离线存储
            logger.debug("用户："+toUsername+" 已离线，消息离线存储");
            return;
        }
        //转发消息
        ChannelFuture future=channel.writeAndFlush(message);
        future.addListener(new GenericFutureListener<Future<? super Void>>() {
            @Override
            public void operationComplete(Future<? super Void> future) throws Exception {
                if(future.isSuccess()){
                    logger.debug("消息转发成功");
                }else{
                    logger.error(future.cause().getMessage(),future.cause());
                }
            }
        });
        //更新一下fromUser的路由表
        SingleMsgChannelRouteItem item=new SingleMsgChannelRouteItem();
        item.setToUsername(fromUsername);
        item.setToChannel(singleMsgChannelRouteHolder.getToUserChannel(fromUsername));
        singleMsgChannelRouteHolder.setOrUpdateItem(fromUsername,item);
    }


}
