package com.hsf.im.sender;

import com.hsf.im.component.ZnodePathHolder;
import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.server.NodeServerSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@Component
public class NodeHeartBeatMsgSender extends BaseSender{

    @Autowired
    private ZnodePathHolder znodePathHolder;

    public void sendNodeHeartBeatMsg(NodeServerSession nodeServerSession){
        super.setNodeServerSession(nodeServerSession);
        ProtoMsg.Message msg= MsgBuilder.nodeHeartBeatMsg(znodePathHolder.getZnodePath());
        sendNodeMsg(msg);
    }
}
