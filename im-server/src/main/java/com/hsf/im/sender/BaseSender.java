package com.hsf.im.sender;

import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.server.NodeServerSession;
import com.hsf.im.server.UserServerSession;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseSender {

    Logger logger= LoggerFactory.getLogger(BaseSender.class);

    private UserServerSession userServerSession;

    private NodeServerSession nodeServerSession;

    /**
     * 客户端--netty server之间发送消息
     * 应使用该方法
     * @param msg
     */
    public void sendMsg(ProtoMsg.Message msg){

        Channel channel = userServerSession.getChannel();
        ChannelFuture f = channel.writeAndFlush(msg);
        f.addListener(new GenericFutureListener<Future<? super Void>>() {
            @Override
            public void operationComplete(Future<? super Void> future)
                    throws Exception {
                // 回调
                if (future.isSuccess()) {
                    logger.debug("msg send success...");

                } else {
                    logger.error("msg send faild:"+future.cause().getMessage(),future.cause());
                }
            }

        });
    }

    /**
     * netty server---netty server之间发送信号
     * 应使用该方法
     * @param msg
     */
    public void sendNodeMsg(ProtoMsg.Message msg){

        Channel channel = nodeServerSession.getChannel();
        ChannelFuture f = channel.writeAndFlush(msg);
        f.addListener(new GenericFutureListener<Future<? super Void>>() {
            @Override
            public void operationComplete(Future<? super Void> future)
                    throws Exception {
                // 回调
                if (future.isSuccess()) {
                    logger.debug("node msg send success...");

                } else {
                    logger.error("node msg send faild:"+future.cause().getMessage(),future.cause());
                }
            }

        });
    }


    public UserServerSession getUserServerSession() {
        return userServerSession;
    }

    public void setUserServerSession(UserServerSession userServerSession) {
        this.userServerSession = userServerSession;
    }

    public NodeServerSession getNodeServerSession() {
        return nodeServerSession;
    }

    public void setNodeServerSession(NodeServerSession nodeServerSession) {
        this.nodeServerSession = nodeServerSession;
    }
}
