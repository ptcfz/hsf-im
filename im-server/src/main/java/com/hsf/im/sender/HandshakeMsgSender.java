package com.hsf.im.sender;

import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.msg.ResultCodeEnum;
import com.hsf.im.server.UserServerSession;
import org.springframework.stereotype.Component;

@Component
public class HandshakeMsgSender extends BaseSender {

    public void sendHandshakeResponseMsg(UserServerSession serverSession,long seqId){
        super.setUserServerSession(serverSession);
        ProtoMsg.Message msg= MsgBuilder.handshakeResponse(ResultCodeEnum.SUCCESS,serverSession.getSessionId(),seqId);
        sendMsg(msg);
    }
}
