package com.hsf.im.sender;

import com.hsf.im.component.NodeServerSessionHolder;
import com.hsf.im.component.UserServerSessionHolder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.server.NodeServerSession;
import com.hsf.im.server.UserServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @Author: chenfz
 */
@Component
public class MsgSender {

    Logger logger= LoggerFactory.getLogger(MsgSender.class);

    @Autowired
    private UserServerSessionHolder userServerSessionHolder;

    @Autowired
    private NodeServerSessionHolder nodeServerSessionHolder;

    @Autowired
    private HeartBeatMsgSender heartBeatMsgSender;

    @Autowired
    private HandshakeMsgSender handshakeMsgSender;

    @Autowired
    private CloseChannelMsgSender closeChannelMsgSender;

    @Autowired
    private NodeHeartBeatMsgSender nodeHeartBeatMsgSender;

    @Autowired
    private SingleMsgSender singleMsgSender;

    @Autowired
    private SingleNotifyMsgSender singleNotifyMsgSender;


    public void sendHeartBeatMsg(String username){
        if(StringUtils.isEmpty(username)){
            logger.error("发送心跳信号失败,用户名不能为空。。。");
            return;
        }
        UserServerSession serverSession=userServerSessionHolder.getNodeSession(username);
        heartBeatMsgSender.sendHeartBearMsg(serverSession);
    }

    public void sendCloseChannelMsg(String username,String code,String info){
        if(StringUtils.isEmpty(username)){
            logger.error("发送关闭通道信号失败,用户名不能为空。。。");
            return;
        }
        UserServerSession serverSession=userServerSessionHolder.getNodeSession(username);
        closeChannelMsgSender.sendCloseChannelMsg(serverSession,code,info);
    }

    /**
     * 发送 关闭节点之间channel 的信号
     * @param znodePath 要关闭节点 的znodepath
     */
    public void sendCloseNodeChannelMsg(String znodePath,String code,String info){
        if(StringUtils.isEmpty(znodePath)){
            logger.error("发送关闭节点之间通道信号失败，节点路径不能为空。。。");
            return;
        }
        NodeServerSession nodeServerSession=nodeServerSessionHolder.getNodeSession(znodePath);
        if(nodeServerSession!=null){
            closeChannelMsgSender.sendCloseNodeChannelMsg(nodeServerSession,code,info);
        }
    }

    /**
     * 对方节点的znodePath
     * @param znodePath
     */
    public void sendNodeHeartBeatMsg(String znodePath){
        if(StringUtils.isEmpty(znodePath)){
            logger.error("发送节点心跳信号失败,节点的znodePath不能为空。。。");
            return;
        }
        nodeHeartBeatMsgSender.sendNodeHeartBeatMsg(nodeServerSessionHolder.getNodeSession(znodePath));
    }

    public void sendHandshakeMsg(String username){
        if(StringUtils.isEmpty(username)){
            logger.error("发送握手信号失败,用户名不能为空。。。");
            return;
        }
        UserServerSession userServerSession=userServerSessionHolder.getNodeSession(username);
        handshakeMsgSender.sendHandshakeResponseMsg(userServerSession,0);
    }

    /**
     * 发送 一对一 单聊 信息
     * @param message
     */
    public void sendSingleMsg(ProtoMsg.Message message){
        singleMsgSender.transmitMsg(message);
    }

    /**
     * 转发一对一的提示消息
     * @param message
     */
    public void sendSingleNotifyMsg(ProtoMsg.Message message){
        singleNotifyMsgSender.transmitMsg(message);
    }
}
