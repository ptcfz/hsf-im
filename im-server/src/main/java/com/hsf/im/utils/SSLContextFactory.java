package com.hsf.im.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;

/**
 * @Author: chenfz
 */
@Component
public class SSLContextFactory implements InitializingBean {

    private SSLContext SSL_CONTEXT_S ;

    private SSLContext SSL_CONTEXT_C ;

    @Value("${ssl.sPkPath}")
    private String sPkPath;

    @Value("${ssl.cPkPath}")
    private String cPkPath;

    public SSLContext getSslContexts(){
        return SSL_CONTEXT_S ;
    }
    public SSLContext getSslContextc(){
        return SSL_CONTEXT_C ;
    }

    private TrustManager[] getTrustManagersServer(){
        InputStream is = null ;
        KeyStore ks = null ;
        TrustManagerFactory keyFac = null ;

        TrustManager[] kms = null ;
        try {
            // 获得KeyManagerFactory对象. 初始化位默认算法
            keyFac = TrustManagerFactory.getInstance("SunX509") ;
            is =new FileInputStream(sPkPath);
            ks = KeyStore.getInstance("JKS") ;
            String keyStorePass = "sNetty" ;
            ks.load(is , keyStorePass.toCharArray()) ;
            keyFac.init(ks) ;
            kms = keyFac.getTrustManagers() ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            if(is != null ){
                try {
                    is.close() ;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return kms ;
    }
    private TrustManager[] getTrustManagersClient(){
        InputStream is = null ;
        KeyStore ks = null ;
        TrustManagerFactory keyFac = null ;

        TrustManager[] kms = null ;
        try {
            // 获得KeyManagerFactory对象. 初始化位默认算法
            keyFac = TrustManagerFactory.getInstance("SunX509") ;
            is =new FileInputStream(cPkPath);
            ks = KeyStore.getInstance("JKS") ;
            String keyStorePass = "sNetty" ;
            ks.load(is , keyStorePass.toCharArray()) ;
            keyFac.init(ks) ;
            kms = keyFac.getTrustManagers() ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            if(is != null ){
                try {
                    is.close() ;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return kms ;
    }
    private KeyManager[] getKeyManagersServer(){
        InputStream is = null ;
        KeyStore ks = null ;
        KeyManagerFactory keyFac = null ;

        KeyManager[] kms = null ;
        try {
            // 获得KeyManagerFactory对象. 初始化位默认算法
            keyFac = KeyManagerFactory.getInstance("SunX509") ;
            is =new FileInputStream(sPkPath);
            ks = KeyStore.getInstance("JKS") ;
            String keyStorePass = "sNetty" ;
            ks.load(is , keyStorePass.toCharArray()) ;
            keyFac.init(ks, keyStorePass.toCharArray()) ;
            kms = keyFac.getKeyManagers() ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            if(is != null ){
                try {
                    is.close() ;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return kms ;
    }
    private KeyManager[] getKeyManagersClient(){
        InputStream is = null ;
        KeyStore ks = null ;
        KeyManagerFactory keyFac = null ;

        KeyManager[] kms = null ;
        try {
            // 获得KeyManagerFactory对象. 初始化位默认算法
            keyFac = KeyManagerFactory.getInstance("SunX509") ;
            is =new FileInputStream(cPkPath);
            ks = KeyStore.getInstance("JKS") ;
            String keyStorePass = "sNetty" ;
            ks.load(is , keyStorePass.toCharArray()) ;
            keyFac.init(ks, keyStorePass.toCharArray()) ;
            kms = keyFac.getKeyManagers() ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
            if(is != null ){
                try {
                    is.close() ;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return kms ;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        SSLContext sslContexts = null ;
        SSLContext sslContextc = null ;
        try {
            sslContexts = SSLContext.getInstance("SSLv3") ;
            sslContextc = SSLContext.getInstance("SSLv3") ;
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        try{
            if(getKeyManagersServer() != null && getTrustManagersServer() != null ){
                sslContexts.init(getKeyManagersServer(), getTrustManagersServer(), null);
            }
            if(getKeyManagersClient() != null && getTrustManagersClient() != null){
                sslContextc.init(getKeyManagersClient(), getTrustManagersClient(), null);
            }

        }catch(Exception e){
            e.printStackTrace() ;
        }
        sslContexts.createSSLEngine().getSupportedCipherSuites() ;
        sslContextc.createSSLEngine().getSupportedCipherSuites() ;
        SSL_CONTEXT_S = sslContexts ;
        SSL_CONTEXT_C = sslContextc ;
    }
}
