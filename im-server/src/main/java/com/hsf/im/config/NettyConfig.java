package com.hsf.im.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "zookeeper")
public class NettyConfig {

    /**
     * zookeeper集群地址
     */
    private String address;

    /**
     * 节点注册的父目录
     */
    private String nettyParentNode;

    /**
     * 节点名称前缀
     */
    private String workNode;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNettyParentNode() {
        return nettyParentNode;
    }

    public void setNettyParentNode(String nettyParentNode) {
        this.nettyParentNode = nettyParentNode;
    }

    public String getWorkNode() {
        return workNode;
    }

    public void setWorkNode(String workNode) {
        this.workNode = workNode;
    }
}
