package com.hsf.im.config;

import net.sf.ehcache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EhcacheConfig {

    @Bean
    public CacheManager cacheManager(){
        CacheManager cacheManager=CacheManager.create();
        return cacheManager;
    }
}
