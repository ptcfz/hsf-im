package com.hsf.im.dao;

import com.hsf.im.domain.GroupApplyMsg;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GroupApplyMsgMapper {
    int deleteByPrimaryKey(Long id);

    int insert(GroupApplyMsg record);

    int insertSelective(GroupApplyMsg record);

    GroupApplyMsg selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GroupApplyMsg record);

    int updateByPrimaryKey(GroupApplyMsg record);
}