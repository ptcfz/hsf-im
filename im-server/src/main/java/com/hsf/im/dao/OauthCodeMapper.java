package com.hsf.im.dao;

import com.hsf.im.domain.OauthCode;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OauthCodeMapper {
    int insert(OauthCode record);

    int insertSelective(OauthCode record);
}