package com.hsf.im.dao;

import com.hsf.im.domain.SingleMsg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SingleMsgMapper {

    int deleteByPrimaryKey(Long msgServerId);

    int insert(SingleMsg record);

    int insertSelective(SingleMsg record);

    SingleMsg selectByPrimaryKey(Long msgServerId);

    int updateByPrimaryKeySelective(SingleMsg record);

    int updateByPrimaryKey(SingleMsg record);

    void ackSingleMsgByMsgServerId(@Param("toId") long toId, @Param("msgServerId") long msgServerId);
}