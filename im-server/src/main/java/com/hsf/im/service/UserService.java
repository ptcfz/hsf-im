package com.hsf.im.service;

import com.hsf.im.domain.User;

public interface UserService {

    public User selectByUserName(String username);
}
