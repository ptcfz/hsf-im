package com.hsf.im.service;

import com.hsf.im.domain.SingleMsg;

/**
 * @Author: chenfz
 */
public interface SingleMsgService {

    int addSingleMsg(SingleMsg singleMsg);

    void ackSingleMsgByMsgServerId(long toId,long msgServerId);
}
