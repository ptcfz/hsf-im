package com.hsf.im.service;

import com.hsf.im.msg.ProtoMsg;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.net.InetSocketAddress;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * netty server节点之间的互联，负载消息在节点之间路由
 */
public class NettyConnect {

    /**
     * 执行连接的线程池,maxinumPoolSize的值，即最多连接多少节点
     */
    private ExecutorService executor=new ThreadPoolExecutor(2,1000,60, TimeUnit.SECONDS,new ArrayBlockingQueue<>(1));

    public void doConnect(InetSocketAddress address){
        ConnectTask task=new ConnectTask(address);
        executor.submit(task);
    }

}

class ConnectTask implements Runnable{

    /**
     * 远程节点地址
     */
    private InetSocketAddress address;

    public ConnectTask(InetSocketAddress address){
        this.address=address;
    }

    @Override
    public void run() {
        //netty基本操作，线程组
        EventLoopGroup group = new NioEventLoopGroup();
        //netty基本操作，启动类
        Bootstrap boot = new Bootstrap();
        boot.option(ChannelOption.SO_KEEPALIVE, true)
                .option(ChannelOption.TCP_NODELAY, true)
                .group(group)
                .handler(new LoggingHandler(LogLevel.INFO))
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        pipeline.addLast(new ProtobufVarint32FrameDecoder());
                        pipeline.addLast(new ProtobufDecoder(ProtoMsg.Message.getDefaultInstance()));
                        pipeline.addLast(new ProtobufVarint32LengthFieldPrepender());
                        pipeline.addLast(new ProtobufEncoder());
                        //TODO 添加连接节点的业务处理器，消息转发\登记路由表等
                    }
                });

        ChannelFuture f=boot.connect(address);

        try{
            f.channel().closeFuture().sync();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
