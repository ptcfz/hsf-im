package com.hsf.im.service;

import com.hsf.im.client.ImForNodeClient;
import com.hsf.im.component.NodeServerSessionHolder;
import com.hsf.im.component.ZkPayloadNode;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.server.NodeServerSession;
import com.hsf.im.utils.ProtostuffUtil;
import com.hsf.im.utils.ZookeeperClientFactory;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class ZookeeperService implements InitializingBean {

    Logger logger= LoggerFactory.getLogger(ZookeeperService.class);

    @Value("${zookeeper.address}")
    private String address;

    //nodeServerSessionHolder内保存着 znodepath----channel的关系
    @Autowired
    private NodeServerSessionHolder nodeServerSessionHolder;

    @Autowired
    private MsgSender msgSender;

    @Autowired
    private ImForNodeClient imForNodeClient;

    private CuratorFramework client;

    /**
     * 创建持久顺序节点
     * @param path
     * @param payload
     */
    public String createPersistentSeqNode(String path,byte[] payload){
        return createNode(path,payload,CreateMode.PERSISTENT_SEQUENTIAL);
    }

    /**
     * 创建持久非顺序节点
     * @param path
     * @param payload
     */
    public String createPersistentNode(String path,byte[] payload){
        return createNode(path,payload,CreateMode.PERSISTENT);
    }

    /**
     * 创建临时顺序节点
     * @param path
     * @param payload
     */
    public String createEphemeralSeqNode(String path,byte[] payload){
        return createNode(path,payload,CreateMode.EPHEMERAL_SEQUENTIAL);
    }

    /**
     * 创建临时节点
     * @param path
     * @param payload
     */
    public String createEphemeralNode(String path,byte[] payload){
        return createNode(path,payload,CreateMode.EPHEMERAL);
    }


    /**
     * 获取节点的子节点
     * @param parentPath
     * @return
     */
    public List<String> getNodeChildren(String parentPath){
        try{
            Stat stat=client.checkExists().forPath(parentPath);
            if(stat!=null){
                List<String> nodes=client.getChildren().forPath(parentPath);
                return nodes;
            }
            return null;
        }catch (Exception e){
            logger.error("get node children error:"+parentPath,e);
        }
        return null;
    }

    /**
     * 获取节点的负载
     * @param path
     * @return
     */
    public byte[] getNodePayload(String path){
        try{
            if(StringUtils.isEmpty(path)){
                return null;
            }
            Stat stat=client.checkExists().forPath(path);
            if(stat!=null){
                byte[] payload=client.getData().forPath(path);
                return payload;
            }
            return null;
        }catch (Exception e){
            logger.error("get node data error:"+path,e);
        }
        return null;
    }

    /**
     * 更新节点
     * @param path
     * @param payload
     */
    public void updateNode(String path,byte[] payload){
        try{
            client.setData().forPath(path,payload);
        }catch (Exception e){
            logger.error("update node :"+path+",error.",e);
        }
    }


    public boolean checkNodeExist(String path){
        try{
            Stat stat=client.checkExists().forPath(path);
            if(stat!=null){
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return false;
    }

    public void deleteNode(String path){
        try{
            client.delete().forPath(path);
        }catch (Exception e){
            logger.error("delete node:"+path+" error.",e);
        }
    }

    /**
     * 当zk上netty server节点的子节点有变化，即有新节点加入  或 退出
     * @param path
     */
    public void createNettyChildrenNodeListener(String path){
        try{
            logger.info("create netty children node listener:"+path);
            PathChildrenCache cache=new PathChildrenCache(client,path,true);
            PathChildrenCacheListener listener=new PathChildrenCacheListener() {
                @Override
                public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
                    switch (pathChildrenCacheEvent.getType()){
                        case CHILD_ADDED:
                            //处理节点新增事件，即与新的netty server节点建立连接
                            byte[] payload=pathChildrenCacheEvent.getData().getData();
                            ZkPayloadNode zkPayloadNode= ProtostuffUtil.deserializer(payload,ZkPayloadNode.class);
                            String host=zkPayloadNode.getHost();
                            Integer port=zkPayloadNode.getNodePort();
                            logger.info("监听到有新的netty server节点加入：host:"+host+",port:"+port);
                            imForNodeClient.start(host,port);
                            break;
                        case CHILD_REMOVED:
                            //处理节点移除事件，即与移除的netty节点断开连接
                            String path=pathChildrenCacheEvent.getData().getPath();
                            //TODO 看看这个path的值
                            logger.info("监听到netty server节点："+path+" 移除事件。。。");
                            NodeServerSession nodeServerSession=nodeServerSessionHolder.getNodeSession(path);
                            if(nodeServerSession!=null){
                                nodeServerSession.getChannel().close();
                                //TODO 是否手动过期
                                //nodeServerSessionHolder里的值 自己会过期
                                //nodeServerSessionHolder.removeNodeSession(path);
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
            cache.getListenable().addListener(listener);
            cache.start(PathChildrenCache.StartMode.BUILD_INITIAL_CACHE);
        }catch (Exception e){
            logger.error("创建zk节点:"+path+"的监听器失败",e);
        }
    }

    private String createNode(String path, byte[] payload, CreateMode mode){
        try{
            return client.create().creatingParentsIfNeeded().withMode(mode).forPath(path,payload);
        }catch (Exception e){
            logger.error("create node path:"+path+" error",e);
        }
        return null;
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        client= ZookeeperClientFactory.createFramework(address);
        client.start();
    }
}
