package com.hsf.im.service.impl;

import com.hsf.im.dao.UserMapper;
import com.hsf.im.domain.User;
import com.hsf.im.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectByUserName(String username) {
        User user=userMapper.selectByUserName(username);
        return user;
    }
}
