package com.hsf.im.service.impl;

import com.hsf.im.dao.SingleMsgMapper;
import com.hsf.im.domain.SingleMsg;
import com.hsf.im.service.SingleMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: chenfz
 */
@Service
public class SingleMsgServiceImpl implements SingleMsgService {

    @Autowired
    private SingleMsgMapper singleMsgMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int addSingleMsg(SingleMsg singleMsg) {
        return singleMsgMapper.insert(singleMsg);
    }

    @Override
    public void ackSingleMsgByMsgServerId(long toId,long msgServerId) {
        System.out.println("server ack single msg,toId:"+toId+",msgServerId:"+msgServerId);
        singleMsgMapper.ackSingleMsgByMsgServerId(toId,msgServerId);
    }
}
