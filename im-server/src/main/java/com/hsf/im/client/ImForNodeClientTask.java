package com.hsf.im.client;

import com.hsf.im.handler.NodeClientHandshakeHandler;
import com.hsf.im.msg.ProtoMsg;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32FrameDecoder;
import io.netty.handler.codec.protobuf.ProtobufVarint32LengthFieldPrepender;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import java.net.InetSocketAddress;

/**
 * @Author: chenfz
 */
public class ImForNodeClientTask implements Runnable{

    Logger logger= LoggerFactory.getLogger(ImForNodeClientTask.class);

    private InetSocketAddress address;

    private NodeClientHandshakeHandler iNodeClientHandshakeHandler;

    private SSLContext sslContext;

    public ImForNodeClientTask(InetSocketAddress address, NodeClientHandshakeHandler iNodeClientHandshakeHandler,SSLContext sslContext){
        this.address=address;
        this.iNodeClientHandshakeHandler=iNodeClientHandshakeHandler;
        this.sslContext=sslContext;
    }

    @Override
    public void run() {
        logger.info("im for node client task begin run method...");
        //netty基本操作，线程组
        EventLoopGroup group = new NioEventLoopGroup();
        //netty基本操作，启动类
        Bootstrap boot = new Bootstrap();
        boot.option(ChannelOption.SO_KEEPALIVE, true)
                .option(ChannelOption.TCP_NODELAY, true)
                .group(group)
                .handler(new LoggingHandler(LogLevel.INFO))
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        pipeline.addLast(new ProtobufVarint32FrameDecoder());
                        pipeline.addLast(new ProtobufDecoder(ProtoMsg.Message.getDefaultInstance()));
                        pipeline.addLast(new ProtobufVarint32LengthFieldPrepender());
                        pipeline.addLast(new ProtobufEncoder());
                        pipeline.addLast("nodeHandshakeClient",iNodeClientHandshakeHandler);
                        SSLEngine engine = sslContext.createSSLEngine();
                        engine.setUseClientMode(true);
                        pipeline.addFirst("ssl", new SslHandler(engine));
                    }
                });

        ChannelFuture f=boot.connect(address);
        f.addListener(new GenericFutureListener<Future<? super Void>>() {
            @Override
            public void operationComplete(Future<? super Void> future) throws Exception {
                if(future.isSuccess()){
                    logger.info("im for node client task连接新节点成功。。。");
                }else{
                    Throwable e=future.cause();
                    logger.error(e.getMessage(),e);
                }
            }
        });
        try{
            f.channel().closeFuture().sync();
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
    }
}
