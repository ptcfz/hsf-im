package com.hsf.im.client;

import com.hsf.im.handler.NodeClientHandshakeHandler;
import com.hsf.im.utils.SSLContextFactory;
import com.hsf.im.utils.ThreadPoolUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.net.InetSocketAddress;

/**
 * @Author: chenfz
 * 用于连接其它netty server节点的client
 */
@Component
public class ImForNodeClient {

    Logger logger= LoggerFactory.getLogger(ImForNodeClient.class);

    @Autowired
    private NodeClientHandshakeHandler iNodeClientHandshakeHandler;

    @Autowired
    private SSLContextFactory sslContextFactory;

    public void start(String host,Integer port){
        logger.info("启动im for node client去连接新节点：host:"+host+",port:"+port);
        if(StringUtils.isEmpty(host) || port==null){
            logger.error("启动ImForNodeClient的参数host、port不能为空。。。",new IllegalArgumentException());
        }
        InetSocketAddress inetSocketAddress=new InetSocketAddress(host,port);

        ThreadPoolUtil.runTask(new ImForNodeClientTask(inetSocketAddress,iNodeClientHandshakeHandler,sslContextFactory.getSslContextc()));
    }

}
