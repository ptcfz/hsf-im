package com.hsf.im.server;

import com.hsf.im.domain.User;
import io.netty.channel.Channel;
import io.netty.util.AttributeKey;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 用户channel 服务端的Session会话
 */
public class UserServerSession {

    public static final AttributeKey<UserServerSession> SESSION_KEY =
            AttributeKey.valueOf("USER_SESSION_KEY");

    /**
     * 连接通道
     */
    private Channel channel;
    /**
     * 用户
     */
    private User user;
    /**
     * sessionId
     */
    private final String sessionId;

    /**
     * 最后一次接收到客户端心跳信号的时间
     */
    private LocalDateTime lastHeartBeatTime;
    /**
     * session中存储的session 变量属性值
     */
    private Map<String, Object> map = new HashMap<String, Object>();




    public UserServerSession(Channel channel) {
        this.channel = channel;
        this.sessionId = buildNewSessionId();
    }

    /**
     * 关闭连接
     */
    public void closeSession() {

    }

    public String getSessionId() {
        return sessionId;
    }

    private String buildNewSessionId() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replaceAll("-", "");
    }

    public synchronized void set(String key, Object value) {
        map.put(key, value);
    }


    public synchronized <T> T get(String key) {
        return (T) map.get(key);
    }


    public boolean isValid() {
        return getUser() != null ? true : false;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public LocalDateTime getLastHeartBeatTime() {
        return lastHeartBeatTime;
    }

    public void setLastHeartBeatTime(LocalDateTime lastHeartBeatTime) {
        this.lastHeartBeatTime = lastHeartBeatTime;
    }
}
