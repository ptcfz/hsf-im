package com.hsf.im.server;

import io.netty.channel.Channel;
import io.netty.util.AttributeKey;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @Author: chenfz
 * netty server节点之间 channel 服务端的session
 */
public class NodeServerSession {

    public static final AttributeKey<NodeServerSession> SESSION_KEY =
            AttributeKey.valueOf("NODE_SESSION_KEY");

    //对方节点的znodepath
    private String znodePath;

    //跟此节点的channel
    private Channel channel;

    private String sessionId;

    private LocalDateTime lastHeartBeatTime;

    public NodeServerSession(String znodePath,Channel channel){
        this.znodePath=znodePath;
        this.channel=channel;
        this.sessionId=buildNewSessionId();
    }

    public String buildNewSessionId() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replaceAll("-", "");
    }

    public String getZnodePath() {
        return znodePath;
    }

    public void setZnodePath(String znodePath) {
        this.znodePath = znodePath;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public LocalDateTime getLastHeartBeatTime() {
        return lastHeartBeatTime;
    }

    public void setLastHeartBeatTime(LocalDateTime lastHeartBeatTime) {
        this.lastHeartBeatTime = lastHeartBeatTime;
    }
}
