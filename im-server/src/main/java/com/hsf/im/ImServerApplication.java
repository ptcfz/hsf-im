package com.hsf.im;

import com.hsf.im.component.ZkPayloadNode;
import com.hsf.im.component.ZnodePathHolder;
import com.hsf.im.config.NettyConfig;
import com.hsf.im.server.ImForNodeServer;
import com.hsf.im.server.ImForUserServer;
import com.hsf.im.service.ZookeeperService;
import com.hsf.im.utils.NetUtil;
import com.hsf.im.utils.ProtostuffUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.util.StringUtils;

import java.util.concurrent.CountDownLatch;

@SpringBootApplication
@EnableConfigurationProperties(NettyConfig.class)
public class ImServerApplication {

    static Logger logger= LoggerFactory.getLogger(ImServerApplication.class);

    public static int IM_FOR_USER_SERVER_PORT=9090;
    public static int IM_FOR_NODE_SERVER_PORT=9091;
    public static double SERVER_FACTOR=100.0;

    public static String NETTY_PARENT_NODE_PATH="/im/server/nettyNode";
    public static String NETTY_NODE_PATH="/im/server/nettyNode/nettyServer";

    @Autowired
    private NettyConfig nettyConfig;

    public static void main(String[] args) {
        CountDownLatch cdl=new CountDownLatch(2);
        //当im for user和im for node都启动之后再创建Znode，避免其它节点连不上的情况
        CountDownLatch createZnodeCdl=new CountDownLatch(2);
        ApplicationContext applicationContext=SpringApplication.run(ImServerApplication.class, args);
        ImForUserServer imForUserServer=(ImForUserServer) applicationContext.getBean("imForUserServer");
        ImForNodeServer imForNodeServer=(ImForNodeServer) applicationContext.getBean("imForNodeServer");
        ZookeeperService zookeeperService=(ZookeeperService)applicationContext.getBean("zookeeperService");
        ZnodePathHolder znodePathHolder=(ZnodePathHolder)applicationContext.getBean("znodePathHolder");
        if(imForUserServer==null){
            logger.error("imForUserServer is null");
            return;
        }
        if(imForNodeServer==null){
            logger.error("imForNodeServer is null");
            return;
        }
        Thread t=new Thread(new RunImForUserServer(imForUserServer,IM_FOR_USER_SERVER_PORT,cdl,createZnodeCdl));
        t.start();
        Thread t2=new Thread(new RunImForNodeServer(imForNodeServer,IM_FOR_NODE_SERVER_PORT,cdl,createZnodeCdl));
        t2.start();
        //向zookeeper注册临时顺序节点
        try{
            createZnodeCdl.await();
        }catch (InterruptedException e){
            logger.error(e.getMessage(),e);
        }
        ZkPayloadNode zkPayloadNode=new ZkPayloadNode();
        zkPayloadNode.setConNum(0);
        //TODO 这个系数修改为可配置
        zkPayloadNode.setFactor(SERVER_FACTOR);
        zkPayloadNode.setPort(IM_FOR_USER_SERVER_PORT);
        zkPayloadNode.setNodePort(IM_FOR_NODE_SERVER_PORT);
        String host= NetUtil.getHostIp();
        if(StringUtils.isEmpty(host)){
            logger.error("获取本机ip地址出错。。。");
            return;
        }
        zkPayloadNode.setHost(host);
        byte[] payload= ProtostuffUtil.serializer(zkPayloadNode);
        String path=zookeeperService.createEphemeralSeqNode(NETTY_NODE_PATH,payload);
        znodePathHolder.setZnodePath(path);
        logger.info("Create znode:"+path+" success!");
        //向zookeeper的netty节点添加监听器
        zookeeperService.createNettyChildrenNodeListener(NETTY_PARENT_NODE_PATH);
        try{
            cdl.await();
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
    }

}

class RunImForUserServer implements Runnable{

    Logger logger=LoggerFactory.getLogger(RunImForUserServer.class);

    private CountDownLatch cdl;
    private CountDownLatch znodeCdl;
    private ImForUserServer imForUserServer;

    private int port;

    public RunImForUserServer(ImForUserServer imForUserServer, int port,CountDownLatch cdl,CountDownLatch znodeCdl){
        this.imForUserServer=imForUserServer;
        this.port=port;
        this.cdl=cdl;
        this.znodeCdl=znodeCdl;
    }

    @Override
    public void run() {
        try{
            imForUserServer.startServer(port,znodeCdl);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }finally {
            cdl.countDown();
        }
    }
}

class RunImForNodeServer implements Runnable{

    Logger logger=LoggerFactory.getLogger(RunImForUserServer.class);

    private CountDownLatch cdl;
    private CountDownLatch znodeCdl;
    private ImForNodeServer imForNodeServer;

    private int port;

    public RunImForNodeServer(ImForNodeServer imForNodeServer, int port, CountDownLatch cdl,CountDownLatch znodeCdl){
        this.imForNodeServer=imForNodeServer;
        this.port=port;
        this.cdl=cdl;
        this.znodeCdl=znodeCdl;
    }

    @Override
    public void run() {
        try{
            imForNodeServer.startServer(port,znodeCdl);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }finally {
            cdl.countDown();
        }
    }
}

