package com.hsf.im.auth;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hsf.im.utils.EncoderUtil;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class AuthRequest {

    private static int SUCCESS_CODE=200;

    public static AuthResult auth(String authTokenUrl, String name, String password, String token){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();

        HttpPost httpPost=new HttpPost(authTokenUrl+token);
        String authStr= EncoderUtil.encoderBase64(name+":"+password);
        httpPost.setHeader("Authorization","Basic "+authStr);

        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE != statusCode){
                AuthResult result=new AuthResult();
                result.setActive(false);
                return result;
            }
            System.out.println("响应状态为："+response.getStatusLine());
           // System.out.println("响应内容为："+ EntityUtils.toString(responseEntity));
            JSONObject obj= JSON.parseObject(EntityUtils.toString(responseEntity));
            if(obj!=null){
                AuthResult result=new AuthResult();
                result.setActive(obj.getBoolean("active"));
                result.setUsername(obj.getString("user_name"));
                return result;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
