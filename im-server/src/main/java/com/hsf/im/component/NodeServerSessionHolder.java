package com.hsf.im.component;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.hsf.im.server.NodeServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @Author: chenfz
 * 本地 与其它 netty server channel的 session缓存
 */
@Component
public class NodeServerSessionHolder implements InitializingBean {

    Logger logger= LoggerFactory.getLogger(NodeServerSessionHolder.class);

    /**
     * 保存本地节点 到 其它netty server节点的channel
     * key:所连接节点的znode名称
     * value：此次连接的NodeServerSession
     */
    private Cache<String,NodeServerSession> cache;

    /**
     * 根据nodePath 找到这个节点的channel
     * @param nodePath  节点名称
     * @return
     */
    public NodeServerSession getNodeSession(String nodePath){
        try{
            return cache.get(nodePath, new Callable<NodeServerSession>() {
                @Override
                public NodeServerSession call() throws Exception {
                    return null;
                }
            });
        }catch (ExecutionException e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    /**
     *
     * @param nodePath  所连接节点名称
     * @param session   到此节点的channel
     */
    public void setNodeSession(String nodePath,NodeServerSession session){
        cache.put(nodePath,session);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        cache= CacheBuilder.newBuilder()
                .maximumSize(1000000)
                .expireAfterAccess(60, TimeUnit.SECONDS)
                .build();
    }
}
