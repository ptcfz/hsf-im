package com.hsf.im.component;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.hsf.im.route.RouteTableItem;
import com.hsf.im.route.SingleMsgChannelRouteItem;
import com.hsf.im.server.NodeServerSession;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @Author: chenfz
 * 单聊消息 在 本节点的路由 表
 * fromchannel --tochannel
 */
@Component
public class SingleMsgChannelRouteHolder implements InitializingBean {

    Logger logger= LoggerFactory.getLogger(SingleMsgChannelRouteHolder.class);

    @Autowired
    private ZnodePathHolder znodePathHolder;

    @Autowired
    private RouteTableHolder routeTableHolder;

    @Autowired
    private LocalClientChannelRouteHolder localClientChannelRouteHolder;

    @Autowired
    private NodeServerSessionHolder nodeServerSessionHolder;

    private LoadingCache<String, SingleMsgChannelRouteItem> singleMsgRouteTable;


    public Channel getToUserChannel(String toUsername){
        try{
            SingleMsgChannelRouteItem item=singleMsgRouteTable.get(toUsername);
            if(item==null || item.getToChannel()==null){
                return null;
            }
            return item.getToChannel();
        }catch (ExecutionException e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    public void setOrUpdateItem(String username,SingleMsgChannelRouteItem item){
        singleMsgRouteTable.put(username,item);
    }

    //TODO 修改当查到不到缓存项的时候，返回一个空对象，要不然会报错
    @Override
    public void afterPropertiesSet() throws Exception {
        singleMsgRouteTable= CacheBuilder.newBuilder()
                .maximumSize(10000000)
                .expireAfterWrite(30, TimeUnit.SECONDS)
                .build(new CacheLoader<String, SingleMsgChannelRouteItem>() {
                    @Override
                    public SingleMsgChannelRouteItem load(String toUsername) throws Exception {
                        if(StringUtils.isEmpty(toUsername)){
                            return buildEmptyRouteItem(toUsername);
                        }
                        RouteTableItem routeTableItem=routeTableHolder.getTableItem(toUsername);
                        if(routeTableItem==null){
                            //全局路由表里面没有此用户，说明不在线
                            return buildEmptyRouteItem(toUsername);
                        }
                        String toZonePath=routeTableItem.getZnodePath();
                        if(StringUtils.isEmpty(toZonePath)){
                            //全局路由表里面没有此用户，说明不在线
                            return buildEmptyRouteItem(toUsername);
                        }
                        SingleMsgChannelRouteItem item=new SingleMsgChannelRouteItem();
                        if(toZonePath.equalsIgnoreCase(znodePathHolder.getZnodePath())){
                            //说明toUser 也是连接在此netty server节点上，从localClientChannelRouteHolder可以获取到用户channel
                            Channel channel= localClientChannelRouteHolder.getUserChannel(toUsername);
                            if(channel==null){
                                //说明此用户下线了
                                return buildEmptyRouteItem(toUsername);
                            }
                            logger.info("toUsername:"+toUsername+",znodepath:"+toZonePath+",remoteAddress:"+channel.remoteAddress());
                            item.setToChannel(channel);
                            item.setToUsername(toUsername);
                        }else{
                            //说明此用户，不是连接在此节点上。从nodeServerSessionHolder查找用户所在的节点，返回到此节点的channel
                            NodeServerSession nodeServerSession=nodeServerSessionHolder.getNodeSession(toZonePath);
                            if(nodeServerSession==null){
                                //说明此时，这个节点可能已经掉线了
                                return buildEmptyRouteItem(toUsername);
                            }
                            Channel channel=nodeServerSession.getChannel();
                            logger.info("toUsername:"+toUsername+",znodepath:"+toZonePath+",remoteAddress:"+channel.remoteAddress());
                            item.setToUsername(toUsername);
                            item.setToChannel(channel);
                        }
                        return item;
                    }
                });
    }

    private SingleMsgChannelRouteItem buildEmptyRouteItem(String username){
        SingleMsgChannelRouteItem singleMsgChannelRouteItem = new SingleMsgChannelRouteItem();
        singleMsgChannelRouteItem.setToUsername(username);
        return singleMsgChannelRouteItem;
    }
}
