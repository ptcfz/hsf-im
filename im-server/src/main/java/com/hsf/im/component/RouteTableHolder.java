package com.hsf.im.component;

import com.hsf.im.route.RouteTable;
import com.hsf.im.route.RouteTableItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 全局用户--节点路由表
 * 保存着所有用户--netty server节点的连接关系
 */
@Component
public class RouteTableHolder {

    @Autowired
    private RouteTable routeTable;

    public void setTableItem(String username,String nodePath){
        routeTable.setTableItem(username,nodePath);
    }

    public RouteTableItem getTableItem(String username){
        return routeTable.getTableItem(username);
    }


}
