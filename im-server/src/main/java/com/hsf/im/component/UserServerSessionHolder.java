package com.hsf.im.component;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.hsf.im.server.UserServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @Author: chenfz
 * key: username
 * value: UserServerSession
 */
@Component
public class UserServerSessionHolder implements InitializingBean {

    Logger logger= LoggerFactory.getLogger(UserServerSessionHolder.class);

    private Cache<String,UserServerSession> cache;

    public long getSessionHolderSize(){
        return cache.size();
    }

    public UserServerSession getNodeSession(String username){
        try{
            return cache.get(username, new Callable<UserServerSession>() {
                @Override
                public UserServerSession call() throws Exception {
                    return null;
                }
            });
        }catch (ExecutionException e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    public void setNodeSession(String username,UserServerSession userServerSession){
        cache.put(username,userServerSession);
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        cache= CacheBuilder.newBuilder()
                .maximumSize(1000000)
                .expireAfterAccess(30, TimeUnit.SECONDS)
                .build();
    }
}
