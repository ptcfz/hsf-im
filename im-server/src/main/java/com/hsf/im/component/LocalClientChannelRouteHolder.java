package com.hsf.im.component;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.hsf.im.route.ClientChannelRouteItem;
import com.hsf.im.server.UserServerSession;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @Author: chenfz
 * 本地  client--channel 的路由表
 * 只存储连接在这个节点的client
 */
@Component
public class LocalClientChannelRouteHolder implements InitializingBean {

    Logger logger= LoggerFactory.getLogger(LocalClientChannelRouteHolder.class);


    @Autowired
    private UserServerSessionHolder userServerSessionHolder;


    private LoadingCache<String, ClientChannelRouteItem> clientRouteTable;


    public Channel getUserChannel(String username){
        try{
            return clientRouteTable.get(username).getChannel();
        }catch (ExecutionException e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        clientRouteTable= CacheBuilder.newBuilder()
                .maximumSize(1000000)
                .expireAfterWrite(60, TimeUnit.SECONDS)
                .build(new CacheLoader<String, ClientChannelRouteItem>() {
                    @Override
                    public ClientChannelRouteItem load(String username) throws Exception {
                        ClientChannelRouteItem item=new ClientChannelRouteItem();
                        item.setUsername(username);
                        UserServerSession userServerSession=userServerSessionHolder.getNodeSession(username);
                        if(userServerSession!=null){
                            item.setChannel(userServerSession.getChannel());
                            return item;
                        }
                        return null;
                    }
                });
    }
}
