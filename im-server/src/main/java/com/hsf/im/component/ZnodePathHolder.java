package com.hsf.im.component;

import com.hsf.im.service.ZookeeperService;
import com.hsf.im.utils.ProtostuffUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class ZnodePathHolder implements InitializingBean {

    Logger logger= LoggerFactory.getLogger(ZnodePathHolder.class);

    @Autowired
    private ZookeeperService zookeeperService;

    /**
     * 节点上的netty server连接数
     */
    private AtomicLong nodeConNum=new AtomicLong(0);

    /**
     * 表示此netty server在zookeerer上注册的节点路径
     */
    private String znodePath;

    public void incrConNum(){
        nodeConNum.getAndIncrement();
    }

    public void decConNum(){
        nodeConNum.getAndDecrement();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        //每60秒钟，本地的连接数与zookeeper上的同步
        ScheduledThreadPoolExecutor schedule=new ScheduledThreadPoolExecutor(1);
        schedule.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                logger.info("同步本地znode连接数："+nodeConNum.get());
                byte[] payload=zookeeperService.getNodePayload(znodePath);
                if(payload!=null){
                    ZkPayloadNode payloadNode= ProtostuffUtil.deserializer(payload,ZkPayloadNode.class);
                    payloadNode.setConNum(nodeConNum.get());
                    zookeeperService.updateNode(znodePath,ProtostuffUtil.serializer(payloadNode));
                }
            }
        },15,5,TimeUnit.SECONDS);
    }

    public String getZnodePath() {
        return znodePath;
    }

    public void setZnodePath(String znodePath) {
        this.znodePath = znodePath;
    }
}
