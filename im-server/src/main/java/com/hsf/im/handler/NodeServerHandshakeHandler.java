package com.hsf.im.handler;

import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.component.NodeServerSessionHolder;
import com.hsf.im.component.ZnodePathHolder;
import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.msg.ResultCodeEnum;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.server.NodeServerSession;
import com.hsf.im.service.ZookeeperService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 * 节点之间握手处理器---被动连接端
 */
@Component
@ChannelHandler.Sharable
public class NodeServerHandshakeHandler extends SimpleChannelInboundHandler<ProtoMsg.Message> {

    Logger logger= LoggerFactory.getLogger(NodeServerHandshakeHandler.class);

    private final String NODE_SERVER_HEART_BEAT_KEY="NODE_SERVER_HEART_BEAT_HANDLER";
    private final String MSG_TRANSMIT_KEY="MSG_TRANSMIT_KEY";

    @Autowired
    private NodeServerSessionHolder nodeServerSessionHolder;

    @Autowired
    private ZookeeperService zookeeperService;

    @Autowired
    private ZnodePathHolder znodePathHolder;

    @Autowired
    private NodeServerHeartBeatHandler nodeServerHeartBeatHandler;

    @Autowired
    private MsgSender msgSender;

    @Autowired
    private MsgTransmiterHandler msgTransmiterHandler;


    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, ProtoMsg.Message message) throws Exception {
        logger.info("inode server handshake handler接收到信号。。。");
        ProtoMsg.HeadType headType = message.getType();
        logger.info("headType:"+headType.name());
        //如果接收到对方 发送过来的关闭channel信号，将关闭channel
        if (headType.equals(ProtoMsg.HeadType.CLOSE_CHANNEL)) {
            channelHandlerContext.channel().close();
        }

        if (!headType.equals(ProtoMsg.HeadType.NODE_HANDSHAKE_REQUEST)) {
            return;
        }
        logger.info("接收到 对方 发送的 握手信号：NODE_HANDSHAKE_REQUEST...");
        //到这里为 接收到对方 发送的握手信号
        //校验对方发送过来的节点路径是否存在，存在 则建立连接，否则关闭通道
        if(!zookeeperService.checkNodeExist(message.getNodeHandshakeRequest().getNodepath())){
            logger.info("节点之间建立连接，节点路径："+message.getNodeHandshakeRequest().getNodepath()+" 不存在，通道将关闭。。。");
            ProtoMsg.Message closeMsg= MsgBuilder.closeChannelBeforeSession(0);
            channelHandlerContext.channel().writeAndFlush(closeMsg);
            channelHandlerContext.channel().close();
            return;
        }
        //创建NodeServerSession,并存到nodeServerSessionHolder内
        logger.info("生成NodeServerSession:"+message.getNodeHandshakeRequest().getNodepath());
        NodeServerSession serverSession=new NodeServerSession(message.getNodeHandshakeRequest().getNodepath(),channelHandlerContext.channel());
        nodeServerSessionHolder.setNodeSession(serverSession.getZnodePath(),serverSession);
        //将session与channel绑定
        channelHandlerContext.channel().attr(NodeServerSession.SESSION_KEY).set(serverSession);
        //返回对方 握手信号响应
        ProtoMsg.Message msg= MsgBuilder.nodeHandshakeResponseMsg(znodePathHolder.getZnodePath(),true, ResultCodeEnum.SUCCESS.getCode()
                , ResultCodeEnum.SUCCESS.getDesc(),serverSession.getSessionId());

        channelHandlerContext.channel().writeAndFlush(msg);
        channelHandlerContext.pipeline().addLast(NODE_SERVER_HEART_BEAT_KEY,nodeServerHeartBeatHandler);

        channelHandlerContext.pipeline().remove(this);
        //TODO 添加消息转发器
        channelHandlerContext.pipeline().addLast(MSG_TRANSMIT_KEY,msgTransmiterHandler);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(),cause);
        msgSender.sendCloseNodeChannelMsg(ctx.channel().attr(NodeServerSession.SESSION_KEY).get().getZnodePath(),
                SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getCode(),SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getMsg());
        ctx.channel().close();
    }
}
