package com.hsf.im.handler;

import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.component.NodeServerSessionHolder;
import com.hsf.im.component.ZnodePathHolder;
import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.server.NodeServerSession;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 * 节点之间的握手处理器---主动连接端
 */
@Component
@ChannelHandler.Sharable
public class NodeClientHandshakeHandler extends SimpleChannelInboundHandler<ProtoMsg.Message> {

    Logger logger= LoggerFactory.getLogger(NodeClientHandshakeHandler.class);

    private final String NODE_CLIENT_HEART_BEAT_KEY="NODE_CLIENT_HEART_BEAT_HANDLER";
    private final String MSG_TRANSMIT_KEY="MSG_TRANSMIT_KEY";

    @Autowired
    private ZnodePathHolder znodePathHolder;

    @Autowired
    private NodeServerSessionHolder nodeServerSessionHolder;

    @Autowired
    private NodeClientHeartBeatHandler nodeClientHeartBeatHandler;

    @Autowired
    private MsgTransmiterHandler msgTransmiterHandler;

    @Autowired
    private MsgSender msgSender;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //当通道激活的时候，连接端 主动发送握手信号
        logger.info("连接端主动发送握手信号。。。");
        ProtoMsg.Message msg= MsgBuilder.nodeHandshakeRequestMsg(znodePathHolder.getZnodePath());
        ctx.channel().writeAndFlush(msg);
    }


    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, ProtoMsg.Message message) throws Exception {
        //取得请求类型
        ProtoMsg.HeadType headType = message.getType();
        logger.info("response headType:"+headType.name());
        //如果接收到对方 发送过来的关闭channel信号，将关闭channel
        if (headType.equals(ProtoMsg.HeadType.CLOSE_CHANNEL)) {
            logger.info("node client handshake handler接收到对方发送过来的 CLOSE_CHANNEL信号，将关闭通道。。。");
            channelHandlerContext.channel().close();
        }
        if (!headType.equals(ProtoMsg.HeadType.NODE_HANDSHAKE_RESPONSE)) {
            return;
        }
        //到这里为 接收到对方 对握手信号的 响应
        logger.info("对方 返回NODE_HANDSHAKE_RESPONSE...");
        NodeServerSession serverSession=new NodeServerSession(message.getNodeHandshakeResponse().getNodepath(),channelHandlerContext.channel());
        serverSession.setSessionId(message.getNodeHandshakeResponse().getSessionId());
        nodeServerSessionHolder.setNodeSession(serverSession.getZnodePath(),serverSession);
        channelHandlerContext.channel().attr(NodeServerSession.SESSION_KEY).set(serverSession);
        channelHandlerContext.pipeline().addLast(NODE_CLIENT_HEART_BEAT_KEY,nodeClientHeartBeatHandler);
        channelHandlerContext.pipeline().remove(this);
        //TODO 消息转发器
        channelHandlerContext.pipeline().addLast(MSG_TRANSMIT_KEY,msgTransmiterHandler);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(),cause);
        msgSender.sendCloseNodeChannelMsg(ctx.channel().attr(NodeServerSession.SESSION_KEY).get().getZnodePath()
        , SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getCode(),SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getMsg());
        ctx.channel().close();
    }
}
