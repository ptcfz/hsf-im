package com.hsf.im.handler;

import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.component.UserServerSessionHolder;
import com.hsf.im.component.ZnodePathHolder;
import com.hsf.im.domain.SingleMsg;
import com.hsf.im.model.RocketMqConst;
import com.hsf.im.mq.producer.producer.ImDefaultMQProducer;
import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.msg.ResultCodeEnum;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.server.UserServerSession;
import com.hsf.im.service.SingleMsgService;
import com.hsf.im.utils.HttpUtils;
import com.hsf.im.utils.MsgConvertUtil;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: chenfz
 */
@Component
@ChannelHandler.Sharable
public class MsgServerIdHandler extends ChannelInboundHandlerAdapter {

    Logger logger= LoggerFactory.getLogger(MsgServerIdHandler.class);

    private static ThreadPoolExecutor poolExecutor=new ThreadPoolExecutor(10,1000,60, TimeUnit.SECONDS,new ArrayBlockingQueue<>(1000));

    private ImDefaultMQProducer msgProducer ;

    @Value("${mq.rocketmq.nameSrvAddr}")
    private String rocketMqNameSrvAddr;

    @Value("${system.msgIdUrl}")
    private String msgIdUrl;

    @Autowired
    private ZnodePathHolder znodePathHolder;

    @Autowired
    private UserServerSessionHolder userServerSessionHolder;

    @Autowired
    private MsgSender msgSender;

    @Autowired
    private SingleMsgService singleMsgService;


    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        msgProducer= new ImDefaultMQProducer(RocketMqConst.SINGLE_MSG_PRODUCER_GROUP,rocketMqNameSrvAddr);
        if(msgProducer!=null){
            logger.info("成功创建 msgProducer");
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (null == msg || !(msg instanceof ProtoMsg.Message)) {
            super.channelRead(ctx, msg);
            return;
        }
        //判断类型
        ProtoMsg.Message pkg = (ProtoMsg.Message) msg;
        ProtoMsg.HeadType headType = ((ProtoMsg.Message) msg).getType();
        //不需要为消息赋值id的消息类型，直接向后传递
        if (!headType.equals(ProtoMsg.HeadType.SINGLE_MESSAGE_REQUEST)) {
            super.channelRead(ctx, msg);
            return;
        }
        //到这里接收到 msg
        //使用统一的id生成服务，避免不同im-server节点之间时间不同步，造成id乱序
        long msgServerId= HttpUtils.getMsgServerId(msgIdUrl);

        ProtoMsg.SingleMessageRequest request =pkg.getSingleMessageRequest().newBuilderForType()
                .setMsgServerId(msgServerId)
                .setMsgClientId(pkg.getSingleMessageRequest().getMsgClientId())
                .setFrom(pkg.getSingleMessageRequest().getFrom())
                .setFromId(pkg.getSingleMessageRequest().getFromId())
                .setTo(pkg.getSingleMessageRequest().getTo())
                .setToId(pkg.getSingleMessageRequest().getToId())
                .setTime(pkg.getSingleMessageRequest().getTime())
                .setMsgType(pkg.getSingleMessageRequest().getMsgType())
                .setContent(pkg.getSingleMessageRequest().getContent()).build();
        //TODO 把消息存储在mysql，再通过其它手段同步到mq
        SingleMsg singleMsg = MsgConvertUtil.singleMessageRequest2SingleMsg(request);
        int addResult=0;
        try{
            addResult=singleMsgService.addSingleMsg(singleMsg);
        }catch (Exception e){
            logger.error("保存消息："+singleMsg.getMsgServerId()+",from:"+singleMsg.getFromName()+",to:"+singleMsg.getToName()+"异常。"+e.getMessage(),e);
            return;
        }
        if(addResult!=0){
            //当把消息落库之后，原路返回一个 response
            ProtoMsg.Message resMsg= MsgBuilder.singleMessageResponse(true,singleMsg.getMsgClientId(),singleMsg.getMsgServerId(), ResultCodeEnum.SUCCESS
                    ,userServerSessionHolder.getNodeSession(singleMsg.getFromName()).getSessionId(),0,
                   singleMsg.getFromId() ,singleMsg.getFromName(),singleMsg.getToId(),singleMsg.getToName());
            ctx.channel().writeAndFlush(resMsg);
            //向后面传递此消息
            ProtoMsg.Message message=pkg.newBuilderForType()
                    .setType(pkg.getType())
                    .setSessionId(pkg.getSessionId())
                    .setFrom(pkg.getFrom())
                    .setTo(pkg.getTo())
                    .setMsgServerId(msgServerId)
                    .setSequence(pkg.getSequence()).setSingleMessageRequest(request).build();
            super.channelRead(ctx,message);
        }
        //在生成id之后，把消息转发 rocketmq-->db
        //这段代码 ：服务端接收到消息之后同步到mq-->mongodb,修改为把消息存储到mysql，再通过同步binlog的方式同步到mq-->mongodb
//        SingleMsgStoreModel singleMsgStoreModel= MsgConvertUtil.singleMessageRequest2SingleMsgStoreModel(request);
//        byte[] bytes= ProtostuffUtil.serializer(singleMsgStoreModel);
//        Message mqMsg=new Message(RocketMqConst.SINGLE_MSG_TOPIC, RocketMqConst.SINGLE_MSG_TAG, Long.toString(msgServerId), bytes);
//        SendResult result=msgProducer.send(mqMsg,msgServerId);
//        if(result.getSendStatus().compareTo(SendStatus.SEND_OK)==0){
//            logger.debug("单聊消息转发到mq成功");
//            //当把消息转发mq之后，原路返回一个 response
//            ProtoMsg.Message message= MsgBuilder.singleMessageResponse(true,singleMsgStoreModel.getMsgClientId(),singleMsgStoreModel.getMsgServerId(), ResultCodeEnum.SUCCESS
//                    ,userServerSessionHolder.getNodeSession(singleMsgStoreModel.getFrom()).getSessionId(),0
//                    ,singleMsgStoreModel.getFrom(),singleMsgStoreModel.getTo());
//            ctx.channel().writeAndFlush(message);
//        }else{
//            logger.info(result.getSendStatus().toString());
//        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(),cause);
        msgSender.sendCloseChannelMsg(ctx.channel().attr(UserServerSession.SESSION_KEY).get().getUser().getUsername()
                , SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getCode(),SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getMsg());
        ctx.channel().close();
    }

}