package com.hsf.im.handler;

import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.component.ZnodePathHolder;
import com.hsf.im.model.RocketMqConst;
import com.hsf.im.model.SingleMsgStoreModel;
import com.hsf.im.mq.producer.producer.ImDefaultMQProducer;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.server.UserServerSession;
import com.hsf.im.service.SingleMsgService;
import com.hsf.im.utils.MsgConvertUtil;
import com.hsf.im.utils.ProtostuffUtil;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: chenfz
 */
@Component
@ChannelHandler.Sharable
public class MsgTransmiterHandler extends ChannelInboundHandlerAdapter {

    Logger logger= LoggerFactory.getLogger(MsgTransmiterHandler.class);

    private static ThreadPoolExecutor poolExecutor=new ThreadPoolExecutor(10,1000,60, TimeUnit.SECONDS,new ArrayBlockingQueue<>(1000));

    private ImDefaultMQProducer msgProducer;

    @Value("${mq.rocketmq.nameSrvAddr}")
    private String rocketMqNameSrvAddr;

    @Autowired
    private MsgSender msgSender;

    @Autowired
    private ZnodePathHolder znodePathHolder;

    @Autowired
    private SingleMsgService singleMsgService;

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        msgProducer= new ImDefaultMQProducer(RocketMqConst.SINGLE_MSG_PRODUCER_GROUP,rocketMqNameSrvAddr);
        if(msgProducer!=null){
            logger.info("成功创建 msgProducer");
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (null == msg || !(msg instanceof ProtoMsg.Message)) {
            super.channelRead(ctx, msg);
            return;
        }
        //判断类型
        ProtoMsg.Message message = (ProtoMsg.Message) msg;
        ProtoMsg.HeadType headType = message.getType();
        if(headType.equals(ProtoMsg.HeadType.CLOSE_CHANNEL)){
            //如果接收到服务端发送过来关闭通道信号，将关闭通道
            ctx.channel().close();
        }
        if(headType.equals(ProtoMsg.HeadType.SINGLE_MESSAGE_RESPONSE)){
            //如果收到的是 客户端对消息的ack，msg-->rocketmq-->db修改消息的状态
//            ProtoMsg.SingleMessageResponse response=message.getSingleMessageResponse();
//            SingleMsgStoreModel singleMsgStoreModel= MsgConvertUtil.singleMessageResponse2SingleMsgStoreModel(response);
//            byte[] bytes= ProtostuffUtil.serializer(singleMsgStoreModel);
//            Message mqMsg=new Message(RocketMqConst.SINGLE_MSG_TOPIC, RocketMqConst.SINGLE_MSG_TAG, Long.toString(message.getMsgServerId()), bytes);
//            //poolExecutor.execute(new SubmitMsgResToMqTask(msgProducer,mqMsg,message.getMsgServerId()));
//            SendResult result=msgProducer.send(mqMsg,message.getMsgServerId());
//            if(result.getSendStatus().compareTo(SendStatus.SEND_OK)==0){
//                logger.debug("send singlemsg ack to mq success");
//            }else{
//                logger.info(result.getSendStatus().toString());
//            }
            //修改：如果收到的是，客户端对消息的ack，修改mysql里消息的状态（后续修改为批量修改消息状态，以减少tps）
            ProtoMsg.SingleMessageResponse response=message.getSingleMessageResponse();
            System.out.println("server ack single msg,toId:"+response.getToId()+",msgServerId:"+response.getMsgServerId());
            singleMsgService.ackSingleMsgByMsgServerId(response.getToId(),response.getMsgServerId());
            return;
        }
        if(headType.equals(ProtoMsg.HeadType.SINGLE_MESSAGE_REQUEST)){
            logger.debug("转发单聊消息");
            msgSender.sendSingleMsg(message);
        }
        if(headType.equals(ProtoMsg.HeadType.GROUP_QUIT_NOTIFY)){
            //处理用户退群的提示消息
            logger.debug("转发用户退群消息");
            //消息先保存到数据库
            msgSender.sendSingleNotifyMsg(message);
        }
        //TODO 添加其它类型消息 转发器
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(),cause);
        msgSender.sendCloseChannelMsg(ctx.channel().attr(UserServerSession.SESSION_KEY).get().getUser().getUsername()
                , SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getCode(),SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getMsg());
        ctx.channel().close();
    }
}

class SubmitMsgResToMqTask implements Runnable{

    Logger logger=LoggerFactory.getLogger(SubmitMsgResToMqTask.class);

    private ImDefaultMQProducer msgProducer;

    private Message message;

    private long msgServerId;

    public SubmitMsgResToMqTask(ImDefaultMQProducer msgProducer, Message message,long msgServerId){
        this.msgProducer=msgProducer;
        this.message=message;
        this.msgServerId=msgServerId;
    }

    @Override
    public void run() {
        SendResult result=msgProducer.send(message,msgServerId);
        if(result.getSendStatus().compareTo(SendStatus.SEND_OK)==0){
            logger.info("send msg to mq success");
        }else{
            logger.info(result.getSendStatus().toString());
        }
    }
}