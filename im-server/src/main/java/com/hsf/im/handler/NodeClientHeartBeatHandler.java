package com.hsf.im.handler;

import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.component.NodeServerSessionHolder;
import com.hsf.im.component.ZnodePathHolder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.server.NodeServerSession;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: chenfz
 */
@Component
@ChannelHandler.Sharable
public class NodeClientHeartBeatHandler extends ChannelInboundHandlerAdapter {

    Logger logger= LoggerFactory.getLogger(NodeClientHeartBeatHandler.class);

    @Autowired
    private NodeServerSessionHolder nodeServerSessionHolder;

    @Autowired
    private ZnodePathHolder znodePathHolder;

    @Autowired
    private MsgSender msgSender;

    public static final AttributeKey<ScheduledThreadPoolExecutor> SCHEDULED_KEY =
            AttributeKey.valueOf("SCHEDULED_KEY");

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        logger.info("nodeClientHeartBeatHandler add success...");
        ScheduledThreadPoolExecutor schedule=new ScheduledThreadPoolExecutor(1);
        ctx.channel().attr(SCHEDULED_KEY).set(schedule);
        schedule.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                msgSender.sendNodeHeartBeatMsg(ctx.channel().attr(NodeServerSession.SESSION_KEY).get().getZnodePath());
            }
        }, 10, 10, TimeUnit.SECONDS);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (null == msg || !(msg instanceof ProtoMsg.Message)) {
            super.channelRead(ctx, msg);
            return;
        }
        //判断类型
        ProtoMsg.Message message = (ProtoMsg.Message) msg;
        ProtoMsg.HeadType headType = message.getType();
        if(headType.equals(ProtoMsg.HeadType.CLOSE_CHANNEL)){
            //如果接收到服务端发送过来关闭通道信号，将关闭通道
            ctx.channel().attr(SCHEDULED_KEY).get().shutdown();
            ctx.channel().close();
        }
        if (!headType.equals(ProtoMsg.HeadType.NODE_HEART_BEAT)) {
            super.channelRead(ctx,msg);
            return;
        }

        //到此为接收到 服务端返回的 心跳信号
        //更新心跳信号最后接收时间
        LocalDateTime now=LocalDateTime.now();
        nodeServerSessionHolder.getNodeSession(message.getNodeHeartBeat().getNodepath())
                .setLastHeartBeatTime(now);
        logger.debug("update lastHeartBeat:"+now.toLocalTime());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(),cause);
        ctx.channel().attr(SCHEDULED_KEY).get().shutdownNow();
        msgSender.sendCloseNodeChannelMsg(znodePathHolder.getZnodePath()
        , SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getCode(),SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getMsg());
        ctx.channel().close();
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        ctx.channel().attr(SCHEDULED_KEY).get().shutdownNow();
        ctx.channel().close();
    }
}
