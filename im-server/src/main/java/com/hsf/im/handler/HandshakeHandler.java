package com.hsf.im.handler;

import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.auth.AuthRequest;
import com.hsf.im.auth.AuthResult;
import com.hsf.im.component.RouteTableHolder;
import com.hsf.im.component.SystemConst;
import com.hsf.im.component.UserServerSessionHolder;
import com.hsf.im.component.ZnodePathHolder;
import com.hsf.im.domain.User;
import com.hsf.im.msg.MsgBuilder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.msg.ResultCodeEnum;
import com.hsf.im.server.UserServerSession;
import com.hsf.im.service.UserService;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
@EnableConfigurationProperties(SystemConst.class)
@ChannelHandler.Sharable
public class HandshakeHandler extends ChannelInboundHandlerAdapter {

    Logger logger= LoggerFactory.getLogger(HandshakeHandler.class);

    public static final AttributeKey<Boolean> HANDSHAKE_KEY =
            AttributeKey.valueOf("HANDSHAKE_KEY");

    @Autowired
    private SystemConst systemConst;

    @Autowired
    private UserService userService;

    @Autowired
    private ZnodePathHolder znodePathHolder;

    @Autowired
    private RouteTableHolder routeTableHolder;

    @Autowired
    private UserServerSessionHolder userServerSessionHolder;

    @Autowired
    private HeartBeatHandler heartBeatHandler;

    @Autowired
    private MsgTransmiterHandler msgTransmiterHandler;

    @Autowired
    private MsgServerIdHandler msgServerIdHandler;


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //通道激活之后，若1分钟之内客户端未发送握手信息，将关闭通道
        ctx.channel().attr(HANDSHAKE_KEY).set(false);
        ScheduledThreadPoolExecutor schedule=new ScheduledThreadPoolExecutor(1);
        schedule.schedule(new CheckHandshakeTask(ctx),60, TimeUnit.SECONDS);
        super.channelActive(ctx);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (null == msg || !(msg instanceof ProtoMsg.Message)) {
            super.channelRead(ctx, msg);
            return;
        }
        ProtoMsg.Message pkg = (ProtoMsg.Message) msg;

        //取得请求类型
        ProtoMsg.HeadType headType = pkg.getType();

        if(headType.equals(ProtoMsg.HeadType.CLOSE_CHANNEL)){
            ctx.channel().close();
        }

        if (!headType.equals(ProtoMsg.HeadType.HANDSHAKE_REQUEST)) {
            super.channelRead(ctx, msg);
            return;
        }
        //验证token
        String token=pkg.getHandshakeRequest().getToken();
        UserServerSession session = new UserServerSession(ctx.channel());
        logger.debug("handshake handler token:"+token);
        logger.debug("check token url:"+systemConst.getCheckTokenUrl());
        logger.debug("token url:"+systemConst.getTokenUrl());
        logger.debug("register url:"+systemConst.getRegisterUrl());
        logger.debug("netty url:"+systemConst.getRequestNettyServerUrl());
        AuthResult result= AuthRequest.auth(systemConst.getCheckTokenUrl(),"browser","112233",token);
        if(result !=null && result.isActive()){
            String username=result.getUsername();
            User user=userService.selectByUserName(username);
            if(user==null){
                logger.error("客户端握手失败，无此用户："+username+",将连接通道关闭。。。");
                sendCloseMsg(ctx,SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getCode()
                ,SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getMsg());
            }else{
                session.setUser(user);
                ProtoMsg.Message resMsg= MsgBuilder.handshakeResponse(ResultCodeEnum.SUCCESS,session.getSessionId(),0);
                ctx.channel().writeAndFlush(resMsg);
                setRouteTable(username);
                ctx.channel().attr(HANDSHAKE_KEY).set(true);
                userServerSessionHolder.setNodeSession(user.getUsername(),session);
                //把session设置到channel
                ctx.channel().attr(UserServerSession.SESSION_KEY).set(session);
                ctx.pipeline().remove(this);
                ctx.pipeline().addLast("msgIdHandler",msgServerIdHandler);
                ctx.pipeline().addLast("heartBeat",heartBeatHandler);
                ctx.pipeline().addLast(msgTransmiterHandler);
                logger.info("用户："+user.getUsername()+",连接成功。。。");

            }
        }else{
            logger.info("token 验证失败。。。关闭channel");
            sendCloseMsg(ctx,SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getCode()
            ,SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getMsg());
        }


    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("handshake handler exception:"+cause.getMessage(),cause);
        sendCloseMsg(ctx,SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getCode()
        ,SystemExceptionEnum.CHANNEL_HANDSHAKE_CLOSE.getMsg());
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
        super.channelReadComplete(ctx);
    }

    private void sendCloseMsg(ChannelHandlerContext ctx,String code,String info){
        UserServerSession serverSession=ctx.channel().attr(UserServerSession.SESSION_KEY).get();
        ProtoMsg.Message closeMsg=null;
        if(serverSession!=null){
            closeMsg=MsgBuilder.closeChannel(serverSession.getSessionId(),0,code,info);
        }else {
            closeMsg=MsgBuilder.closeChannel("0",0,code,info);
        }
        ctx.channel().writeAndFlush(closeMsg);
        ctx.channel().close();

    }

    /**
     * 握手成功之后，设置路由项，其它节点上的用户根据路由项，将信息发送到本节点
     * @param username
     */
    private void setRouteTable(String username){
        routeTableHolder.setTableItem(username,znodePathHolder.getZnodePath());
    }

//    /**
//     * 移除路由表项
//     * @param username
//     */
//    private void removeRouteTable(String username){
//        routeTableHolder.removeTableItem(username);
//    }


    class CheckHandshakeTask implements Runnable{

        private ChannelHandlerContext ctx;

        public final AttributeKey<Boolean> HANDSHAKE_KEY =
                AttributeKey.valueOf("HANDSHAKE_KEY");

        public CheckHandshakeTask(ChannelHandlerContext ctx){
            this.ctx=ctx;
        }

        @Override
        public void run() {
            Boolean hashandshake=ctx.channel().attr(HANDSHAKE_KEY).get();
            if(!hashandshake){
                sendCloseMsg(ctx, SystemExceptionEnum.CHANNEL_HANDSHAKE_TIMEOUT_CLOSE.getCode()
                ,SystemExceptionEnum.CHANNEL_HANDSHAKE_TIMEOUT_CLOSE.getMsg());
            }
        }
    }
}

