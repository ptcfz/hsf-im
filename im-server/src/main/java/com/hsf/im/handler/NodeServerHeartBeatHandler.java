package com.hsf.im.handler;

import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.component.NodeServerSessionHolder;
import com.hsf.im.component.ZnodePathHolder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.server.NodeServerSession;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @Author: chenfz
 */
@Component
@ChannelHandler.Sharable
public class NodeServerHeartBeatHandler extends ChannelInboundHandlerAdapter {

    Logger logger= LoggerFactory.getLogger(NodeServerHeartBeatHandler.class);

    @Autowired
    private NodeServerSessionHolder nodeServerSessionHolder;

    @Autowired
    private ZnodePathHolder znodePathHolder;

    @Autowired
    private MsgSender msgSender;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        if (null == msg || !(msg instanceof ProtoMsg.Message)) {
            super.channelRead(ctx, msg);
            return;
        }
        //判断类型
        ProtoMsg.Message message = (ProtoMsg.Message) msg;
        ProtoMsg.HeadType headType = message.getType();
        if(headType.equals(ProtoMsg.HeadType.CLOSE_CHANNEL)){
            //如果接收到服务端发送过来关闭通道信号，将关闭通道
            ctx.channel().close();
        }
        if (!headType.equals(ProtoMsg.HeadType.NODE_HEART_BEAT)) {
            super.channelRead(ctx,msg);
            return;
        }
        logger.info("服务端接收到 客户端的 心跳信号。。。");
        NodeServerSession nodeServerSession=nodeServerSessionHolder.getNodeSession(message.getNodeHeartBeat().getNodepath());
        if(nodeServerSession!=null){
            nodeServerSession.setLastHeartBeatTime(LocalDateTime.now());
            nodeServerSessionHolder.setNodeSession(message.getNodeHeartBeat().getNodepath(),nodeServerSession);
        }
        //向连接端回送心跳信号
        logger.info("向客户端回送心跳信号。。。");
        msgSender.sendNodeHeartBeatMsg(ctx.channel().attr(NodeServerSession.SESSION_KEY).get().getZnodePath());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(),cause);
        msgSender.sendCloseNodeChannelMsg(znodePathHolder.getZnodePath()
        , SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getCode(),SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getMsg());
        ctx.channel().close();
    }
}
