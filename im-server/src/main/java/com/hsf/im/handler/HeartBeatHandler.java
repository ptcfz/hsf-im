package com.hsf.im.handler;

import com.hsf.im.Exception.SystemExceptionEnum;
import com.hsf.im.component.RouteTableHolder;
import com.hsf.im.component.UserServerSessionHolder;
import com.hsf.im.component.ZnodePathHolder;
import com.hsf.im.msg.ProtoMsg;
import com.hsf.im.sender.MsgSender;
import com.hsf.im.server.UserServerSession;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @Author: chenfz
 * 发送心跳信号
 */
@Component
@ChannelHandler.Sharable
public class HeartBeatHandler extends ChannelInboundHandlerAdapter {

    Logger logger= LoggerFactory.getLogger(HeartBeatHandler.class);

    @Autowired
    private UserServerSessionHolder userServerSessionHolder;

    @Autowired
    private RouteTableHolder routeTableHolder;

    @Autowired
    private ZnodePathHolder znodePathHolder;

    @Autowired
    private MsgSender msgSender;


    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        if (null == msg || !(msg instanceof ProtoMsg.Message)) {
            super.channelRead(ctx, msg);
            return;
        }
        //判断类型
        ProtoMsg.Message pkg = (ProtoMsg.Message) msg;
        ProtoMsg.HeadType headType = ((ProtoMsg.Message) msg).getType();
        if(headType.equals(ProtoMsg.HeadType.CLOSE_CHANNEL)){
            //如果接收到服务端发送过来关闭通道信号，将关闭通道
            logger.info("服务端收到通道关闭信号。。。");
            ctx.channel().close();
        }
        if (!headType.equals(ProtoMsg.HeadType.HEART_BEAT)) {
            super.channelRead(ctx, msg);
            return;
        }
        //到此为接收到 客户端的 心跳信号
        //1，更新心跳信号最后接收时间
        UserServerSession serverSession = userServerSessionHolder.getNodeSession(pkg.getHeartBeat().getUsername());
        if(serverSession!=null){
            serverSession.setLastHeartBeatTime(LocalDateTime.now());
            userServerSessionHolder.setNodeSession(pkg.getHeartBeat().getUsername(),serverSession);
        }
        //2，更新路由表,ehcache缓存有效期为60秒
        routeTableHolder.setTableItem(pkg.getHeartBeat().getUsername(),znodePathHolder.getZnodePath());
        //向客户端回送心跳信号
        msgSender.sendHeartBeatMsg(serverSession.getUser().getUsername());

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error(cause.getMessage(),cause);
        msgSender.sendCloseChannelMsg(ctx.channel().attr(UserServerSession.SESSION_KEY).get().getUser().getUsername()
        , SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getCode(),SystemExceptionEnum.CHANNEL_HEARTBEAT_CLOSE.getMsg());
        ctx.channel().close();
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        znodePathHolder.incrConNum();
    }


    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        znodePathHolder.decConNum();
    }
}
