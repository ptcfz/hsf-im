package com.hsf.im.route;

import java.io.Serializable;

/**
 * @Author: chenfz
 * ehcache里用户-节点路由表项
 */
public class RouteTableItem implements Serializable {

    /**
     * 用户名
     */
    private String username;

    /**
     * 这个用户所连接的节点路径
     */
    private String znodePath;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getZnodePath() {
        return znodePath;
    }

    public void setZnodePath(String znodePath) {
        this.znodePath = znodePath;
    }
}
