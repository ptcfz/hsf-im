package com.hsf.im.route;

/**
 * @Author: chenfz
 */
public interface RouteTable {

    public void setTableItem(String username,String nodePath);

    public RouteTableItem getTableItem(String username);
}
