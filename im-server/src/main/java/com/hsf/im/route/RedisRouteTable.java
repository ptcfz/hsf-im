package com.hsf.im.route;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @Author: chenfz
 */
@ConditionalOnProperty(value = "route.type",havingValue = "redis")
@Component("redisRouteTable")
public class RedisRouteTable implements RouteTable{

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void setTableItem(String username,String nodePath){
        RouteTableItem routeTableItem=new RouteTableItem();
        routeTableItem.setUsername(username);
        routeTableItem.setZnodePath(nodePath);
        redisTemplate.opsForValue().set(username,routeTableItem,60, TimeUnit.SECONDS);
    }

    @Override
    public RouteTableItem getTableItem(String username){
        RouteTableItem item=(RouteTableItem) redisTemplate.opsForValue().get(username);
        if(item!= null){
            return (RouteTableItem) item;
        }
        return null;
    }

}
