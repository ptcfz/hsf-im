package com.hsf.im.route;

import io.netty.channel.Channel;

/**
 * @Author: chenfz
 * 单聊消息在本节点的通道路由项
 */
public class SingleMsgChannelRouteItem {

    private String toUsername;

    private Channel toChannel;

    public String getToUsername() {
        return toUsername;
    }

    public void setToUsername(String toUsername) {
        this.toUsername = toUsername;
    }

    public Channel getToChannel() {
        return toChannel;
    }

    public void setToChannel(Channel toChannel) {
        this.toChannel = toChannel;
    }
}
