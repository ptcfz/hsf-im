package com.hsf.im.route;

import io.netty.channel.Channel;

/**
 * @Author: chenfz
 * 本节点  客户端--channel 路由项
 */
public class ClientChannelRouteItem {

    private String username;

    private Channel channel;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }
}
