package com.hsf.im.route;

import com.hsf.im.service.EhcacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

/**
 * @Author: chenfz
 */
@ConditionalOnProperty(value = "route.type",havingValue = "ehcache")
@Component("ehcacheRouteTable")
public class EhcacheRouteTable implements RouteTable{

    @Value("${ehcache.routeTableCacheName}")
    private String routeTableCacheName;


    @Autowired
    private EhcacheService ehcacheService;

    @Override
    public void setTableItem(String username,String nodePath){
        RouteTableItem routeTableItem=new RouteTableItem();
        routeTableItem.setUsername(username);
        routeTableItem.setZnodePath(nodePath);
        ehcacheService.put(routeTableCacheName,username,routeTableItem);
    }

    @Override
    public RouteTableItem getTableItem(String username){
        Object obj=ehcacheService.get(routeTableCacheName,username);
        if(obj!= null){
            return (RouteTableItem) obj;
        }
        return null;
    }

}
