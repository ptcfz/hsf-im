package com.hsf.im.msg;


import java.util.Date;

public class MsgBuilder {

    //连接请求
    public static ProtoMsg.Message handshakeRequest(long seqId, String token){
        ProtoMsg.Message.Builder msgBuilder= ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.HANDSHAKE_REQUEST)
                .setSequence(seqId);

        ProtoMsg.HandshakeRequest.Builder builder= ProtoMsg.HandshakeRequest.newBuilder()
                .setToken(token)
                .setDeviceId("5");

        msgBuilder.setHandshakeRequest(builder.build());
        return msgBuilder.build();
    }

    //连接应答
    public static ProtoMsg.Message handshakeResponse(ResultCodeEnum en, String sessionId, long seqId){
        ProtoMsg.Message.Builder msgBuilder= ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.HANDSHAKE_RESPONSE)
                .setSessionId(sessionId)
                .setSequence(seqId);

        ProtoMsg.HandshakeResponse.Builder builder= ProtoMsg.HandshakeResponse.newBuilder()
                .setCode(en.getCode())
                .setInfo(en.getDesc())
                .setExpose(1);

        msgBuilder.setHandshakeResponse(builder.build());
        return msgBuilder.build();
    }

    //消息发送请求
    public static ProtoMsg.Message singleMessageRequest(String msgClientId,long msgServerId, long fromId, String from, long toId, String to
            , Date time, int msgType, String content, String sessionId, long seqId){
        ProtoMsg.Message.Builder msgBuilder= ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.SINGLE_MESSAGE_REQUEST)
                .setSessionId(sessionId)
                .setFrom(from)
                .setTo(to)
                .setSequence(seqId);

        ProtoMsg.SingleMessageRequest.Builder builder= ProtoMsg.SingleMessageRequest.newBuilder()
                .setMsgClientId(msgClientId)
                .setMsgServerId(msgServerId)
                .setFromId(fromId)
                .setFrom(from)
                .setToId(toId)
                .setTo(to)
                .setTime(time.getTime())
                .setMsgType(msgType)
                .setContent(content);

        msgBuilder.setSingleMessageRequest(builder.build());
        return msgBuilder.build();
    }

    //消息应答
    public static ProtoMsg.Message singleMessageResponse(boolean result,String msgClientId,long msgServerId,ResultCodeEnum en, String sessionId, long seqId,
                                                         long fromId,String from,long toId,String to){
        ProtoMsg.Message.Builder msgBuilder= ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.SINGLE_MESSAGE_RESPONSE)
                .setSessionId(sessionId)
                .setSequence(seqId);

        ProtoMsg.SingleMessageResponse.Builder builder= ProtoMsg.SingleMessageResponse.newBuilder()
                .setResult(result)
                .setMsgClientId(msgClientId)
                .setMsgServerId(msgServerId)
                .setCode(en.getCode())
                .setInfo(en.getDesc())
                .setFromId(fromId)
                .setFrom(from)
                .setToId(toId)
                .setTo(to);
        msgBuilder.setSingleMessageResponse(builder.build());
        return msgBuilder.build();
    }

    //心跳信号
    public static ProtoMsg.Message heartBeat(String username, String sessionId, long seqId){
        ProtoMsg.Message.Builder msgBuilder= ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.HEART_BEAT)
                .setSessionId(sessionId)
                .setSequence(seqId);

        ProtoMsg.MessageHeartBeat.Builder builder= ProtoMsg.MessageHeartBeat.newBuilder()
                .setUsername(username);

        msgBuilder.setHeartBeat(builder.build());
        return msgBuilder.build();
    }

    public static ProtoMsg.Message closeChannel(String sessionId, long seqId,String code,String info){
        ProtoMsg.Message.Builder msgBuilder = ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.CLOSE_CHANNEL)
                .setSessionId(sessionId)
                .setSequence(seqId);

        ProtoMsg.CloseChannel.Builder builder=ProtoMsg.CloseChannel.newBuilder()
                .setCode(code)
                .setInfo(info)
                .setSeq(0);
        msgBuilder.setCloseChannel(builder);
        return msgBuilder.build();
    }

    /**
     * 在握手阶段，还未建立session，用此方法发送 关闭通道信号
     * @param seq
     * @return
     */
    public static ProtoMsg.Message closeChannelBeforeSession(long seq){
        ProtoMsg.Message.Builder msgBuilder = ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.CLOSE_CHANNEL)
                .setSequence(seq);

        ProtoMsg.CloseChannel.Builder builder=ProtoMsg.CloseChannel.newBuilder()
                .setSeq(0);
        msgBuilder.setCloseChannel(builder);
        return msgBuilder.build();
    }

    public static ProtoMsg.Message nodeHandshakeRequestMsg(String znodePath){
        ProtoMsg.Message.Builder msgBuilder = ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.NODE_HANDSHAKE_REQUEST);

        ProtoMsg.NodeHandshakeRequest.Builder builder=ProtoMsg.NodeHandshakeRequest.newBuilder()
                .setNodepath(znodePath);
        msgBuilder.setNodeHandshakeRequest(builder);
        return msgBuilder.build();
    }

    public static ProtoMsg.Message nodeHandshakeResponseMsg(String znodePath,boolean result,int code,String info,String sessionId){
        ProtoMsg.Message.Builder msgBuilder = ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.NODE_HANDSHAKE_RESPONSE);

        ProtoMsg.NodeHandshakeResponse.Builder builder=ProtoMsg.NodeHandshakeResponse.newBuilder()
                .setCode(code)
                .setInfo(info)
                .setNodepath(znodePath)
                .setResult(result)
                .setSessionId(sessionId);

        msgBuilder.setNodeHandshakeResponse(builder);
        return msgBuilder.build();
    }

    public static ProtoMsg.Message nodeHeartBeatMsg(String znodePath){
        ProtoMsg.Message.Builder msgBuilder = ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.NODE_HEART_BEAT);

        ProtoMsg.NodeHeartBeat.Builder builder=ProtoMsg.NodeHeartBeat.newBuilder()
                .setNodepath(znodePath)
                .setSeq(0);
        msgBuilder.setNodeHeartBeat(builder);
        return msgBuilder.build();
    }

    public static ProtoMsg.Message quitGroupMsg(long fromUserId,String fromUsername,String toUsername,
                                                long groupId, String groupName,String msg){
        ProtoMsg.Message.Builder msgBuilder = ProtoMsg.Message.newBuilder()
                .setType(ProtoMsg.HeadType.GROUP_QUIT_NOTIFY)
                .setFrom(fromUsername)
                .setTo(toUsername);

        ProtoMsg.GroupQuitNotify.Builder builder = ProtoMsg.GroupQuitNotify.newBuilder()
                .setGroupId(groupId)
                .setGroupName(groupName)
                .setUserId(fromUserId)
                .setUserName(fromUsername)
                .setMsg(msg);
        msgBuilder.setGroupQuitNotify(builder);
        return msgBuilder.build();
    }
}
