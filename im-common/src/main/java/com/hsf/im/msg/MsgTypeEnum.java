package com.hsf.im.msg;

/**
 * @Author: chenfz
 */
public enum MsgTypeEnum {

    SINGLE_MSG_REQUEST(0,"单聊请求信息"),
    SINGLE_MSG_RESPONSE(1,"单聊ack信息"),
    GROUP_QUIT_NOTIFY(3,"用户退群提示信息");

    private int code;
    private String desc;

    MsgTypeEnum(int code,String desc){
        this.code=code;
        this.desc=desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
