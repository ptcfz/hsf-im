package com.hsf.im.msg;

/**
 * @Author: chenfz
 * 消息状态
 * 客户端：CLIENT_SEND-->CLIENT_ACK-->CLIENT_READ
 *      客户端发送消息之后-->CLIENT_SEND状态，服务端收到消息返回ack-->CLIENT_ACK
 *      接收到对方发送的消息-->CLIENT_ACK
 *
 * 服务端：SERVER_RECEIVE-->SERVER_ACK
 *      服务端接收到客户端的消息-->SERVER_RECEIVE
 *      客户端确认收到消息-->SERVER_ACK
 */
public enum MsgStatusEnum {

    CLIENT_SEND("CLIENT_SEND","客户端已发送"),
    CLIENT_ACK("CLIENT_ACK","服务端已确认客户端发送的消息"),
    CLIENT_READ("CLIENT_READ","消息已阅"),
    SERVER_RECEIVE("SERVER_RECEIVE","服务端收到消息"),
    SERVER_ACK("SERVER_ACK","客户端已确认服务端投递的消息");

    private String code;
    private String desc;

    MsgStatusEnum(String code, String desc){
        this.code=code;
        this.desc=desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
