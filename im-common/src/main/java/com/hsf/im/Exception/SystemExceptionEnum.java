package com.hsf.im.Exception;

/**
 * @Author: chenfz
 */
public enum SystemExceptionEnum {

    //系统相关
    ROLE_NOT_FOUND("900100","角色不存在"),
    USER_NOT_FOUND("900101","用户不存在"),

    //消息异常
    MESSAGE_UNSUPPORT_TYPE("900200","不支持的消息类型"),
    MESSAGE_ARGUMENT_TOUSER_NULL("900201","消息接收者参数为空"),
    MESSAGE_ARGUMENT_FROMUSER_NULL("900202","消息发送者参数为空"),


    //通道相关
    CHANNEL_HANDSHAKE_CLOSE("900300","握手阶段异常关闭"),
    CHANNEL_HANDSHAKE_TIMEOUT_CLOSE("900301","握手超时"),
    CHANNEL_HEARTBEAT_CLOSE("900302","心跳信号异常关闭通道"),
    CHANNEL_HEARTBEAT_SERVER_TIMEOUT_CLOSE("900303","服务端心跳超时，关闭通道");


    private String code;
    private String msg;

    SystemExceptionEnum(String code,String msg){
        this.code=code;
        this.msg=msg;
    }

    public String getCode(){
        return this.code;
    }
    public String getMsg(){
        return this.msg;
    }
}
