package com.hsf.im.Exception;

/**
 * @Author: chenfz
 */
public class UnSupportMsgException extends RuntimeException{

    private String code;

    private String msg;

    public UnSupportMsgException(String code, String msg){
        this.code=code;
        this.msg=msg;
    }
}
