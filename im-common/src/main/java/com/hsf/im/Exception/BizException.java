package com.hsf.im.Exception;

/**
 * @Author: chenfz
 */
public class BizException extends RuntimeException{

    private String code;

    private String msg;

    public BizException(String code,String msg){
        this.code=code;
        this.msg=msg;
    }
}
