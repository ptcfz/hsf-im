package com.hsf.im.domain;

import java.util.Date;

public class GroupCatelog {

    private Integer id;

    private String catelogName;

    private Byte level;

    private Integer parentCatelogId;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCatelogName() {
        return catelogName;
    }

    public void setCatelogName(String catelogName) {
        this.catelogName = catelogName == null ? null : catelogName.trim();
    }

    public Byte getLevel() {
        return level;
    }

    public void setLevel(Byte level) {
        this.level = level;
    }

    public Integer getParentCatelogId() {
        return parentCatelogId;
    }

    public void setParentCatelogId(Integer parentCatelogId) {
        this.parentCatelogId = parentCatelogId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}