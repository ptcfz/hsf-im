package com.hsf.im.domain;

import java.util.Date;

public class Group {
    private Long id;

    private String groupName;

    private Long createUserId;

    private Integer catelogTopId;

    private String catelogTopName;

    private Integer catelogSecondId;

    private String catelogSecondName;

    private Byte needApply;

    private Date createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Integer getCatelogTopId() {
        return catelogTopId;
    }

    public void setCatelogTopId(Integer catelogTopId) {
        this.catelogTopId = catelogTopId;
    }

    public String getCatelogTopName() {
        return catelogTopName;
    }

    public void setCatelogTopName(String catelogTopName) {
        this.catelogTopName = catelogTopName == null ? null : catelogTopName.trim();
    }

    public Integer getCatelogSecondId() {
        return catelogSecondId;
    }

    public void setCatelogSecondId(Integer catelogSecondId) {
        this.catelogSecondId = catelogSecondId;
    }

    public String getCatelogSecondName() {
        return catelogSecondName;
    }

    public void setCatelogSecondName(String catelogSecondName) {
        this.catelogSecondName = catelogSecondName == null ? null : catelogSecondName.trim();
    }

    public Byte getNeedApply() {
        return needApply;
    }

    public void setNeedApply(Byte needApply) {
        this.needApply = needApply;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}