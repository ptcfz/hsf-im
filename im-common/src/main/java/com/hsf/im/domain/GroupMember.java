package com.hsf.im.domain;

import java.util.Date;

public class GroupMember {
    private Long id;

    private Long groupId;

    private Long userId;

    private Long approveUserId;

    private Long lastAckMsgId;

    private Date applyTime;

    private Date joinTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getApproveUserId() {
        return approveUserId;
    }

    public void setApproveUserId(Long approveUserId) {
        this.approveUserId = approveUserId;
    }

    public Long getLastAckMsgId() {
        return lastAckMsgId;
    }

    public void setLastAckMsgId(Long lastAckMsgId) {
        this.lastAckMsgId = lastAckMsgId;
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public Date getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Date joinTime) {
        this.joinTime = joinTime;
    }
}