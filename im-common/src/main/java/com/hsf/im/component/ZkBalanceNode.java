package com.hsf.im.component;

/**
 * 用于计算netty在zk注册节点连接的负载
 */
public class ZkBalanceNode implements Comparable<ZkBalanceNode>{

    private String nodeName;

    private double factor;


    @Override
    public int compareTo(ZkBalanceNode o) {
        if(this.factor>o.getFactor()){
            return 1;
        }else if(this.factor<o.getFactor()){
            return -1;
        }
        return 0;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public double getFactor() {
        return factor;
    }

    public void setFactor(double factor) {
        this.factor = factor;
    }
}
