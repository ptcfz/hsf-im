package com.hsf.im.component;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "system")
public class SystemConst {

    private String registerUrl;

    private String requestNettyServerUrl;

    private String tokenUrl;

    private String checkTokenUrl;

    private String findUserUrl;

    private String findUserByIdUrl;

    private String userOffLineMsgUrl;

    private String userOffLineMsgFromMysqlUrl;

    private String findUserJoinGroups;

    private String modifyGroupInfo;

    private String userQuitGroup;

    private String batchAckMsgByLastMsgServerId;

    private String getAllGroupCatelogUrl;

    private String createGroupUrl;

    public String getRegisterUrl() {
        return registerUrl;
    }

    public void setRegisterUrl(String registerUrl) {
        this.registerUrl = registerUrl;
    }

    public String getRequestNettyServerUrl() {
        return requestNettyServerUrl;
    }

    public void setRequestNettyServerUrl(String requestNettyServerUrl) {
        this.requestNettyServerUrl = requestNettyServerUrl;
    }

    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
    }

    public String getCheckTokenUrl() {
        return checkTokenUrl;
    }

    public void setCheckTokenUrl(String checkTokenUrl) {
        this.checkTokenUrl = checkTokenUrl;
    }

    public String getFindUserUrl() {
        return findUserUrl;
    }

    public void setFindUserUrl(String findUserUrl) {
        this.findUserUrl = findUserUrl;
    }

    public String getUserOffLineMsgUrl() {
        return userOffLineMsgUrl;
    }

    public void setUserOffLineMsgUrl(String userOffLineMsgUrl) {
        this.userOffLineMsgUrl = userOffLineMsgUrl;
    }

    public String getFindUserJoinGroups() {
        return findUserJoinGroups;
    }

    public void setFindUserJoinGroups(String findUserJoinGroups) {
        this.findUserJoinGroups = findUserJoinGroups;
    }

    public String getModifyGroupInfo() {
        return modifyGroupInfo;
    }

    public void setModifyGroupInfo(String modifyGroupInfo) {
        this.modifyGroupInfo = modifyGroupInfo;
    }

    public String getFindUserByIdUrl() {
        return findUserByIdUrl;
    }

    public void setFindUserByIdUrl(String findUserByIdUrl) {
        this.findUserByIdUrl = findUserByIdUrl;
    }

    public String getUserQuitGroup() {
        return userQuitGroup;
    }

    public void setUserQuitGroup(String userQuitGroup) {
        this.userQuitGroup = userQuitGroup;
    }

    public String getUserOffLineMsgFromMysqlUrl() {
        return userOffLineMsgFromMysqlUrl;
    }

    public void setUserOffLineMsgFromMysqlUrl(String userOffLineMsgFromMysqlUrl) {
        this.userOffLineMsgFromMysqlUrl = userOffLineMsgFromMysqlUrl;
    }

    public String getBatchAckMsgByLastMsgServerId() {
        return batchAckMsgByLastMsgServerId;
    }

    public void setBatchAckMsgByLastMsgServerId(String batchAckMsgByLastMsgServerId) {
        this.batchAckMsgByLastMsgServerId = batchAckMsgByLastMsgServerId;
    }

    public String getGetAllGroupCatelogUrl() {
        return getAllGroupCatelogUrl;
    }

    public void setGetAllGroupCatelogUrl(String getAllGroupCatelogUrl) {
        this.getAllGroupCatelogUrl = getAllGroupCatelogUrl;
    }

    public String getCreateGroupUrl() {
        return createGroupUrl;
    }

    public void setCreateGroupUrl(String createGroupUrl) {
        this.createGroupUrl = createGroupUrl;
    }
}
