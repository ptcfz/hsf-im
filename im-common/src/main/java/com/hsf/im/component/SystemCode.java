package com.hsf.im.component;

public enum  SystemCode {

    SUCCESS(200,"成功"),
    NOT_FOUND(404,"资源不存在");

    private int code;
    private String msg;

    SystemCode(int code,String msg){
        this.code=code;
        this.msg=msg;
    }

    public int getCode(){
        return this.code;
    }

    public String getMsg(){
        return this.msg;
    }
}
