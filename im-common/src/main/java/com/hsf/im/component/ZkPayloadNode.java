package com.hsf.im.component;

/**
 * netty服务向zk注册时znode的payload对象
 */
public class ZkPayloadNode {

    /**
     * 节点主机地址
     */
    private String host;

    /**
     * 用于接收客户端连接的监听端口
     */
    private int port;

    /**
     * 用于节点之间连接的监听端口
     */
    private int nodePort;

    /**
     * 连接系数
     */
    private double factor;

    /**
     * 节点当前连接数
     */
    private long conNum;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public double getFactor() {
        return factor;
    }

    public void setFactor(double factor) {
        this.factor = factor;
    }

    public long getConNum() {
        return conNum;
    }

    public void setConNum(long conNum) {
        this.conNum = conNum;
    }

    public int getNodePort() {
        return nodePort;
    }

    public void setNodePort(int nodePort) {
        this.nodePort = nodePort;
    }
}
