package com.hsf.im.component;

/**
 * account服务器返回给客户端netty服务器地址类
 */
public class NettyAddressModel {

    private int code;

    private String msg;

    private String host;

    private int port;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString(){
        StringBuffer sb=new StringBuffer();
        sb.append("code=").append(code).append(",msg=").append(msg).append(",host=").append(host)
                .append(",port=").append(port);
        return sb.toString();
    }
}
