package com.hsf.im.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledThreadPoolExecutor;

/**
 * @Author: chenfz
 */
public class ImScheduledThreadPoolExecutor extends ScheduledThreadPoolExecutor {

    private Map<String,Object> attributes=new ConcurrentHashMap<>();

    public ImScheduledThreadPoolExecutor(int corePoolSize){
        super(corePoolSize);
    }

    public void setAttribute(String key,Object obj){
        attributes.put(key,obj);
    }

    public Object getAttribute(String key){
        return attributes.get(key);
    }
}
