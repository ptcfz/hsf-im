package com.hsf.im.utils;

import java.util.Base64;

public class EncoderUtil {

    public static String encoderBase64(String text){
        if(text==null){
            return null;
        }
        Base64.Encoder encoder=Base64.getEncoder();
        byte[] bytes=null;
        try{
            bytes=text.getBytes("UTF-8");
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
        String encoderText=encoder.encodeToString(bytes);
        return encoderText;
    }
}
