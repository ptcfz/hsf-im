package com.hsf.im.utils;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public class NetUtil {

    public static String getHostIp(){
        try{
            Enumeration<NetworkInterface> allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            InetAddress ip=null;
            while(allNetInterfaces.hasMoreElements()){
                NetworkInterface networkInterface=(NetworkInterface)allNetInterfaces.nextElement();
                if(networkInterface.isLoopback() || networkInterface.isVirtual() || !networkInterface.isUp()){
                    continue;
                }
                Enumeration<InetAddress> address=networkInterface.getInetAddresses();
                while(address.hasMoreElements()){
                    ip=address.nextElement();
                    if(ip !=null && ip instanceof Inet4Address){
                        return ip.getHostAddress();
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
