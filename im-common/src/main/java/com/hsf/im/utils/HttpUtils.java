package com.hsf.im.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hsf.im.component.*;
import com.hsf.im.domain.Group;
import com.hsf.im.domain.GroupCatelog;
import com.hsf.im.domain.SingleMsg;
import com.hsf.im.domain.User;
import com.hsf.im.model.SingleMsgStoreModel;
import com.hsf.im.model.dto.GroupDto;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HttpUtils {

    static Logger logger = LoggerFactory.getLogger(HttpUtils.class);

    private final static Integer SUCCESS_CODE=200;

    /**
     * 发送用户注册信息
     * @param url
     * @param registerObj
     * @return  返回注册结果 true or false
     */
    public static String sendRegisterPost(String url, RegisterObj registerObj){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        HttpPost httpPost=new HttpPost(url);

        String jsonString = JSON.toJSONString(registerObj);
        StringEntity entity=new StringEntity(jsonString,"UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-Type","application/json;charset=utf8");

        CloseableHttpResponse response=null;

        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            String result=EntityUtils.toString(responseEntity);
            return result;
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }

        return null;
    }

    /**
     * 发送获取token请求
     * @param url
     * @param obj
     * @return  返回token信息
     */
    public static TokenResponseObj sendTokenPost(String url, TokenRequestObj obj){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        StringBuffer sb=new StringBuffer();
        sb.append("?username=");
        sb.append(obj.getUsername()).append("&password=").append(obj.getPassword()).append("&grant_type=")
                .append(obj.getGrant_type()).append("&scope=").append(obj.getScope());
        HttpPost httpPost=new HttpPost(url+sb.toString());

        String authStr= EncoderUtil.encoderBase64("browser:112233");
        httpPost.setHeader("Authorization","Basic "+authStr);

        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
                return null;
            }
            JSONObject jsonObj= JSON.parseObject(EntityUtils.toString(responseEntity));
            if(jsonObj!=null){
                TokenResponseObj result=new TokenResponseObj();
                result.setToken(jsonObj.getString("access_token"));
                result.setRefresh_token(jsonObj.getString("refresh_token"));
               // result.setExpireDate(jsonObj.getInteger("expire_in"));
                return result;
            }

        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }


    /**
     * 刷新token请求
     * @param url
     * @param obj
     * @return  返回token信息
     */
    public static TokenResponseObj sendRefreshTokenPost(String url, TokenRequestObj obj){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        StringBuffer sb=new StringBuffer();
        sb.append("?username=");
        sb.append(obj.getUsername()).append("&password=").append(obj.getPassword()).append("&grant_type=")
                .append(obj.getGrant_type()).append("&scope=").append(obj.getScope()).append("&refresh_token=")
                .append(obj.getRefresh_token());
        HttpPost httpPost=new HttpPost(url+sb.toString());

        String authStr= EncoderUtil.encoderBase64("browser:112233");
        httpPost.setHeader("Authorization","Basic "+authStr);

        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
                return null;
            }
            JSONObject jsonObj= JSON.parseObject(EntityUtils.toString(responseEntity));
            if(jsonObj!=null){
                TokenResponseObj result=new TokenResponseObj();
                result.setToken(jsonObj.getString("access_token"));
                result.setRefresh_token(jsonObj.getString("refresh_token"));
                // result.setExpireDate(jsonObj.getInteger("expire_in"));
                return result;
            }

        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    /**
     * 发送请求netty server地址的请求
     * @return
     */
    public static NettyAddressModel sendNettyServerRequest(String url,String access_token){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        HttpPost httpPost=new HttpPost(url);
        httpPost.setHeader("Authorization","Bearer "+access_token);

        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
                return null;
            }
            JSONObject jsonObj= JSON.parseObject(EntityUtils.toString(responseEntity));
            if(jsonObj!=null){
                NettyAddressModel result=new NettyAddressModel();
                result.setCode(jsonObj.getIntValue("code"));
                result.setMsg(jsonObj.getString("msg"));
                result.setHost(jsonObj.getString("host"));
                result.setPort(jsonObj.getIntValue("port"));
                return result;
            }
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    public static User findUserByUsername(String url,String access_token,String username){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        HttpPost httpPost=new HttpPost(url);
        FindUserObj findUserObj=new FindUserObj();
        findUserObj.setUsername(username);
        String jsonString = JSON.toJSONString(findUserObj);
        StringEntity entity=new StringEntity(jsonString,"UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-Type","application/json;charset=utf8");
        httpPost.setHeader("Authorization","Bearer "+access_token);
        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
                return null;
            }
            JSONObject jsonObj= JSON.parseObject(EntityUtils.toString(responseEntity));
            if(jsonObj!=null){
                User user=new User();
                user.setId(jsonObj.getLong("id"));
                user.setUsername(jsonObj.getString("username"));
                user.setPassword(jsonObj.getString("password"));
                user.setMobile(jsonObj.getString("mobile"));
                user.setGender(jsonObj.getString("gender"));
                user.setAddress(jsonObj.getString("address"));
                user.setRegisterTime(jsonObj.getDate("registerTime"));
                user.setBak(jsonObj.getString("bak"));
                return user;
            }
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    public static User findUserByUserId(String url,String access_token,long userId){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        StringBuffer sb=new StringBuffer();
        sb.append("?userId=");
        sb.append(userId);
        HttpPost httpPost=new HttpPost(url+sb.toString());
        httpPost.setHeader("Content-Type","application/json;charset=utf8");
        httpPost.setHeader("Authorization","Bearer "+access_token);
        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
                return null;
            }
            JSONObject jsonObj= JSON.parseObject(EntityUtils.toString(responseEntity));
            if(jsonObj!=null){
                User user=new User();
                user.setId(jsonObj.getLong("id"));
                user.setUsername(jsonObj.getString("username"));
                user.setPassword(jsonObj.getString("password"));
                user.setMobile(jsonObj.getString("mobile"));
                user.setGender(jsonObj.getString("gender"));
                user.setAddress(jsonObj.getString("address"));
                user.setRegisterTime(jsonObj.getDate("registerTime"));
                user.setBak(jsonObj.getString("bak"));
                return user;
            }
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    public static List<SingleMsg> loadUserOffLineMsgFromMysql(String url, Long userId){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        StringBuffer sb=new StringBuffer();
        sb.append("?userId=");
        sb.append(userId);
        HttpGet httpGet=new HttpGet(url+sb.toString());

        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpGet);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
                return null;
            }
            String resultStr=EntityUtils.toString(responseEntity);
            JSONArray jsonArray=JSON.parseArray(resultStr);
            List<SingleMsg> list=new ArrayList<>();
            Iterator it=jsonArray.iterator();
            while(it.hasNext()){
                JSONObject jsonObject=(JSONObject) it.next();
                SingleMsg singleMsg=new SingleMsg();
                singleMsg.setMsgServerId(jsonObject.getLong("msgServerId"));
                singleMsg.setMsgClientId(jsonObject.getString("msgClientId"));
                singleMsg.setFromId(jsonObject.getLong("fromId"));
                singleMsg.setFromName(jsonObject.getString("fromName"));
                singleMsg.setToId(jsonObject.getLong("toId"));
                singleMsg.setToName(jsonObject.getString("toName"));
                singleMsg.setSendTime(jsonObject.getDate("sendTime"));
                singleMsg.setMsgType(jsonObject.getInteger("msgType"));
                singleMsg.setContent(jsonObject.getString("content"));
                singleMsg.setStatus(jsonObject.getString("status"));
                list.add(singleMsg);
            }
            return list;
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    public static void batchAckMsgByLastMsgServerId(String url,Long userId,Long lastMsgServerId){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        StringBuffer sb=new StringBuffer();
        sb.append("?userId=");
        sb.append(userId);
        sb.append("&lastMsgServerId=");
        sb.append(lastMsgServerId);
        HttpGet httpGet=new HttpGet(url+sb.toString());
        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpGet);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
            }
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
    }

    public static List<SingleMsgStoreModel> loadUserOffLineMsg(String url, String username){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        StringBuffer sb=new StringBuffer();
        sb.append("?username=");
        sb.append(username);
        HttpPost httpPost=new HttpPost(url+sb.toString());

//        String authStr= EncoderUtil.encoderBase64("browser:112233");
//        httpPost.setHeader("Authorization","Basic "+authStr);

        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
               // return null;
            }
            String resultStr=EntityUtils.toString(responseEntity);
            JSONArray jsonArray=JSON.parseArray(resultStr);
            List<SingleMsgStoreModel> list=new ArrayList<>();
            Iterator it=jsonArray.iterator();
            while(it.hasNext()){
                JSONObject jsonObject=(JSONObject) it.next();
                SingleMsgStoreModel singleMsgStoreModel=new SingleMsgStoreModel();
                singleMsgStoreModel.setTime(jsonObject.getDate("time"));
                singleMsgStoreModel.setType(jsonObject.getInteger("type"));
                singleMsgStoreModel.setMsgServerId(jsonObject.getLong("msgServerId"));
                singleMsgStoreModel.setMsgClientId(jsonObject.getString("msgClientId"));
                singleMsgStoreModel.setFrom(jsonObject.getString("from"));
                singleMsgStoreModel.setTo(jsonObject.getString("to"));
                singleMsgStoreModel.setStatus(jsonObject.getString("status"));
                singleMsgStoreModel.setContent(jsonObject.getString("content"));
                list.add(singleMsgStoreModel);
            }
            return list;
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    public static List<Group> findUserJoinGroups(String url,String access_token,Long userId,int page){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        StringBuffer sb=new StringBuffer();
        sb.append("?userId=");
        sb.append(userId);
        sb.append("&page=");
        sb.append(page);
        HttpPost httpPost=new HttpPost(url+sb.toString());
        httpPost.setHeader("Content-Type","application/json;charset=utf8");
        httpPost.setHeader("Authorization","Bearer "+access_token);
        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
                return null;
            }
            String resultStr=EntityUtils.toString(responseEntity);
            JSONArray jsonArray=JSON.parseArray(resultStr);
            List<Group> list=new ArrayList<>();
            Iterator it=jsonArray.iterator();
            while(it.hasNext()){
                JSONObject jsonObject=(JSONObject) it.next();
                Group group=new Group();
                group.setId(jsonObject.getLong("id"));
                group.setGroupName(jsonObject.getString("groupName"));
                group.setCreateUserId(jsonObject.getLong("createUserId"));
                group.setCatelogTopId(jsonObject.getInteger("catelogTopId"));
                group.setCatelogTopName(jsonObject.getString("catelogTopName"));
                group.setCatelogSecondId(jsonObject.getInteger("catelogSecondId"));
                group.setCatelogSecondName(jsonObject.getString("catelogSecondName"));
                group.setNeedApply(jsonObject.getByte("needApply"));
                group.setCreateTime(jsonObject.getDate("createTime"));
                list.add(group);
            }
            return list;
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    public static void modifyGroupInfo(String url,String access_token,Group group){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        HttpPost httpPost=new HttpPost(url);
        String jsonString = JSON.toJSONString(group);
        StringEntity entity=new StringEntity(jsonString,"UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-Type","application/json;charset=utf8");
        httpPost.setHeader("Authorization","Bearer "+access_token);
        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
            }
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
    }

    public static void userQuitGroup(String url,String access_token,long userId,long groupId){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        StringBuffer sb=new StringBuffer();
        sb.append("?userId=");
        sb.append(userId);
        sb.append("&groupId=");
        sb.append(groupId);
        HttpPost httpPost=new HttpPost(url+sb.toString());
        httpPost.setHeader("Content-Type","application/json;charset=utf8");
        httpPost.setHeader("Authorization","Bearer "+access_token);
        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
            }
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
    }

    public static Long getMsgServerId(String url){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        HttpGet httpGet=new HttpGet(url);

        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpGet);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
                return null;
            }
            String resultStr=EntityUtils.toString(responseEntity);
            return Long.parseLong(resultStr);
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    public static List<GroupCatelog> getAllGroupCatelog(String url,String access_token){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        HttpGet httpGet=new HttpGet(url);
        httpGet.setHeader("Content-Type","application/json;charset=utf8");
        httpGet.setHeader("Authorization","Bearer "+access_token);
        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpGet);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
            }
            String resultStr=EntityUtils.toString(responseEntity);
            JSONArray jsonArray=JSON.parseArray(resultStr);
            List<GroupCatelog> list=new ArrayList<>();
            Iterator it=jsonArray.iterator();
            while(it.hasNext()){
                JSONObject jsonObject=(JSONObject) it.next();
                GroupCatelog groupCatelog=new GroupCatelog();
                groupCatelog.setId(jsonObject.getInteger("id"));
                groupCatelog.setCatelogName(jsonObject.getString("catelogName"));
                groupCatelog.setCreateTime(jsonObject.getDate("createTime"));
                groupCatelog.setLevel(jsonObject.getByte("level"));
                groupCatelog.setParentCatelogId(jsonObject.getInteger("parentCatelogId"));
                list.add(groupCatelog);
            }
            return list;
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
        return null;
    }

    public static void createGroup(String url, String access_token, GroupDto groupDto){
        CloseableHttpClient httpClient= HttpClientBuilder.create().build();
        HttpPost httpPost=new HttpPost(url);
        String jsonString = JSON.toJSONString(groupDto);
        StringEntity entity=new StringEntity(jsonString,"UTF-8");
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-Type","application/json;charset=utf8");
        httpPost.setHeader("Authorization","Bearer "+access_token);
        CloseableHttpResponse response=null;
        try{
            response=httpClient.execute(httpPost);
            HttpEntity responseEntity=response.getEntity();
            int statusCode=response.getStatusLine().getStatusCode();
            if(SUCCESS_CODE !=statusCode){
                logger.error("响应内容为："+ EntityUtils.toString(responseEntity));
            }
        }catch (Exception e){
            logger.error(e.getMessage(),e);
        }
    }
}
