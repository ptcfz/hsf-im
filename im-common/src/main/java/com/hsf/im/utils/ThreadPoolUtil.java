package com.hsf.im.utils;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolUtil {

    static ThreadPoolExecutor poolExecutor=new ThreadPoolExecutor(10,1000,60, TimeUnit.SECONDS,new SynchronousQueue());

    public static void runTask(Runnable task){
        poolExecutor.execute(task);
    }
}
