package com.hsf.im.utils;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

public class ZookeeperClientFactory {

    public static CuratorFramework createFramework(String connectionString){
        ExponentialBackoffRetry retryPolicy=new ExponentialBackoffRetry(1000,3);

        return CuratorFrameworkFactory.newClient(connectionString,retryPolicy);
    }
}
