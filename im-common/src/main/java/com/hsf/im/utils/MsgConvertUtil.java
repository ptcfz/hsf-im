package com.hsf.im.utils;

import com.hsf.im.domain.SingleMsg;
import com.hsf.im.model.SingleMsgStoreModel;
import com.hsf.im.msg.MsgStatusEnum;
import com.hsf.im.msg.MsgTypeEnum;
import com.hsf.im.msg.ProtoMsg;

import java.util.Date;

/**
 * @Author: chenfz
 */
public class MsgConvertUtil {

    public static SingleMsgStoreModel singleMessageRequest2SingleMsgStoreModel(ProtoMsg.SingleMessageRequest request){
        SingleMsgStoreModel singleMsgStoreModel=new SingleMsgStoreModel();
        singleMsgStoreModel.setMsgClientId(request.getMsgClientId());
        singleMsgStoreModel.setMsgServerId(request.getMsgServerId());
        singleMsgStoreModel.setFrom(request.getFrom());
        singleMsgStoreModel.setTo(request.getTo());
        singleMsgStoreModel.setContent(request.getContent());
        singleMsgStoreModel.setType(MsgTypeEnum.SINGLE_MSG_REQUEST.getCode());
        singleMsgStoreModel.setStatus(MsgStatusEnum.SERVER_RECEIVE.getCode());
        singleMsgStoreModel.setTime(new Date(request.getTime()));
        return singleMsgStoreModel;
    }

    public static SingleMsg singleMessageRequest2SingleMsg(ProtoMsg.SingleMessageRequest request){
        SingleMsg singleMsg = new SingleMsg();
        singleMsg.setMsgServerId(request.getMsgServerId());
        singleMsg.setMsgClientId(request.getMsgClientId());
        singleMsg.setFromName(request.getFrom());
        singleMsg.setFromId(request.getFromId());
        singleMsg.setToName(request.getTo());
        singleMsg.setToId(request.getToId());
        singleMsg.setContent(request.getContent());
        singleMsg.setMsgType(request.getMsgType());
        singleMsg.setStatus(MsgStatusEnum.SERVER_RECEIVE.getCode());
        singleMsg.setSendTime(new Date(request.getTime()));
        return singleMsg;
    }

    public static SingleMsgStoreModel singleMessageResponse2SingleMsgStoreModel(ProtoMsg.SingleMessageResponse response){
        SingleMsgStoreModel singleMsgStoreModel=new SingleMsgStoreModel();
        singleMsgStoreModel.setStatus(MsgStatusEnum.SERVER_ACK.getCode());
        singleMsgStoreModel.setFrom(response.getFrom());
        singleMsgStoreModel.setTo(response.getTo());
        singleMsgStoreModel.setMsgClientId(response.getMsgClientId());
        singleMsgStoreModel.setMsgServerId(response.getMsgServerId());
        singleMsgStoreModel.setType(MsgTypeEnum.SINGLE_MSG_RESPONSE.getCode());
        singleMsgStoreModel.setTime(new Date());
        return singleMsgStoreModel;
    }


}
