package com.hsf.im.mq.producer.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProducerFactory {

    static Logger logger= LoggerFactory.getLogger(ProducerFactory.class);

    public static ImTransactionMQProducer getTransactionProducer(String groupName,String nameSrvAddr){
        return new ImTransactionMQProducer(groupName,nameSrvAddr);
    }

    public static ImDefaultMQProducer getProducer(String groupName,String nameSrvAddr){
        ImDefaultMQProducer producer=new ImDefaultMQProducer(groupName,nameSrvAddr);
        return  producer;
    }
}
