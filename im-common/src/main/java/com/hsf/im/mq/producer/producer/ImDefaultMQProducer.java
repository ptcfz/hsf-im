package com.hsf.im.mq.producer.producer;

import com.hsf.im.utils.RunTimeUtil;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ImDefaultMQProducer {

    Logger logger= LoggerFactory.getLogger(ImDefaultMQProducer.class);

    private DefaultMQProducer producer;

    private MessageQueueSelector queueSelector;

    public ImDefaultMQProducer(String groupName,String nameSrvAddr){
        producer=new DefaultMQProducer(groupName);
        producer.setInstanceName(RunTimeUtil.getRocketMqUniqeInstanceName());
        producer.setRetryTimesWhenSendFailed(3);
        producer.setNamesrvAddr(nameSrvAddr);
        try{
            producer.start();
            queueSelector=new MessageQueueSelector() {
                @Override
                public MessageQueue select(List<MessageQueue> mqs, Message msg, Object arg) {
                    Long id=(Long)arg;
                    Long tempIndex=id%mqs.size();
                    Integer index=tempIndex.intValue();
                    return mqs.get(index);
                }
            };
        }catch(MQClientException e){
            logger.error("start rocket mq producer error:"+e.getErrorMessage(),e);
        }
    }

    public SendResult send(Message message, Long arg){
        SendResult result=null;
        try{
          result = producer.send(message,queueSelector,arg);
        }catch(Exception e){
            logger.error("send message error:"+e.getMessage(),e);
        }
        return result;
    }
}
