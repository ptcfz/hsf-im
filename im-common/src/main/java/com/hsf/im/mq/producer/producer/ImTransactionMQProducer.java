package com.hsf.im.mq.producer.producer;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.*;

public class ImTransactionMQProducer {

    Logger logger= LoggerFactory.getLogger(ImTransactionMQProducer.class);

    private TransactionMQProducer producer;

    public ImTransactionMQProducer(String groupName,String nameSrvAddr){
        producer=new TransactionMQProducer(groupName);
        ExecutorService executorService=new ThreadPoolExecutor(2,5,100, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(2000),new ThreadFactory(){
            @Override
            public Thread newThread(Runnable r){
                Thread thread=new Thread(r);
                thread.setName("client-thransaction-msg-check-thread");
                return thread;
            }
        });
        producer.setNamesrvAddr(nameSrvAddr);
        producer.setExecutorService(executorService);
        try{
            producer.start();
        }catch(MQClientException e){
            logger.error("start mq producer error:"+e.getErrorMessage(),e);
        }
    }

    public TransactionSendResult sendMessageInTransaction(final Message msg, final Object arg) throws MQClientException{
        return producer.sendMessageInTransaction(msg,arg);
    }

    public void setTransactionListener(TransactionListener transactionListener) {
        producer.setTransactionListener(transactionListener);
    }
}
