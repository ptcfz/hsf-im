package com.hsf.im.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

/**
 * @Author: chenfz
 * 一对一 消息 用于 mq传输，和 存储的对象
 */
@Document(collection = "singlemsg")
public class SingleMsgStoreModel {

    public static final String MSG_STATUS_SERVER_RECEIVE="SERVER_RECEIVE";
    public static final String MSG_STATUS_CLIENT_ACK="CLIENT_ACK";

    public static final int MSG_REQUEST_TYPE=0;
    public static final int MSG_ACK_TYPE=1;

    @Id
    private long msgServerId;

    @Field("msgClientId")
    private String msgClientId;

    @Field("from")
    private String from;

    @Field("to")
    private String to;

    @Field("time")
    private Date time;

    /**
     * 消息类型：
     * 0--单聊消息
     * 1--单聊消息的ack
     */
    @Field("type")
    private int type;

    @Field("content")
    private String content;

    /**
     * SERVER_RECEIVE(服务端接收到)
     * CLIENT_ACK(客户端确认--投递成功)
     */
    @Field("status")
    private String status;

    public String getMsgClientId() {
        return msgClientId;
    }

    public void setMsgClientId(String msgClientId) {
        this.msgClientId = msgClientId;
    }

    public long getMsgServerId() {
        return msgServerId;
    }

    public void setMsgServerId(long msgServerId) {
        this.msgServerId = msgServerId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
