package com.hsf.im.model;

/**
 * @Author: chenfz
 */
public class RocketMqConst {

    //消息请求

    public final static String SINGLE_MSG_CONSUMER_GROUP="SINGLE_MSG_CONSUMER_GROUP";
    public final static String SINGLE_MSG_PRODUCER_GROUP="SINGLE_MSG_PRODUCER_GROUP";
    public final static String SINGLE_MSG_TOPIC="SINGLE_MSG_TOPIC";
    public final static String SINGLE_MSG_TAG="SINGLE_MSG_TAG";

    //消息响应

    public final static String SINGLE_MSG_RES_CONSUMER_GROUP="SINGLE_MSG_RES_CONSUMER_GROUP";
    public final static String SINGLE_MSG_RES_PRODUCER_GROUP="SINGLE_MSG_RES_PRODUCER_GROUP";
    public final static String SINGLE_MSG_RES_TOPIC="SINGLE_MSG_RES_TOPIC";
    public final static String SINGLE_MSG_RES_TAG="SINGLE_MSG_RES_TAG";
}
