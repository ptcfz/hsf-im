package com.hsf.im.model.dto;

import com.hsf.im.domain.Group;
import com.hsf.im.domain.User;

/**
 * @Author: chenfz
 * 群组相关的传输对象
 */
public class GroupDto {

    private Group group;

    private User user;

    private Integer page;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
