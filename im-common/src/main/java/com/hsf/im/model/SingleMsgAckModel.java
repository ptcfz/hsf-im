package com.hsf.im.model;

/**
 * @Author: chenfz
 */
public class SingleMsgAckModel {



    private long msgServerId;

    private String msgClientId;

    private boolean result;

    private int code;

    private String info;

    private String from;

    private String to;

    public long getMsgServerId() {
        return msgServerId;
    }

    public void setMsgServerId(long msgServerId) {
        this.msgServerId = msgServerId;
    }

    public String getMsgClientId() {
        return msgClientId;
    }

    public void setMsgClientId(String msgClientId) {
        this.msgClientId = msgClientId;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
